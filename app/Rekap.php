<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekap extends Model
{
    //

    protected $table = 'rekap';

    protected $fillable = [
        'user_id', 'poin', 'juri_id' , 'jarak' , 'waktu' , 'kecepatan'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function juri()
    {
        return $this->belongsTo('App\Juri');
    }
}
