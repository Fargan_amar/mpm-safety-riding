<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_penilaian extends Model
{
    //
    protected $fillable = [
        'user_id', 'penilaian_id', 'kesalahan' , 'juri_id' , 'tgl'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function penilaian()
    {
        return $this->belongsTo('App\Penilaian');
    }

    public function juri()
    {
        return $this->belongsTo('App\Juri');
    }
}
