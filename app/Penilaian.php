<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    //
    protected $fillable = [
        'deskripsi', 'poin', 'modul_id' , 'coaching' , 'jarak' , 'waktu'
    ];

    public function modul()
    {
        return $this->belongsTo('App\Modul');
    }


    public function nilai()
    {
        return $this->hasMany('App\User_penilaian');
    }

}
