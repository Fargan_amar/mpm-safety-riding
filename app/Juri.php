<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Juri extends Model
{
    //
    protected $fillable = [
        'user_id', 'modul_id', 'tgl' ,'deskripsi' , 'poin_pengurangan' , 'kategori_id' , 'target_waktu'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function modul()
    {
        return $this->belongsTo('App\Modul');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }


    public function user_juris()
    {
        return $this->hasMany('App\User_penilaian.php');
    }
}
