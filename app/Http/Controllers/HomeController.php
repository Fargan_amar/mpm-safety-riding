<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penilaian;
use App\Kategori;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Juri;
use App\Jabatan;
use App\Modul;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


     public function mpm()
     {
         return view('error.error-page-session');
     }

    public function index()
    {
        $today = date('Y-m-d');
        $juri = Juri::whereDate('tgl' , $today)->where('user_id' , Auth::user()->id)->latest('tgl')->first();
        $cek_juri = Juri::whereDate('tgl' , $today)->where('selesai' , 0)->latest('tgl')->first();
        // $kalender_juri  = Juri::all();
        $kalender_juri  = Juri::distinct('tgl_calendar' , 'modul_id')->orderBy('modul_id')->get(['tgl_calendar' ,'modul_id']);
        // return $kalender_juri;
        $daftar_modul = Modul::all();
        $modul = Penilaian::distinct()->get(['modul_id']);
        $kategori = Kategori::whereNotIn('id',[5])->get();
        $p_instruktur  = User::where('kategori_id' , 1)->get();
        $p_sport = User::where('kategori_id', 2)->get();
        $p_dealer = User::where('kategori_id', 3)->get();
        $p_matic = User::where('kategori_id', 4)->get();
// return $kalender_juri;
        return view('home', compact('modul', 'kategori', 'p_instruktur' , 'p_sport' , 'p_dealer' , 'p_matic' , 'juri' , 'cek_juri' , 'kalender_juri' , 'daftar_modul'));
    }

    public function profile()
    {
        $jabatan = Jabatan::all();
        $kategori = Kategori::all();
        return view('Dashboard.profile' , compact('jabatan' , 'kategori'));
    }



}
