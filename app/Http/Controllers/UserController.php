<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\sendPasswordAdmin;
use App\Mail\LupaPassword;
use App\User;
use App\Kategori;
use App\Jabatan;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Rekap;
use App\User_penilaian;
use App\Modul;
use App\Penilaian;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Password_reset;
use Illuminate\Support\Facades\Route;
use App\Juri;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    public function admin()
    {
        $user = User::where('is_admin', 1)->whereNotIn('jabatan_id', [1])->get();
        $kategori = Kategori::all();
        $jabatan = Jabatan::all();
        return view('Dashboard.user.admin.index', compact('user', 'jabatan', 'kategori'));
    }

    public function peserta()
    {
        $user = User::where('jabatan_id' , 3)->get();
        $kategori =  Kategori::whereNotIn('id', [5])->get();
        $jabatan = Jabatan::where('id',3)->get();
        return view('Dashboard.user.peserta.index' , compact('user','jabatan' , 'kategori'));
    }

    public function tambahPeserta(Request $request)
    {
        $time = Carbon::now()->toDateTimeString();
        $cek = User::where('email', $request->email)->first();
        if(empty($request->email)) {
            $user = User::insert([
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'kategori_id' => $request->kategori_id,
                'jabatan_id' => $request->jabatan_id,
                'no_punggung' => $request->no_punggung,
                'password' => '$2y$12$W.jiO3v5eLGH6fsxf7TXreA16ickZLYTJiZgbU3kdtTDChGxN7/q6',
                'created_at' => $time,
                'updated_at' => $time
            ]);
            return back()->with('sukses', 'sukses');
        }
        if(!empty($cek)){
            return back()->with('error', 'error');
        }
        $user = User::insert([
            'name' => $request->name,
            'email' => $request->email,
            'gender' => $request->gender,
            'kategori_id' => $request->kategori_id,
            'jabatan_id' => $request->jabatan_id,
            'no_punggung' => $request->no_punggung,
            'password' => '$2y$12$W.jiO3v5eLGH6fsxf7TXreA16ickZLYTJiZgbU3kdtTDChGxN7/q6',
            'created_at' => $time,
            'updated_at' => $time
        ]);
        return back()->with('sukses' , 'sukses');
    }

    public function tambahAdmin(Request $request)
    {
        $is_admin = 0;
        $time = Carbon::now()->toDateTimeString();
        $cek = User::where('email', $request->email)->first();
        $pass = preg_replace('/\s/', '', $request->name) . str_random(10) . rand(10, 99);
        if (!empty($cek)) {
            return back()->with('error', 'error');
        }
        if($request->is_admin == "on" || $request->jabatan_id == 2){
            $is_admin = 1;
        }
        else if($request->is_admin == "off"){
            $is_admin = 0;
        }

        $user = User::insert([
            'name' => $request->name,
            'email' => $request->email,
            'gender' => $request->gender,
            'kategori_id' => $request->kategori_id,
            'jabatan_id' => $request->jabatan_id,
            'no_punggung' => $request->no_punggung,
            'password' => Hash::make($pass),
            'is_admin' => $is_admin,
            'created_at' => $time,
            'updated_at' => $time
        ]);
        Mail::to($request->email)->send(new sendPasswordAdmin($request->email , $pass));
        return back()->with('sukses', 'sukses');

            // return (new sendPasswordAdmin($request->email, $pass))->render();
    }

    public function EditUser(Request $request)
    {
        $time = Carbon::now()->toDateTimeString();
        $cek = User::where('email', $request->email)->first();
        $cek_email = User::find($request->id);
        if($cek_email->email == $request->email){
            $user = User::where('id', $request->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'kategori_id' => $request->kategori_id,
                'jabatan_id' => $request->jabatan_id,
                'no_punggung' => $request->no_punggung,
                'password' => '$2y$12$W.jiO3v5eLGH6fsxf7TXreA16ickZLYTJiZgbU3kdtTDChGxN7/q6',
                'updated_at' => $time
            ]);
        }
        else{
            if (!empty($cek)) {
                return back()->with('error', 'error');
            }
            $user = User::where('id', $request->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'kategori_id' => $request->kategori_id,
                'jabatan_id' => $request->jabatan_id,
                'no_punggung' => $request->no_punggung,
                'password' => '$2y$12$W.jiO3v5eLGH6fsxf7TXreA16ickZLYTJiZgbU3kdtTDChGxN7/q6',
                'updated_at' => $time
            ]);
        }

        return back()->with('update', 'sukses');
    }

    public function ubahProfile(Request $request)
    {
        $time = Carbon::now()->toDateTimeString();
        $cek = User::where('email', $request->email)->first();
        $user = User::where('id', $request->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'gender' => $request->gender,
            'kategori_id' => $request->kategori,
            'jabatan_id' => $request->jabatan,
            'no_punggung' => $request->no_punggung,
            'updated_at' => $time
        ]);


        return back()->with('sukses', 'sukses');
    }

    public function CekPassword($id , $pass)
    {
        $ambil_password  = User::find($id)->password;
       if(Hash::check($pass , $ambil_password))
       {
           return "true";
       }
       else{
            return "false";
       }

    }

    public function UbahPassword(Request $request)
    {
        $update = User::where('id' , Auth::user()->id)->update([
            'password' => Hash::make($request->password)
        ]);
        return back()->with('pass', 'pass');

    }


    public function EditAdmin(Request $request)
    {
        $time = Carbon::now()->toDateTimeString();
        $is_admin = 0;
        if ($request->is_admin == "on" || $request->jabatan_id == 2) {
            $is_admin = 1;
        } else if ($request->is_admin == "off") {
            $is_admin = 0;
        }
        $cek = User::where('email', $request->email)->first();
        $cek_email = User::find($request->id);
        if($cek_email->email == $request->email){
            $user = User::where('id', $request->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'kategori_id' => $request->kategori_id,
                'jabatan_id' => $request->jabatan_id,
                'no_punggung' => $request->no_punggung,
                'is_admin' => $is_admin,
                'updated_at' => $time
            ]);
        }
        else{
            if (!empty($cek)) {
                return back()->with('error', 'error');
            }
            $user = User::where('id', $request->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'kategori_id' => $request->kategori_id,
                'jabatan_id' => $request->jabatan_id,
                'no_punggung' => $request->no_punggung,
                'is_admin' => $is_admin,
                'updated_at' => $time
            ]);
        }


        return back()->with('update', 'sukses');
    }

    public function DeleteUser(Request $request)
    {
        $time = Carbon::now()->toDateTimeString();
        // $cek = User::where('name', $request->name);
        // if (!empty($cek)) {
        //     return back()->with('error', 'error');
        // }
        $user = User::where('id', $request->id)->delete();
        return back()->with('delete', 'sukses');
    }


    public function makeAdmin(Request $request)
    {
        $time = Carbon::now()->toDateTimeString();
        // $cek = User::where('name', $request->name);
        // if (!empty($cek)) {
        //     return back()->with('error', 'error');
        // }
        $usr = User::find($request->id);
        $pass =  preg_replace('/\s/', '', $usr->name).str_random(10) . rand(10, 99);
        $user = User::where('id', $request->id)->update(['is_admin' => 1, 'password' => Hash::make($pass)
        ]);
        Mail::to($usr->email)->send(new sendPasswordAdmin($usr->email, $pass));
        return back()->with('update', 'sukses');
    }

    public function removeAdmin(Request $request)
    {
        $time = Carbon::now()->toDateTimeString();
        // $cek = User::where('name', $request->name);
        // if (!empty($cek)) {
        //     return back()->with('error', 'error');
        // }
        $usr = User::find($request->id);
        $pass = $usr->name . str_random(10) . rand(10, 99);
        $user = User::where('id', $request->id)->update(['is_admin' => 0 , 'password' => ""
        ]);

        return back()->with('update', 'sukses');
    }


    public function lupaPassword(Request $request)
    {
        $cek_email = User::where('email' , $request->email)->first();
        if (!empty($cek_email)) {
            $token = Str::random(40);
            $insert_reset_pass = Password_reset::insert(['email' => $request->email , 'token' => $token]);
            Mail::to($request->email)->send(new lupaPassword( $request->email, $token));
            return back()->with('reset' , 'reset');
        }
        else{
            return back()->with('error' , 'error');
        }
    }

    public function resetPassword($token)
    {
        $cek_token = Password_reset::where('token' , $token)->first();
        if(!empty($cek_token)){
            return view('reset-pass', compact('token'));
        }
        else{
            return view('error.error-page');
        }
    }

    public function submitReset(Request $request , $token)
    {
        $email = Password_reset::where('token', $token)->first();
        if(!empty($email)){
            $password = hash::make($request->password);
            $update_password = User::where('email' , $email->email)->update(['password' => $password]);
            $remove_token = Password_reset::where('token', $token)->delete();
            return view('login')->with('sukses' , 'sukses');
        }
        else{
            return view('error.error-page');
        }
    }


    public function report($id)
    {
        $modul = Modul::all();
        $token = hash::make(Str::random(40));
        return view('Dashboard.user.peserta.report' , compact('modul' ,'id' , 'token'));


    }

    public function reportPeserta($id,$modul_id ,$year , $month )
    {
        $array_bulan = array('Januari' , 'Februari' , 'Maret' , 'April' , 'Mei' , 'Juni' , 'Juli' , 'Agustus' , 'September' , 'Oktober' , 'November' , 'Desember');
        $modul = Modul::all();
        $nama_modul = Modul::find($modul_id);
        $penilaian_id = Penilaian::where('modul_id', $modul_id)->get(['id']);
        $bulan =  $month;
        $tahun = $year;
        $ambil_juri = User_penilaian::where('user_id', $id)->whereIn('penilaian_id', $penilaian_id)->whereMonth('tgl',$bulan)->whereYear('tgl' , $tahun)->distinct('juri_id')->get(['juri_id']);
        // $ambil_juri = Juri::whereMonth('tgl',$bulan)->whereYear('tgl' , $tahun)->distinct('id')->get(['id']);
        $rekap  = Rekap::whereIn('juri_id', $ambil_juri)->where('user_id', $id)->with('juri')->get();
        $user = User::find($id);
        $token = Str::random(40);
        $jumlah_poin_1000 = Rekap::whereIn('juri_id', $ambil_juri)->where('user_id', $id)->where('poin' , 1000)->count('poin');
        $jumlah_event = count($rekap);
        $persentase = 0;
        if($jumlah_poin_1000 > 0){
            $persentase = ($jumlah_poin_1000 * 100) /  $jumlah_event;
        }
        $avg_jarak = Rekap::whereIn('juri_id', $ambil_juri)->where('user_id', $id)->where('poin' , 1000)->avg('jarak');
        $avg_kecepatan = Rekap::whereIn('juri_id', $ambil_juri)->where('user_id', $id)->where('poin', 1000)->avg('kecepatan');


        return view('Dashboard.user.peserta.report-modul', compact('modul' ,'rekap' ,'id' , 'nama_modul' ,'bulan' , 'array_bulan' , 'tahun' ,'user' ,'token' , 'persentase' , 'jumlah_event' , 'avg_jarak' , 'avg_kecepatan'));



    }

    public function publicReportPeserta($token , $id,$modul_id ,$year , $month )
    {
        $array_bulan = array('Januari' , 'Februari' , 'Maret' , 'April' , 'Mei' , 'Juni' , 'Juli' , 'Agustus' , 'September' , 'Oktober' , 'November' , 'Desember');
        $modul = Modul::all();
        $nama_modul = Modul::find($modul_id);
        $penilaian_id = Penilaian::where('modul_id', $modul_id)->get(['id']);
        $bulan =  $month;
        $tahun = $year;
        $ambil_juri = User_penilaian::where('user_id', $id)->whereIn('penilaian_id', $penilaian_id)->whereMonth('tgl',$bulan)->whereYear('tgl' , $tahun)->distinct('juri_id')->get(['juri_id']);
        $rekap  = Rekap::whereIn('juri_id', $ambil_juri)->where('user_id', $id)->with('juri')->get();
        $user = User::find($id);
        $jumlah_poin_1000 = Rekap::whereIn('juri_id', $ambil_juri)->where('user_id', $id)->where('poin' , 1000)->count('poin');
        $jumlah_event = count($rekap);
        $persentase = 0;
        if($jumlah_poin_1000 > 0){
            $persentase = ($jumlah_poin_1000 * 100) /  $jumlah_event;
        }
        $avg_jarak = Rekap::whereIn('juri_id', $ambil_juri)->where('user_id', $id)->where('poin' , 1000)->avg('jarak');
        $avg_kecepatan = Rekap::whereIn('juri_id', $ambil_juri)->where('user_id', $id)->where('poin', 1000)->avg('kecepatan');

        // $token = hash::make(Str::random(40));

        // return $array_bulan;

        return view('Dashboard.user.peserta.public-report-modul', compact('modul' ,'rekap' ,'id' , 'nama_modul' ,'bulan' , 'array_bulan' , 'tahun' ,'user' ,'token' ,'persentase', 'jumlah_event', 'avg_jarak', 'avg_kecepatan'));


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
