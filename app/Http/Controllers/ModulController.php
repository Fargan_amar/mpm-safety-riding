<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modul;
use Carbon\Carbon;
use App\User_penilaian;
use App\Penilaian;
use App\Juri;
use App\Rekap;

class ModulController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $modul = Modul::all();
        return view('Dashboard.modul.index', compact('modul'));
    }

    public function tambahModul(Request $request)
    {
        //
        $time = Carbon::now()->toDateTimeString();
        $cek = Modul::where('name' , $request->name)->first();
        if (!empty($cek)) {
            return back()->with('error', 'error');
        }
        $tambah = Modul::insert([
            'name' => $request->name,
            'created_at' => $time,
            "updated_at" => $time,
        ]);
        return back()->with('sukses', 'sukses');
    }

    public function editModul(Request $request)
    {
        //
        $time = Carbon::now()->toDateTimeString();
        $cek = Modul::where('name', $request->name)->first();
        $cek_nama = Modul::find($request->id);
        if($cek_nama->name ==  $request->name){
            $tambah = Modul::where('id',$request->id)->update([
                'name' => $request->name,
                // 'created_at' => $time,
                "updated_at" => $time,
            ]);
        }
        else{
            if (!empty($cek)) {
                return back()->with('error', 'error');
            }
            $tambah = Modul::where('id',$request->id)->update([
                'name' => $request->name,
                // 'created_at' => $time,
                "updated_at" => $time,
            ]);

        }
        return back()->with('update', 'sukses');
    }


    public function hapusModul(Request $request)
    {
        //
        $delete = Modul::where('id', $request->id)->delete();
        return back()->with('delete', 'sukses');
    }

    public  function lihatReport($modul_id)
    {
        $array_penilaian_id = array();
        $penilaian = Penilaian::where('modul_id' , $modul_id)->get();
        foreach ($penilaian as $value) {
            array_push($array_penilaian_id, $value->id);
        }
        $current_date = User_penilaian::whereIn('penilaian_id' , $array_penilaian_id)->max('tgl');
        $tgl = User_penilaian::whereIn('penilaian_id' , $array_penilaian_id)->distinct('tgl')->get(['tgl']);
        $report = User_penilaian::whereIn('penilaian_id' , $array_penilaian_id)->where('tgl' , $current_date)->distinct('penilaian_id' ,'user_id')->get(['penilaian_id' ,'juri_id'  ]);
        $report_user = User_penilaian::whereIn('penilaian_id', $array_penilaian_id)->where('tgl', $current_date)->distinct('user_id')->get(['user_id']);
        // $array_user = array();
        $array_kesalahan = array();
        $index = 0;
        $index_kesalahan = 0;
        $index_report = 0;
        $index_user = 0;
        foreach ($report as $key => $value) {
            $kesalahan = User_penilaian::where('penilaian_id' , $value->penilaian_id)->where('tgl', $current_date)->get(['kesalahan']);
            $jarak = User_penilaian::where('penilaian_id', $value->penilaian_id)->where('tgl', $current_date)->get(['jarak']);
            $waktu = User_penilaian::where('penilaian_id', $value->penilaian_id)->where('tgl', $current_date)->get(['waktu']);
            $keterangan = User_penilaian::where('penilaian_id', $value->penilaian_id)->where('tgl', $current_date)->get(['keterangan']);
            // $report[$key]['deskripsi'] = $value->penilaian->deskripsi;
            foreach ($kesalahan as $i => $item) {
                    $name_index = "kesalahan".$i;
                    $report[$key][$name_index] = $item->kesalahan;
                    $index_user++;
            }
            foreach ($jarak as $index => $j) {
                $nama_index = "jarak".$index;
                $report[$key][$nama_index] = $j->jarak;
            }
            foreach ($waktu as $w => $value_waktu) {
                $nama_index_waktu = "waktu".$w;
                $report[$key][$nama_index_waktu] = $value_waktu->waktu;
            }
            foreach($keterangan as $k => $value_keterangan){
                $keterangan = "keterangan".$k;
                $report[$key][$keterangan] = $value_keterangan->keterangan;
            }
        }

        foreach ($report_user as $i => $value) {
            $sum_nilai = User_penilaian::where('tgl', $current_date)->where('user_id' ,  $value->user_id)->sum('nilai');
            $report_user[$i]['poin'] = 1000 - $sum_nilai;
        }

        if($current_date == null){
            return view('error.error-page');
        }
        else{
            // return $report;
            $juri = Juri::find($report[0]->juri_id);
            $rekap = Rekap::where('juri_id' , $juri->id)->get();
            return view('Dashboard.modul.lihat-report' , compact('tgl' , 'report','report_user' , 'penilaian' ,'nilai' ,'array_user' , 'array_kesalahan' ,'modul_id' ,'current_date' , 'juri' , 'rekap'));
        }
        // return $array_kesalahan;
        // return $array_user;
        // return $report_user;
    }

    public function lihatReportTgl($modul_id , $date)
    {
        $array_penilaian_id = array();
        $penilaian = Penilaian::where('modul_id', $modul_id)->get();
        foreach ($penilaian as $value) {
            array_push($array_penilaian_id, $value->id);
        }
        $current_date = $date;
        $tgl = User_penilaian::whereIn('penilaian_id', $array_penilaian_id)->distinct('tgl')->get(['tgl']);
        $report = User_penilaian::whereIn('penilaian_id', $array_penilaian_id)->where('tgl', $current_date)->distinct('penilaian_id', 'user_id')->get(['penilaian_id', 'juri_id']);
        $report_user = User_penilaian::whereIn('penilaian_id', $array_penilaian_id)->where('tgl', $current_date)->distinct('user_id')->get(['user_id']);
        // $array_user = array();
        $array_kesalahan = array();
        $index = 0;
        $index_kesalahan = 0;
        $index_report = 0;
        $index_user = 0;
        foreach ($report as $key => $value) {
            $kesalahan = User_penilaian::where('penilaian_id', $value->penilaian_id)->where('tgl', $current_date)->get(['kesalahan']);
            $jarak = User_penilaian::where('penilaian_id', $value->penilaian_id)->where('tgl', $current_date)->get(['jarak']);
            $waktu = User_penilaian::where('penilaian_id', $value->penilaian_id)->where('tgl', $current_date)->get(['waktu']);
            // $report[$key]['deskripsi'] = $value->penilaian->deskripsi;
            foreach ($kesalahan as $i => $item) {
                $name_index = "kesalahan" . $i;
                $report[$key][$name_index] = $item->kesalahan;
                $index_user++;
            }
            foreach ($jarak as $index => $j) {
                $nama_index = "jarak" . $index;
                $report[$key][$nama_index] = $j->jarak;
            }

            foreach ($waktu as $w => $value_waktu) {
                $nama_index_waktu = "waktu".$w;
                $report[$key][$nama_index_waktu] = $value_waktu->waktu;
            }
        }
        foreach ($report_user as $i => $value) {
            $sum_nilai = User_penilaian::where('tgl', $current_date)->where('user_id', $value->user_id)->sum('nilai');
            $kecepatan = Rekap::where('juri_id', $report[0]->juri->id)->where('user_id' , $value->user_id)->get();
            foreach ($kecepatan as  $k) {
                $report_user[$i]['kecepatan'] = $k->kecepatan;
            }
            $report_user[$i]['poin'] = 1000 - $sum_nilai;
        }
        $juri = Juri::find($report[0]->juri->id);
        $rekap = Rekap::where('juri_id' , $report[0]->juri->id)->get();
        return view('Dashboard.modul.lihat-report', compact('tgl', 'report', 'report_user', 'penilaian', 'nilai', 'array_user', 'array_kesalahan', 'modul_id', 'current_date' , 'rekap' ,'juri'));
        // return $array_kesalahan;
        // return $array_user;
        // return $report_user;
    }

    public function PublicLihatReportTgl($token , $modul_id, $date)
    {
        $array_penilaian_id = array();
        $penilaian = Penilaian::where('modul_id', $modul_id)->get();
        foreach ($penilaian as $value) {
            array_push($array_penilaian_id, $value->id);
        }
        $current_date = $date;
        $tgl = User_penilaian::whereIn('penilaian_id', $array_penilaian_id)->distinct('tgl')->get(['tgl']);
        $report = User_penilaian::whereIn('penilaian_id', $array_penilaian_id)->where('tgl', $current_date)->distinct('penilaian_id', 'user_id')->get(['penilaian_id', 'juri_id']);
        $report_user = User_penilaian::whereIn('penilaian_id', $array_penilaian_id)->where('tgl', $current_date)->distinct('user_id')->get(['user_id']);
        // $array_user = array();
        $array_kesalahan = array();
        $index = 0;
        $index_kesalahan = 0;
        $index_report = 0;
        $index_user = 0;
        foreach ($report as $key => $value) {
            $kesalahan = User_penilaian::where('penilaian_id', $value->penilaian_id)->where('tgl', $current_date)->get(['kesalahan']);
            $jarak = User_penilaian::where('penilaian_id', $value->penilaian_id)->where('tgl', $current_date)->get(['jarak']);
            $waktu = User_penilaian::where('penilaian_id', $value->penilaian_id)->where('tgl', $current_date)->get(['waktu']);
            // $report[$key]['deskripsi'] = $value->penilaian->deskripsi;
            foreach ($kesalahan as $i => $item) {
                $name_index = "kesalahan" . $i;
                $report[$key][$name_index] = $item->kesalahan;
                $index_user++;
            }
            foreach ($jarak as $index => $j) {
                $nama_index = "jarak" . $index;
                $report[$key][$nama_index] = $j->jarak;
            }

            foreach ($waktu as $w => $value_waktu) {
                $nama_index_waktu = "waktu".$w;
                  $report[$key][$nama_index_waktu] = $value_waktu->waktu;
            }
        }
        foreach ($report_user as $i => $value) {
            $sum_nilai = User_penilaian::where('tgl', $current_date)->where('user_id', $value->user_id)->sum('nilai');
            $kecepatan = Rekap::where('juri_id', $report[0]->juri->id)->where('user_id', $value->user_id)->get();
            foreach ($kecepatan as  $k) {
                $report_user[$i]['kecepatan'] = $k->kecepatan;
            }
            $report_user[$i]['poin'] = 1000 - $sum_nilai;

            $report_user[$i]['poin'] = 1000 - $sum_nilai;
        }
        $juri = Juri::find($report[0]->juri->id);
        $rekap = Rekap::where('juri_id', $report[0]->juri->id)->get();
        return view('Dashboard.modul.public-lihat-report', compact('tgl', 'report', 'report_user', 'penilaian', 'nilai', 'array_user', 'array_kesalahan', 'modul_id', 'current_date', 'rekap','juri'));
        // return $array_kesalahan;
        // return $array_user;
        // return $report_user;
    }


    public function LihatPenilaian($modul_id)
    {
        $penilaian_modul  = Penilaian::where('modul_id' , $modul_id)->get();
        $modul = Modul::all();
        $nama_modul = Modul::find($modul_id);
        return view('Dashboard.modul.lihat-penilaian', compact('penilaian_modul', 'modul' , 'nama_modul'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
