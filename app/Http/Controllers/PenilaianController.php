<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penilaian;
use Carbon\Carbon;
use App\Modul;

class PenilaianController extends Controller
{
    //


    public function index()
    {
        $penilaian_modul  = Penilaian::all();
        $modul = Modul::all();
        return view('Dashboard.penilaian.modul-penilaian', compact('penilaian_modul', 'modul'));
    }

    public function tambahPenilaian(Request $request , $modul_id)
    {
        $time = Carbon::now()->toDateTimeString();
        if(isset($request->deskripsi)){
            for ($i=0; $i < sizeof($request->deskripsi) ; $i++) {
                $insert = Penilaian::insert([
                    'deskripsi' => $request->deskripsi[$i],
                    'poin' => $request->poin[$i],
                    'modul_id' => $modul_id,
                    'coaching' => $request->coaching[$i],
                    'created_at' => $time,
                    'updated_at' => $time
                ]);
            }

            // return redirect('/penilaian')->with('penilaian', 'penilaian');
            return back()->with('penilaian', 'penilaian');
        }
        else{
            return back();
        }
    }

    public function tambahPenilaianDis(Request $request, $modul_id)
    {
        $time = Carbon::now()->toDateTimeString();
        if (isset($request->deskripsi)) {
            for ($i = 0; $i < sizeof($request->deskripsi); $i++) {
                $insert = Penilaian::insert([
                    'deskripsi' => $request->deskripsi[$i],
                    'poin' => $request->poin[$i],
                    'modul_id' => $modul_id,
                    'coaching' => $request->coaching[$i],
                    'diskualifikasi' => "yes",
                    'created_at' => $time,
                    'updated_at' => $time
                ]);
            }

            return redirect('/penilaian')->with('penilaian', 'penilaian');
        } else {
            return back();
        }
    }

    public function tambahPenilaianJarak(Request $request, $modul_id)
    {
        $time = Carbon::now()->toDateTimeString();
        if (isset($request->deskripsi)) {
            for ($i = 0; $i < sizeof($request->deskripsi); $i++) {
                $insert = Penilaian::insert([
                    'deskripsi' => $request->deskripsi[$i],
                    'poin' => $request->poin[$i],
                    'modul_id' => $modul_id,
                    'coaching' => $request->coaching[$i],
                    'jarak' => "yes",
                    'created_at' => $time,
                    'updated_at' => $time
                ]);
            }

            return redirect('/penilaian')->with('penilaian', 'penilaian');
        } else {
            return back();
        }
    }

    public function tambahPenilaianWaktu(Request $request, $modul_id)
    {
        $time = Carbon::now()->toDateTimeString();
        if (isset($request->deskripsi)) {
            for ($i = 0; $i < sizeof($request->deskripsi); $i++) {
                $insert = Penilaian::insert([
                    'deskripsi' => $request->deskripsi[$i],
                    'poin' => $request->poin[$i],
                    'modul_id' => $modul_id,
                    'coaching' => $request->coaching[$i],
                    'waktu' => "yes",
                    'created_at' => $time,
                    'updated_at' => $time
                ]);
            }

            return redirect('/penilaian')->with('penilaian', 'penilaian');
        } else {
            return back();
        }
    }

    public function editPenilaian(Request $request)
    {
        $update = Penilaian::where('id' , $request->id)->update([
            "deskripsi" => $request->deskripsi,
            "poin" => $request->poin,
            "diskualifikasi" => $request->diskualifikasi,
            "jarak" => $request->jarak,
            "waktu" => $request->waktu,
            "modul_id" => $request->modul_id
        ]);
        return back()->with('update', 'update');
    }

    public function deletePenilaian(Request $request)
    {
        $delete = Penilaian::where('id', $request->id)->delete();
        return back()->with('delete', 'delete');
    }

}
