<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Juri;
use App\Modul;
use App\Rekap;
use Illuminate\Support\Str;
use App\User_penilaian;
use App\Penilaian;

class KategoriController extends Controller
{
    //

    public function index()
    {
        $kategori = Kategori::all();
        return view('Dashboard.kategori.index', compact('kategori'));
    }

    public function tambah(Request $request)
    {
        $nama = $request->name;
        $insert = Kategori::insert([
            'name' => $nama
        ]);
        return back()->with('sukses' , 'sukses');
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $nama = $request->name;
        $insert = Kategori::where('id' , $id)->update([
            'name' => $nama
        ]);
        return back()->with('update','update');
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $nama = $request->name;
        $insert = Kategori::where('id', $id)->delete();
        return back()->with('delete', 'delete');
    }

    public function report($id)
    {
        $ambil_juri_id = Juri::where('kategori_id' , $id)->get(['id']);
        $modul = Modul::all();
        return view('Dashboard.kategori.report' , compact('modul' ,'id'));
    }

    public function reportKategori($id, $modul_id, $year, $month , $tgl)
    {
        $array_bulan = array('Januari' , 'Februari' , 'Maret' , 'April' , 'Mei' , 'Juni' , 'Juli' , 'Agustus' , 'September' , 'Oktober' , 'November' , 'Desember');
        $array_rekap = array();
        $array_peserta = array();
        $modul = Modul::all();
        $nama_modul = Modul::find($modul_id);
        $data_kategori = Kategori::all();
        // $penilaian_id = Penilaian::where('modul_id', $modul_id)->get(['id']);
        $bulan =  $month;
        $tahun = $year;
        $rekap = Juri::where('kategori_id', $id)->where('modul_id', $modul_id)->whereMonth('tgl',$bulan)->whereYear('tgl' , $tahun)->whereDay('tgl' , $tgl)->get();
        if (sizeof($rekap) > 0) {
            # code...
            foreach ($rekap as $key => $value) {
                $rekap_peserta = Rekap::where('juri_id', $value->id)->orderBy('user_id')->get();
                $index_poin = 0;
                foreach($rekap_peserta as $index=> $item){
                    $poin = 0;
                    if($item->poin < 0){
                        $poin = 0;
                    }
                    else{
                        $poin = $item->poin;
                    }
                    $nama_peserta = "nama".$index++;
                    $poin_peserta = "poin".$index_poin++;
                    $rekap[$key][$nama_peserta] = preg_replace('/\s/', '', $item->user->name);
                    $rekap[$key][$poin_peserta] = $poin;
                    $rekap[$key]['jml_peserta'] = $index_poin;
                    if(in_array(preg_replace('/\s/', '', $item->user->name) , $array_peserta)){

                    }
                    else{
                        array_push($array_peserta , preg_replace('/\s/', '', $item->user->name));
                    }
                }
            }
            // return $array_peserta;
        }

        //======= Rekap total penilaian global ===== //
        $array_braking = array();
        $array_slalom = array();
        $array_lowSpeed = array();
        $array_total_global = array();
        $rekap_juri_braking = array();
        $rekap_juri_slalom = array();
        $rekap_juri_lowSpeed = array();
        $ambil_data_modul = Juri::where('kategori_id', $id)->whereMonth('tgl',$bulan)->whereYear('tgl' , $tahun)->whereDay('tgl' , $tgl)->get();

        foreach ($ambil_data_modul as  $item) {


            if(strcasecmp("braking" , $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_braking, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_braking[$key]['name'] =  $value->user->name;
                    $array_total_global[$key]['name'] =  $value->user->name;
                    $rekap_nilai_peserta = Rekap::whereIn('juri_id' , $rekap_juri_braking)->where('user_id' , $value->user_id)->orderBy('user_id')->sum('poin');

                    foreach($array_total_global as $index => $ab){
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_braking'] =  $rekap_nilai_peserta / count($rekap_juri_braking);
                        }
                    }

                }
            }
            else if( strcasecmp("slalom course", $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_slalom, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_slalom[$key]['name']=$value->user->name;
                     $array_total_global[$key]['name'] =  $value->user->name;
                    $rekap_nilai_peserta = Rekap::whereIn('juri_id', $rekap_juri_slalom)->where('user_id', $value->user_id)->orderBy('user_id')->sum('poin');

                    foreach ($array_total_global as $index => $ab) {
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_slalom'] = $rekap_nilai_peserta / count($rekap_juri_slalom) ;
                        }
                    }
                }
            }
            else if( strcasecmp("low speed balancing", $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_lowSpeed, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_lowSpeed[$key]['name'] =$value->user->name;
                    $array_total_global[$key]['name'] =  $value->user->name;

                    $rekap_nilai_peserta = Rekap::whereIn('juri_id', $rekap_juri_lowSpeed)->where('user_id', $value->user_id)->orderBy('user_id')->sum('poin');


                    foreach ($array_lowSpeed as $index => $ab) {
                        if ($array_lowSpeed[$index]['name'] == $value->user->name) {
                            //===== hitung penilaian per section =====//
                            $ambil_section = User_penilaian::whereIn('juri_id' ,$rekap_juri_lowSpeed)->where('user_id', $value->user_id)->orderBy('user_id')->get();

                            foreach ($ambil_section as $rs) {
                                for ($i=1; $i < 5 ; $i++) {
                                    $nama_section = "section".$i;
                                    if($rs->penilaian->coaching == $i){
                                        $array_penilaian = Penilaian::where('modul_id' , $item->modul->id)->where('coaching' , $i)->get(['id']);
                                        $rekap_section =  User_penilaian::whereIn('juri_id' , $rekap_juri_lowSpeed)->where('user_id', $value->user_id )->orderBy('user_id')->whereIn('penilaian_id' , $array_penilaian)->sum('nilai');
                                        $array_lowSpeed[$index][$nama_section] =  $rekap_section;
                                        $array_total_global[$index][$nama_section] =  $rekap_section;
                                    }
                                }
                            }
                            //==== end hitung penilaian per section ==== //
                            $array_lowSpeed[$index]['poin'] =  $rekap_nilai_peserta / count($rekap_juri_lowSpeed);
                        }
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_low_speed'] =  $rekap_nilai_peserta / count($rekap_juri_lowSpeed)   ;
                        }
                    }
                }
            }
        }

        //check all key from array total global wich is exist or not in array
        foreach ($array_total_global as $key => $value) {
            //check key poin braking exist or not
            if(array_key_exists('poin_braking',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key]['poin_braking'] = 0;
            }
            //check key poin slalom exists or not
            if(array_key_exists('poin_slalom',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key]['poin_slalom'] = 0;
            }
            //check key poin low speed exists or not
            if(array_key_exists( 'poin_low_speed',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key][ 'poin_low_speed'] = 0;
            }

            //check key poin section  1, 2 , ... n exists or not
            for ($i=1; $i < 5 ; $i++) {
                $nama_section = "section".$i;
                if(array_key_exists( $nama_section ,$array_total_global[$key])){

                }
                else{
                    $array_total_global[$key][$nama_section] = 0;
                }
            }
        }

        //====== End Rekap total penilaian global ==== //
        $kategori = Kategori::find($id);
        $token = Str::random(40);

        // return $array_bulan;

        return view('Dashboard.kategori.report-modul', compact('data_kategori' , 'modul' ,'rekap' ,'id' , 'nama_modul' ,'bulan' , 'array_bulan' , 'tahun' ,'kategori' ,'token' , 'tgl' , 'rekap_peserta' , 'array_rekap' , 'array_peserta' , 'array_braking' , 'array_slalom' , 'array_lowSpeed' , 'array_total_global'));

     }

    public function PublicReportKategori($token , $id, $modul_id, $year, $month , $tgl)
    {
        $array_bulan = array('Januari' , 'Februari' , 'Maret' , 'April' , 'Mei' , 'Juni' , 'Juli' , 'Agustus' , 'September' , 'Oktober' , 'November' , 'Desember');
        $array_rekap = array();
        $array_peserta = array();
        $modul = Modul::all();
        $nama_modul = Modul::find($modul_id);
        $data_kategori = Kategori::all();
        // $penilaian_id = Penilaian::where('modul_id', $modul_id)->get(['id']);
        $bulan =  $month;
        $tahun = $year;
        $rekap = Juri::where('kategori_id', $id)->where('modul_id', $modul_id)->whereMonth('tgl',$bulan)->whereYear('tgl' , $tahun)->whereDay('tgl' , $tgl)->get();
        if (sizeof($rekap) > 0) {
            # code...
            foreach ($rekap as $key => $value) {
                $rekap_peserta = Rekap::where('juri_id', $value->id)->orderBy('user_id')->get();
                $index_poin = 0;
                foreach($rekap_peserta as $index=> $item){
                    $poin = 0;
                    if($item->poin < 0){
                        $poin = 0;
                    }
                    else{
                        $poin = $item->poin;
                    }
                    $nama_peserta = "nama".$index++;
                    $poin_peserta = "poin".$index_poin++;
                    $rekap[$key][$nama_peserta] = preg_replace('/\s/', '', $item->user->name);
                    $rekap[$key][$poin_peserta] = $poin;
                    $rekap[$key]['jml_peserta'] = $index_poin;
                    if(in_array(preg_replace('/\s/', '', $item->user->name) , $array_peserta)){

                    }
                    else{
                        array_push($array_peserta , preg_replace('/\s/', '', $item->user->name));
                    }
                }
            }
            // return $array_peserta;
        }

        //======= Rekap total penilaian global ===== //
        $array_braking = array();
        $array_slalom = array();
        $array_lowSpeed = array();
        $array_total_global = array();
        $rekap_juri_braking = array();
        $rekap_juri_slalom = array();
        $rekap_juri_lowSpeed = array();
        $ambil_data_modul = Juri::where('kategori_id', $id)->whereMonth('tgl',$bulan)->whereYear('tgl' , $tahun)->whereDay('tgl' , $tgl)->get();

        foreach ($ambil_data_modul as  $item) {


            if(strcasecmp("braking" , $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_braking, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_braking[$key]['name'] =  $value->user->name;
                    $array_total_global[$key]['name'] =  $value->user->name;
                    $rekap_nilai_peserta = Rekap::whereIn('juri_id' , $rekap_juri_braking)->where('user_id' , $value->user_id)->orderBy('user_id')->sum('poin');

                    foreach($array_total_global as $index => $ab){
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_braking'] =  $rekap_nilai_peserta / count($rekap_juri_braking);
                        }
                    }

                }
            }
            else if( strcasecmp("slalom course", $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_slalom, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_slalom[$key]['name']=$value->user->name;
                     $array_total_global[$key]['name'] =  $value->user->name;
                    $rekap_nilai_peserta = Rekap::whereIn('juri_id', $rekap_juri_slalom)->where('user_id', $value->user_id)->orderBy('user_id')->sum('poin');

                    foreach ($array_total_global as $index => $ab) {
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_slalom'] = $rekap_nilai_peserta / count($rekap_juri_slalom) ;
                        }
                    }
                }
            }
            else if( strcasecmp("low speed balancing", $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_lowSpeed, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_lowSpeed[$key]['name'] =$value->user->name;
                    $array_total_global[$key]['name'] =  $value->user->name;

                    $rekap_nilai_peserta = Rekap::whereIn('juri_id', $rekap_juri_lowSpeed)->where('user_id', $value->user_id)->orderBy('user_id')->sum('poin');


                    foreach ($array_lowSpeed as $index => $ab) {
                        if ($array_lowSpeed[$index]['name'] == $value->user->name) {
                            //===== hitung penilaian per section =====//
                            $ambil_section = User_penilaian::whereIn('juri_id' ,$rekap_juri_lowSpeed)->where('user_id', $value->user_id)->orderBy('user_id')->get();

                            foreach ($ambil_section as $rs) {
                                for ($i=1; $i < 5 ; $i++) {
                                    $nama_section = "section".$i;
                                    if($rs->penilaian->coaching == $i){
                                        $array_penilaian = Penilaian::where('modul_id' , $item->modul->id)->where('coaching' , $i)->get(['id']);
                                        $rekap_section =  User_penilaian::whereIn('juri_id' , $rekap_juri_lowSpeed)->where('user_id', $value->user_id )->orderBy('user_id')->whereIn('penilaian_id' , $array_penilaian)->sum('nilai');
                                        $array_lowSpeed[$index][$nama_section] =  $rekap_section;
                                        $array_total_global[$index][$nama_section] =  $rekap_section;
                                    }
                                }
                            }
                            //==== end hitung penilaian per section ==== //
                            $array_lowSpeed[$index]['poin'] =  $rekap_nilai_peserta / count($rekap_juri_lowSpeed);
                        }
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_low_speed'] =  $rekap_nilai_peserta / count($rekap_juri_lowSpeed)   ;
                        }
                    }
                }
            }
        }

        //check all key from array total global wich is exist or not in array
        foreach ($array_total_global as $key => $value) {
            //check key poin braking exist or not
            if(array_key_exists('poin_braking',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key]['poin_braking'] = 0;
            }
            //check key poin slalom exists or not
            if(array_key_exists('poin_slalom',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key]['poin_slalom'] = 0;
            }
            //check key poin low speed exists or not
            if(array_key_exists( 'poin_low_speed',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key][ 'poin_low_speed'] = 0;
            }

            //check key poin section  1, 2 , ... n exists or not
            for ($i=1; $i < 5 ; $i++) {
                $nama_section = "section".$i;
                if(array_key_exists( $nama_section ,$array_total_global[$key])){

                }
                else{
                    $array_total_global[$key][$nama_section] = 0;
                }
            }
        }

        //====== End Rekap total penilaian global ==== //
        $kategori = Kategori::find($id);
        $token = Str::random(40);

        // return $array_bulan;
        return view('Dashboard.kategori.public-report-modul', compact('data_kategori' , 'modul' ,'rekap' ,'id' , 'nama_modul' ,'bulan' , 'array_bulan' , 'tahun' ,'kategori' ,'token' , 'tgl' , 'rekap_peserta' , 'array_rekap' , 'array_peserta' , 'array_braking' , 'array_slalom' , 'array_lowSpeed' , 'array_total_global'));

     }

    public function Ranking($id, $year, $month , $tgl)
    {
        $array_bulan = array('Januari' , 'Februari' , 'Maret' , 'April' , 'Mei' , 'Juni' , 'Juli' , 'Agustus' , 'September' , 'Oktober' , 'November' , 'Desember');
        $date = Juri::where('kategori_id' , $id)->distinct('tgl_calendar')->select(['tgl_calendar'])->get();
        $array_rekap = array();
        $array_peserta = array();
        $modul = Modul::all();
        $data_kategori = Kategori::all();
        // $penilaian_id = Penilaian::where('modul_id', $modul_id)->get(['id']);
        $bulan =  $month;
        $tahun = $year;

        //======= Rekap total penilaian global ===== //
        $array_braking = array();
        $array_slalom = array();
        $array_lowSpeed = array();
        $array_total_global = array();
        $rekap_juri_braking = array();
        $rekap_juri_slalom = array();
        $rekap_juri_lowSpeed = array();
        $ambil_data_modul = Juri::where('kategori_id', $id)->whereMonth('tgl',$bulan)->whereYear('tgl' , $tahun)->whereDay('tgl' , $tgl)->get();

        foreach ($ambil_data_modul as  $item) {


            if(strcasecmp("braking" , $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_braking, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_braking[$key]['name'] =  $value->user->name;
                    $array_total_global[$key]['name'] =  $value->user->name;
                    $rekap_nilai_peserta = Rekap::whereIn('juri_id' , $rekap_juri_braking)->where('user_id' , $value->user_id)->orderBy('user_id')->sum('poin');

                    foreach($array_total_global as $index => $ab){
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_braking'] =  $rekap_nilai_peserta / count($rekap_juri_braking);
                        }
                    }

                }
            }
            else if( strcasecmp("slalom course", $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_slalom, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_slalom[$key]['name']=$value->user->name;
                     $array_total_global[$key]['name'] =  $value->user->name;
                    $rekap_nilai_peserta = Rekap::whereIn('juri_id', $rekap_juri_slalom)->where('user_id', $value->user_id)->orderBy('user_id')->sum('poin');

                    foreach ($array_total_global as $index => $ab) {
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_slalom'] = $rekap_nilai_peserta / count($rekap_juri_slalom) ;
                        }
                    }
                }
            }
            else if( strcasecmp("low speed balancing", $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_lowSpeed, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_lowSpeed[$key]['name'] =$value->user->name;
                    $array_total_global[$key]['name'] =  $value->user->name;

                    $rekap_nilai_peserta = Rekap::whereIn('juri_id', $rekap_juri_lowSpeed)->where('user_id', $value->user_id)->orderBy('user_id')->sum('poin');


                    foreach ($array_lowSpeed as $index => $ab) {
                        if ($array_lowSpeed[$index]['name'] == $value->user->name) {
                            //===== hitung penilaian per section =====//
                            $ambil_section = User_penilaian::whereIn('juri_id' ,$rekap_juri_lowSpeed)->where('user_id', $value->user_id)->orderBy('user_id')->get();

                            foreach ($ambil_section as $rs) {
                                for ($i=1; $i < 5 ; $i++) {
                                    $nama_section = "section".$i;
                                    if($rs->penilaian->coaching == $i){
                                        $array_penilaian = Penilaian::where('modul_id' , $item->modul->id)->where('coaching' , $i)->get(['id']);
                                        $rekap_section =  User_penilaian::whereIn('juri_id' , $rekap_juri_lowSpeed)->where('user_id', $value->user_id )->orderBy('user_id')->whereIn('penilaian_id' , $array_penilaian)->sum('nilai');
                                        $array_lowSpeed[$index][$nama_section] =  $rekap_section;
                                        $array_total_global[$index][$nama_section] =  $rekap_section;
                                    }
                                }
                            }
                            //==== end hitung penilaian per section ==== //
                            $array_lowSpeed[$index]['poin'] =  $rekap_nilai_peserta / count($rekap_juri_lowSpeed);
                        }
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_low_speed'] =  $rekap_nilai_peserta / count($rekap_juri_lowSpeed)   ;
                        }
                    }
                }
            }
        }

        //check all key from array total global wich is exist or not in array
        foreach ($array_total_global as $key => $value) {
            //check key poin braking exist or not
            if(array_key_exists('poin_braking',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key]['poin_braking'] = 0;
            }
            //check key poin slalom exists or not
            if(array_key_exists('poin_slalom',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key]['poin_slalom'] = 0;
            }
            //check key poin low speed exists or not
            if(array_key_exists( 'poin_low_speed',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key][ 'poin_low_speed'] = 0;
            }

            //check key poin section  1, 2 , ... n exists or not
            for ($i=1; $i < 5 ; $i++) {
                $nama_section = "section".$i;
                if(array_key_exists( $nama_section ,$array_total_global[$key])){

                }
                else{
                    $array_total_global[$key][$nama_section] = 0;
                }
            }
        }

        //====== End Rekap total penilaian global ==== //
        $kategori = Kategori::find($id);
        $token = Str::random(40);

        // return $array_bulan;

        return view('Dashboard.kategori.ranking', compact('data_kategori' , 'modul' ,'rekap' ,'id' , 'nama_modul' ,'bulan' , 'array_bulan' , 'tahun' ,'kategori' ,'token' , 'tgl' , 'rekap_peserta' , 'array_rekap' , 'array_peserta' , 'array_braking' , 'array_slalom' , 'array_lowSpeed' , 'array_total_global' ,'date'));

     }

    public function publicRanking($token , $id, $year, $month , $tgl)
    {
        $array_bulan = array('Januari' , 'Februari' , 'Maret' , 'April' , 'Mei' , 'Juni' , 'Juli' , 'Agustus' , 'September' , 'Oktober' , 'November' , 'Desember');
        $array_rekap = array();
        $array_peserta = array();
        $modul = Modul::all();
        $data_kategori = Kategori::all();
        // $penilaian_id = Penilaian::where('modul_id', $modul_id)->get(['id']);
        $bulan =  $month;
        $tahun = $year;

        //======= Rekap total penilaian global ===== //
        $array_braking = array();
        $array_slalom = array();
        $array_lowSpeed = array();
        $array_total_global = array();
        $rekap_juri_braking = array();
        $rekap_juri_slalom = array();
        $rekap_juri_lowSpeed = array();
        $ambil_data_modul = Juri::where('kategori_id', $id)->whereMonth('tgl',$bulan)->whereYear('tgl' , $tahun)->whereDay('tgl' , $tgl)->get();

        foreach ($ambil_data_modul as  $item) {


            if(strcasecmp("braking" , $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_braking, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_braking[$key]['name'] =  $value->user->name;
                    $array_total_global[$key]['name'] =  $value->user->name;
                    $rekap_nilai_peserta = Rekap::whereIn('juri_id' , $rekap_juri_braking)->where('user_id' , $value->user_id)->orderBy('user_id')->sum('poin');

                    foreach($array_total_global as $index => $ab){
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_braking'] =  $rekap_nilai_peserta / count($rekap_juri_braking);
                        }
                    }

                }
            }
            else if( strcasecmp("slalom course", $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_slalom, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_slalom[$key]['name']=$value->user->name;
                     $array_total_global[$key]['name'] =  $value->user->name;
                    $rekap_nilai_peserta = Rekap::whereIn('juri_id', $rekap_juri_slalom)->where('user_id', $value->user_id)->orderBy('user_id')->sum('poin');

                    foreach ($array_total_global as $index => $ab) {
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_slalom'] = $rekap_nilai_peserta / count($rekap_juri_slalom) ;
                        }
                    }
                }
            }
            else if( strcasecmp("low speed balancing", $item->modul->name) == 0){
                $rekap_nilai = Rekap::where('juri_id', $item->id)->orderBy('user_id')->get();
                array_push($rekap_juri_lowSpeed, $item->id);
                foreach ($rekap_nilai as $key => $value) {
                    $array_lowSpeed[$key]['name'] =$value->user->name;
                    $array_total_global[$key]['name'] =  $value->user->name;

                    $rekap_nilai_peserta = Rekap::whereIn('juri_id', $rekap_juri_lowSpeed)->where('user_id', $value->user_id)->orderBy('user_id')->sum('poin');


                    foreach ($array_lowSpeed as $index => $ab) {
                        if ($array_lowSpeed[$index]['name'] == $value->user->name) {
                            //===== hitung penilaian per section =====//
                            $ambil_section = User_penilaian::whereIn('juri_id' ,$rekap_juri_lowSpeed)->where('user_id', $value->user_id)->orderBy('user_id')->get();

                            foreach ($ambil_section as $rs) {
                                for ($i=1; $i < 5 ; $i++) {
                                    $nama_section = "section".$i;
                                    if($rs->penilaian->coaching == $i){
                                        $array_penilaian = Penilaian::where('modul_id' , $item->modul->id)->where('coaching' , $i)->get(['id']);
                                        $rekap_section =  User_penilaian::whereIn('juri_id' , $rekap_juri_lowSpeed)->where('user_id', $value->user_id )->orderBy('user_id')->whereIn('penilaian_id' , $array_penilaian)->sum('nilai');
                                        $array_lowSpeed[$index][$nama_section] =  $rekap_section;
                                        $array_total_global[$index][$nama_section] =  $rekap_section;
                                    }
                                }
                            }
                            //==== end hitung penilaian per section ==== //
                            $array_lowSpeed[$index]['poin'] =  $rekap_nilai_peserta / count($rekap_juri_lowSpeed);
                        }
                        if($array_total_global[$index]['name'] == $value->user->name){
                            $array_total_global[$index]['poin_low_speed'] =  $rekap_nilai_peserta / count($rekap_juri_lowSpeed)   ;
                        }
                    }
                }
            }
        }

        //check all key from array total global wich is exist or not in array
        foreach ($array_total_global as $key => $value) {
            //check key poin braking exist or not
            if(array_key_exists('poin_braking',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key]['poin_braking'] = 0;
            }
            //check key poin slalom exists or not
            if(array_key_exists('poin_slalom',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key]['poin_slalom'] = 0;
            }
            //check key poin low speed exists or not
            if(array_key_exists( 'poin_low_speed',$array_total_global[$key])){

            }
            else{
                $array_total_global[$key][ 'poin_low_speed'] = 0;
            }

            //check key poin section  1, 2 , ... n exists or not
            for ($i=1; $i < 5 ; $i++) {
                $nama_section = "section".$i;
                if(array_key_exists( $nama_section ,$array_total_global[$key])){

                }
                else{
                    $array_total_global[$key][$nama_section] = 0;
                }
            }
        }

        //====== End Rekap total penilaian global ==== //
        $kategori = Kategori::find($id);
        // $token = Str::random(40);

        // return $array_bulan;

        return view('Dashboard.kategori.public-ranking', compact('data_kategori' , 'modul' ,'rekap' ,'id' , 'nama_modul' ,'bulan' , 'array_bulan' , 'tahun' ,'kategori' ,'token' , 'tgl' , 'rekap_peserta' , 'array_rekap' , 'array_peserta' , 'array_braking' , 'array_slalom' , 'array_lowSpeed' , 'array_total_global'));

     }



}
