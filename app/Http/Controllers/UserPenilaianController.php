<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penilaian;
use App\User_penilaian;
use App\User;
use App\Juri;
use App\Rekap;

class UserPenilaianController extends Controller
{
    //
    private function cekQueue($juri , $user)
    {
        $status_antrian = User_penilaian::where('juri_id', $juri)->where('user_id', $user)->distinct('status_penjurian')->get(['status_penjurian']);
        // return $status_antrian;
        if ($status_antrian[0]->status_penjurian == 0 || $status_antrian[0]->status_penjurian == null) {
            $update_antrian = User_penilaian::where('juri_id', $juri)->where('user_id', $user)->update(['status_penjurian' => 1]);
        }
        else{
            return back()->with('antrian' , 'antrian');
        }
    }

    public function tambah($tgl , $id ,$juri ,$poin)
    {
        // return $this->cekQueue($juri , $id);
        $nilai = User_penilaian::where('juri_id' , $juri)->where('user_id' , $id)->join('penilaians' , 'penilaians.id' , '=' , 'user_penilaians.penilaian_id')->orderBy('penilaians.poin' , 'ASC')->select('user_penilaians.*')->get();
        $user_name = User::find($id);
        $rekap = Rekap::where('juri_id' , $juri)->where('user_id' , $id)->first();
        $modul = Juri::find($juri);
        // foreach ($tampil_nilai as  $value) {
        //     if($value->penilaian->coaching  == "3"){
        //         return $value->penilaian->coaching;
        //     }
        // }
        if(strcasecmp("balancing" , $modul->modul->name) == 0)
        {
            return view('Dashboard.juri.tambah-nilai-balancing' , compact('nilai' , 'tgl' , 'juri' ,'user_name' , 'poin' ,'id' , 'rekap' , 'modul'));
        }
        else{

            return view('Dashboard.juri.tambah-nilai' , compact('nilai' , 'tgl' , 'juri' ,'user_name' , 'poin' ,'id' , 'rekap' , 'modul'));
        }


    }

   public function tambahNilai(Request $request)
   {
    //    dd($request->id);
        $tgl = $request->tgl;
        $date = date("Y-m-d", strtotime($tgl));
        $user = $request->user;
        $kecepatan = 0;
        if(isset($request->kecepatan)){
            $kecepatan = $request->kecepatan;
        }
        for ($i=0; $i < sizeof($request->id) ; $i++) {
            $update_nilai = User_penilaian::where('id', $request->id[$i])->update([
                'kesalahan' => $request->kesalahan[$i],
                'nilai' => $request->nilai[$i],
                'jarak' => $request->jarak[$i],
                'waktu' => $request->waktu[$i],
                'keterangan' => $request->keterangan[$i],
            ]);
        }
        $sum_nilai = User_penilaian::where('juri_id', $request->juri)->where('user_id', $user)->sum('nilai');
        $sum_jarak = User_penilaian::where('juri_id', $request->juri)->where('user_id', $user)->sum('jarak');
        $sum_waktu = User_penilaian::where('juri_id', $request->juri)->where('user_id', $user)->sum('waktu');
        $poin = 1000 - $sum_nilai;
        $juri = Juri::find($request->juri);
        $rekap = Rekap::where('user_id' , $user)->where('juri_id' , $request->juri)->first();
        if (strcasecmp('balancing' , $juri->modul->name) == 0) {
            # code...
            if( $rekap == null){
                $create_rekap = Rekap::insert([
                    'user_id' => $user,
                    'juri_id' => $request->juri,
                    'jarak' => $sum_jarak,
                    'waktu' => $request->time,
                    'poin' => $poin
                ]);
            } else{
                $update_rekap = Rekap::find($rekap->id)->update([
                    'user_id' => $user,
                    'juri_id' => $request->juri,
                    'jarak' => $sum_jarak,
                    'waktu' => $request->time,
                    'poin' => $poin
                ]);
            }
        }
        else {
            # code...
            if($rekap == null){
                $create_rekap = Rekap::insert([
                    'user_id' => $user,
                    'juri_id' => $request->juri,
                    'jarak' => $sum_jarak,
                    'waktu' => $sum_waktu,
                    'poin' => $poin,
                    'kecepatan' => $kecepatan
                ]);
            }
            else{
                $update_rekap = Rekap::find($rekap->id)->update([
                    'user_id' => $user,
                    'juri_id' => $request->juri,
                    'jarak' => $sum_jarak,
                    'waktu' => $sum_waktu,
                    'poin' => $poin,
                    'kecepatan' => $kecepatan
                ]);
            }
        }

        return redirect()->route('penjurian' , ['tgl' => $tgl , 'juri' => $request->juri])->with('sukses' ,'sukses');
        // return redirect()->route('tambah-nilai', ['tgl'=>$tgl , 'id' => $user , 'juri'=> $request->juri , 'poin' => $poin])->with('sukses', 'sukses');
        // return $sum_nilai;
   }

   public function lihatNilaiTanggal($tgl, $id, $juri, $poin)
   {
        $nilai = User_penilaian::where('juri_id', $juri)->where('user_id', $id)->with('user', 'penilaian')->get();
        $user_name = User::find($id);
        $nama_juri = Juri::find($juri);
        $rekap = Rekap::where('juri_id' , $juri)->where('user_id' , $id)->first();
        return view('Dashboard.juri.lihat-hasil', compact('nilai', 'tgl', 'juri', 'user_name', 'poin', 'id', 'nama_juri', 'rekap'));
    }

    public function PublicLihatNilaiTanggal($token , $tgl, $id, $juri, $poin)
    {
        $nilai = User_penilaian::where('juri_id', $juri)->where('user_id', $id)->with('user', 'penilaian')->get();
        $user_name = User::find($id);
        $nama_juri = Juri::find($juri);
        $rekap = Rekap::where('juri_id', $juri)->where('user_id', $id)->first();
        return view('Dashboard.juri.public-lihat-hasil', compact('nilai', 'tgl', 'juri', 'user_name', 'poin', 'id', 'nama_juri', 'rekap'));

   }

}
