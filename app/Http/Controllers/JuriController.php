<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penilaian;
use App\Juri;
use Illuminate\Support\Facades\Auth;
use App\User_penilaian;
use App\Rekap;
use Illuminate\Support\Carbon;
use App\Modul;
use App\Kategori;
use App\User;

class JuriController extends Controller
{
    //

    public function DaftarPenjurian($bulan , $tahun)
    {
        $array_bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $penjurian = Juri::whereMonth('tgl' , $bulan)->whereYear('tgl' , $tahun)->get();
        return view('Dashboard.user.peserta.lihat-penjurian' , compact('penjurian' , 'bulan' , 'tahun', 'array_bulan'));
    }

    public function BatalPenjurian($id , $modul)
    {
        $penjurian = Juri::where('id' , $id)->delete();
        // return redirect()->action('JuriController@Event' , ['tgl'=> date('Y-m-d H:i:s') ,  'modul' => $modul ]);
        return redirect()->route('home');
    }

    public function HapusPenjurian($id)
    {
        $penjurian = Juri::where('id' , $id)->delete();
        return back()->with('sukses' , 'sukses');
    }

    public function Event( $tgl , $id)
    {
        // return $request->kategori;
        $modul = Modul::find($id);
        $kategori = Kategori::all();
        // return $kategori;
        return view('Dashboard.juri.event', compact('kategori' , 'tgl', 'modul'));
    }

    public function LihatEvent($tgl, $id)
    {
        $modul = Modul::find($id);
        $kategori = Kategori::all();
        return view('Dashboard.juri.lihat-event', compact('kategori', 'tgl', 'modul'));

    }

    public function TambahEvent(Request $request)
    {
        $tgl = $request->tgl;
        $bulan = date("m", strtotime($tgl));
        $tahun = date("Y", strtotime($tgl));
        $date = date("d", strtotime($tgl));
        $modul = $request->modul;
        $kategori = $request->kategori;
        $peserta = User::where('kategori_id' , $kategori)->get();

        if(sizeof($peserta) == 0){
            return  back()->with('peserta' , 'peserta' );
        }

        $cek_count_juri = Juri::whereMonth('tgl' , $bulan)->whereYear('tgl' , $tahun)->whereDay('tgl' , $date)->where('kategori_id' , $kategori)->count();

        $juri = Juri::insert([
            'user_id' => Auth::user()->id,
            'deskripsi' => Modul::find($modul)->name." ".Kategori::find($kategori)->name ,
            'kategori_id' => $kategori,
            'modul_id' => $modul,
            'count' => $cek_count_juri+1,
            'tgl_calendar' => date('Y-m-d', strtotime($tgl)),
            'tgl' => $tgl
        ]);
        $juri_id = Juri::max('id');
        $penilaian = Penilaian::where('modul_id', $modul)->get();

        foreach($peserta as $value) {
            foreach($penilaian as $item) {
                $insert = User_penilaian::insert([
                    'user_id' => $value->id,
                    'penilaian_id' => $item->id,
                    'juri_id' => $juri_id,
                    'tgl' => $tgl
                ]);
            }
        }
        return redirect()->route('penjurian' , ['tgl' => $tgl , 'juri_id' => $juri_id ]);


    }

    public function LihatJuri(Request $request , $tgl, $modul_id)
    {
        $kategori = $request->kategori;
        $modul = Modul::find($modul_id);
        $juri = Juri::where('tgl_calendar' , $tgl)->where('modul_id' , $modul_id)->where('kategori_id' , $kategori)->get();

        return view( 'Dashboard.juri.lihat-juri', compact('juri', 'modul' ,'tgl'));
    }

    public function index($tgl , $id)
    {

        // $tanggal = date("d , F Y" , strtotime($tgl));
        $tanggal = $tgl;
        $date = date("d", strtotime($tgl));
        $month = date("m" , strtotime($tgl));
        $year = date("Y" , strtotime($tgl));

        $juri = Juri::find($id);
        $penilaian_selanjutnya = Juri::whereYear('tgl', $year)->whereMonth('tgl', $month)->whereDay('tgl', $date)->orderBy('tgl')->get();
        $count_juri = Juri::whereMonth('tgl', $month)->whereYear('tgl', $year)->whereDay('tgl' , $date)->where('kategori_id' , $juri->kategori_id)->where('modul_id', $juri->modul_id)->count();


        $nilai = User_penilaian::where('juri_id' , $id)->distinct('user_id')->get(['user_id']);
        $index = 0;
        $poindanjarak = "";
        $poindanwaktu = "";
        foreach ($nilai as $key => $value) {
            $sum_nilai = User_penilaian::where('juri_id', $id)->where('user_id' , $value->user_id)->sum('nilai');
            $sum_jarak = User_penilaian::where('juri_id', $id)->where('user_id', $value->user_id)->sum('jarak');
            $sum_waktu = User_penilaian::where('juri_id', $id)->where('user_id', $value->user_id)->max('waktu');
            $nilai[$key]['nilai'] = 1000 - $sum_nilai;
            $nilai[$key]['jarak'] = $sum_jarak;
            $nilai[$key]['waktu'] = $sum_waktu;
        }
        if( strcasecmp("braking", $juri->modul->name) == 0){
            $pointertinggi = Rekap::where('juri_id', $juri->id)->max('poin');
            $jarak = Rekap::where('juri_id', $juri->id)->min('jarak');

            if($jarak != null){

                $poindanjarak = Rekap::where('juri_id', $juri->id)->where('poin', $pointertinggi)->where('jarak', $jarak )->first();

                if($poindanjarak == null ){
                    $poin = Rekap::where('juri_id', $juri->id)->where('poin', $pointertinggi)->orderBy('jarak' , 'ASC')->get(['poin']);
                    $pointertinggi = Rekap::where('juri_id', $juri->id)->where('poin' , $pointertinggi)->orderBy('jarak' , 'ASC')->get();
                    if(sizeof($pointertinggi) > 1) {
                        $jarak = Rekap::where('juri_id', $juri->id)->where('jarak' ,'!=', $jarak)->whereIn('poin' , $poin)->min('jarak');
                        $poindanjarak = Rekap::where('juri_id', $juri->id)->where('poin' , $pointertinggi[0]->poin)->where('jarak' , $jarak)->first();
                    }
                    else {
                        $jarak = $pointertinggi[0]->jarak;
                        $poindanjarak = Rekap::where('juri_id', $juri->id)->where('poin', $pointertinggi[0]->poin)->first();
                    }

                    $rekap = Rekap::where('juri_id' , $juri->id)->whereNotIn('id' , [$poindanjarak->id])->get();
                    $penilaian_jarak = Penilaian::where('modul_id' , $juri->modul_id)->where('jarak' , 'yes')->first();
                    foreach ($rekap as $key => $value) {
                        if($jarak > $value->jarak){
                            // $selisih_jarak = $jarak - $value->jarak;
                            $selisih_jarak = 0;
                        }
                        else if($jarak < $value->jarak){
                            $selisih_jarak = $value->jarak - $jarak;
                        }
                        else if($jarak == $value->jarak){
                            $selisih_jarak = 0;
                        }
                        if(number_format($selisih_jarak, 2) >= 0.10)
                        {
                            $kesalahan = $selisih_jarak * 100;
                            $selisih_jarak = $selisih_jarak * 100 * $penilaian_jarak->poin;
                            $update_kesalahan = User_penilaian::where('juri_id' , $juri->id)->where('user_id' , $value->user_id)->where('penilaian_id' , $penilaian_jarak->id)->update([
                                'kesalahan' => ceil($kesalahan),
                                'nilai' => ceil($selisih_jarak),
                                ]);
                                $sum_total_nilai = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->sum('nilai');
                                $update_poin = Rekap::where('user_id' , $value->user_id)->where('juri_id' , $juri->id)->update(['poin' =>1000 - $sum_total_nilai ]);
                                // return ceil($selisih_jarak);
                            }
                            else{
                                    $kesalahan = 0;
                                    $selisih_jarak = 0;
                                    $update_kesalahan = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->where('penilaian_id', $penilaian_jarak->id)->update([
                                        'kesalahan' => ceil($kesalahan),
                                        'nilai' => ceil($selisih_jarak),
                                    ]);
                                    $sum_total_nilai = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->sum('nilai');
                                    $update_poin = Rekap::where('user_id', $value->user_id)->where('juri_id', $juri->id)->update(['poin' => 1000 - $sum_total_nilai]);
                            // return ceil($selisih_jarak)
                        }
                    }
                }
                else{
                    $rekap = Rekap::where('juri_id', $juri->id)->whereNotIn('id', [$poindanjarak->id])->get();
                    $penilaian_jarak = Penilaian::where('modul_id', $juri->modul_id)->where('jarak', 'yes')->first();
                    foreach ($rekap as $key => $value) {
                        if($jarak > $value->jarak){
                            // $selisih_jarak = $jarak - $value->jarak;
                            $selisih_jarak = 0;
                        }
                        else if($jarak < $value->jarak){
                            $selisih_jarak = $value->jarak - $jarak;
                        }
                        if (number_format($selisih_jarak, 2) >= 0.10) {
                            $kesalahan = $selisih_jarak * 100;
                            $selisih_jarak = $selisih_jarak * 100 * $penilaian_jarak->poin;
                            $update_kesalahan = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->where('penilaian_id', $penilaian_jarak->id)->update([
                                'kesalahan' => ceil($kesalahan),
                                'nilai' => ceil($selisih_jarak),
                            ]);
                            $sum_total_nilai = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->sum('nilai');
                            $update_poin = Rekap::where('user_id', $value->user_id)->where('juri_id', $juri->id)->update(['poin' => 1000 - $sum_total_nilai]);
                            // return ceil($selisih_jarak);
                        } else {
                            $kesalahan = 0;
                            $selisih_jarak = 0 ;
                            $update_kesalahan = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->where('penilaian_id', $penilaian_jarak->id)->update([
                                'kesalahan' => ceil($kesalahan),
                                'nilai' => ceil($selisih_jarak),
                            ]);
                            $sum_total_nilai = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->sum('nilai');
                            $update_poin = Rekap::where('user_id', $value->user_id)->where('juri_id', $juri->id)->update(['poin' => 1000 - $sum_total_nilai]);
                            // return ceil($selisih_jarak)
                        }
                    }

                }
            }

        }
        if(strcasecmp("slalom course", $juri->modul->name) == 0){
            $pointertinggi = Rekap::where('juri_id', $juri->id)->max('poin');
            $waktu = Rekap::where('juri_id', $juri->id)->min('waktu');

            if($waktu != null){
                $poindanwaktu = Rekap::where('juri_id', $juri->id)->where('poin', $pointertinggi)->where('waktu', $waktu)->first();
                if($poindanwaktu == null){
                    $poin = Rekap::where('juri_id', $juri->id)->where('poin', $pointertinggi)->orderBy('waktu', 'ASC')->get(['poin']);
                    $pointertinggi = Rekap::where('juri_id', $juri->id)->where('poin' , $pointertinggi)->orderBy('waktu' , 'ASC')->get();

                    if(sizeof($pointertinggi) > 1){
                        $waktu = Rekap::where('juri_id' , $juri->id)->where('waktu', '!=' , $waktu)->whereIn('poin' , $poin)->min('waktu');
                        $poindanwaktu = Rekap::where('juri_id' , $juri->id)->where('poin' , $pointertinggi[0]->poin)->where('waktu' , $waktu)->first();
                    }
                    else{
                        $waktu = $pointertinggi[0]->waktu;
                        $poindanwaktu = Rekap::where('juri_id', $juri->id)->where('poin', $pointertinggi[0]->poin)->where('waktu' , $waktu)->first();
                    }

                    $rekap = Rekap::where('juri_id', $juri->id)->whereNotIn('id', [$poindanwaktu->id])->get();
                    $penilaian_waktu = Penilaian::where('modul_id', $juri->modul_id)->where('waktu', 'yes')->first();
                    foreach ($rekap as $key => $value) {
                        $waktu_user = $value->waktu * 1000;
                        // $waktu_user = 91.76 * 1000;
                        $waktu_terkecil = $poindanwaktu->waktu * 1000;
                        if($waktu_terkecil > $waktu_user){
                            // $selisih_waktu = $waktu_terkecil - $waktu_user;
                            $selisih_waktu = 0;
                        }
                        else if($waktu_terkecil < $waktu_user){
                            $selisih_waktu = $waktu_user - $waktu_terkecil ;
                            // $selisih_waktu = $waktu_user - 0;
                        }
                        else if($waktu_terkecil == $waktu_user){
                            $selisih_waktu = 0;
                        }

                        if($selisih_waktu >= 100) {

                            $kesalahan = $selisih_waktu;
                            $selisih_waktu = $selisih_waktu * $penilaian_waktu->poin;

                            $update_kesalahan = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->where('penilaian_id', $penilaian_waktu->id)->update([
                                'kesalahan' => ceil($kesalahan),
                                'nilai' => ceil($selisih_waktu),
                            ]);
                            $sum_total_nilai = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->sum('nilai');
                            $update_poin = Rekap::where('user_id', $value->user_id)->where('juri_id', $juri->id)->update(['poin' =>1000 - $sum_total_nilai]);
                            // return c eil($selisih_jarak);
                        }
                         else{
                            $kesalahan = 0 * 100;
                             $selisih_waktu = 0 ;
                            $update_kesalahan = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->where('penilaian_id', $penilaian_waktu->id)->update([
                                'kesalahan' => ceil($kesalahan),
                                'nilai' => ceil($selisih_waktu),
                            ]);
                            $sum_total_nilai = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->sum('nilai');
                            $update_poin = Rekap::where('user_id', $value->user_id)->where('juri_id', $juri->id)->update(['poin' => 1000 - $sum_total_nilai]);
                                        // return ceil($selisih_jarak)
                            }
                    }
                }
                else{
                    $rekap = Rekap::where('juri_id', $juri->id)->whereNotIn('id', [$poindanwaktu->id])->get();
                    $penilaian_waktu = Penilaian::where('modul_id', $juri->modul_id)->where('waktu', 'yes')->first();
                    foreach ($rekap as $key => $value) {
                        $waktu_user = $value->waktu * 1000;
                        // $waktu_user = 91.76 * 1000;
                        $waktu_terkecil = $poindanwaktu->waktu * 1000;

                        if($waktu_terkecil > $waktu_user){
                            // $selisih_waktu = $waktu_terkecil - $waktu_user;
                            $selisih_waktu = 0;
                        }
                        else if($waktu_terkecil < $waktu_user){
                            $selisih_waktu = $waktu_user - $waktu_terkecil ;
                            // $selisih_waktu = $waktu_user - 0;
                        }

                        // return $selisih_waktu;
                        if($selisih_waktu >= 100) {

                            $kesalahan = $selisih_waktu;
                            $selisih_waktu = $selisih_waktu * $penilaian_waktu->poin;

                            $update_kesalahan = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->where('penilaian_id', $penilaian_waktu->id)->update([
                                'kesalahan' => ceil($kesalahan),
                                'nilai' => ceil($selisih_waktu),
                            ]);
                            $sum_total_nilai = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->sum('nilai');
                            $update_poin = Rekap::where('user_id', $value->user_id)->where('juri_id', $juri->id)->update(['poin' =>1000 - $sum_total_nilai]);
                            // return c eil($selisih_jarak);
                        }
                         else{
                            $kesalahan = 0 * 100;
                             $selisih_waktu = 0 ;
                            $update_kesalahan = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->where('penilaian_id', $penilaian_waktu->id)->update([
                                'kesalahan' => ceil($kesalahan),
                                'nilai' => ceil($selisih_waktu),
                            ]);
                            $sum_total_nilai = User_penilaian::where('juri_id', $juri->id)->where('user_id', $value->user_id)->sum('nilai');
                            $update_poin = Rekap::where('user_id', $value->user_id)->where('juri_id', $juri->id)->update(['poin' => 1000 - $sum_total_nilai]);
                                        // return ceil($selisih_jarak)
                            }
                    }

                }

            }

        }
        if(strcasecmp("balancing" , $juri->modul->name) == 0){
                $rekap = Rekap::where('juri_id' , $juri->id)->get();
                $poindanwaktu = 1;
                foreach ($rekap as $key => $value) {
                    foreach ($nilai as $key => $n) {
                        if($n->user_id == $value->user_id){
                            if($value->waktu < $juri->target_waktu){
                                $total_nilai_user = User_penilaian::where('juri_id', $id)->where('user_id' , $value->user_id)->sum('nilai');
                                $update_poin = 1000 - $total_nilai_user  - $juri->poin_pengurangan;
                                $nilai[$key]['nilai'] = $update_poin;
                                $update = Rekap::where('id' , $value->id)->update([
                                    'poin' => $update_poin
                                ]);
                            }
                            $nilai[$key]['waktu'] = $value->waktu;
                        }
                    }
                }
                // return $nilai;
                // return $rekap;
        }
        $kategori = Kategori::all();
        return view('Dashboard.juri.penjurian', compact('count_juri','kategori', 'nilai', 'tanggal' , 'juri' , 'tgl' , 'total_nilai' , 'poindanwaktu' , 'poindanjarak' ,'rekap', 'penilaian_selanjutnya','id'));
        // return $nilai;

    }

    public function tambahTargetWaktu(Request $request)
    {
        $id = $request->id;
        $update = Juri::where('id' , $id)->update([
            'target_waktu' => $request->target_waktu,
            'poin_pengurangan' => $request->poin_pengurangan,
        ]);
        return back();
    }

    public function tambahJuri(Request $request)
    {
        $error = 0;
        $ket_error = "";
        $tgl = $request->tgl;
        $waktu = $request->waktu.$tgl;
        // $date = Carbon::now()->toDateTimeString();//date("Y-m-d" , strtotime($tgl));
        $date = date( "Y-m-d H:i:s" , strtotime($waktu));
        for($m = 0; $m < sizeof($request->modul_id) ; $m++){
            $cek_juri_tgl = Juri::where('tgl' , $date)->where('modul_id' , $request->modul_id[$m])->first();
        }
        // return date( "Y-m-d H:i:s" , strtotime($waktu));
        // return date( 'Y-m-d H:i:s' , strtotime($tambah_satu_jam, strtotime($date)));
        // return Carbon::parse()->addHour();
        if(empty($request->peserta)){
            return back()->with('peserta' , 'peserta');
        }

        if(empty($cek_juri_tgl)){
            for ($m = 0; $m < sizeof($request->modul_id); $m++) {

                $penilaian = Penilaian::where('modul_id' , $request->modul_id[$m])->get();
                for ($j=1; $j <= $request->jml_event ; $j++) {
                    $interval = $j-1;
                    $tambah_satu_jam = "+".$interval. " hour";
                    if($j == 1){
                        $juri = Juri::insert([
                            'user_id' => Auth::user()->id,
                            'deskripsi' => $request->deskripsi.$j,
                            'kategori_id' => $request->kategori_id,
                            'modul_id' => $request->modul_id[$m],
                            'tgl' => $date
                            ]);
                            $juri_id = Juri::max('id');
                            for ($u=0; $u < sizeof($request->peserta); $u++) {
                                foreach ($penilaian as $item) {
                                    $insert = User_penilaian::insert([
                                        'user_id' => $request->peserta[$u],
                                        'penilaian_id' => $item->id,
                                        'juri_id' => $juri_id,
                                        'tgl' => $date
                                        ]);
                                    }
                                    // $rekap = Rekap::insert([
                                        //     'user_id' =>  $request->peserta[$u],
                                        //     'juri_id' => $juri_id,
                                        //     'jarak' => rand(10 , 60),
                                        //     'waktu' => 0,
                                        //     'poin' => rand(900 , 1001)
                                        // ]);
                                    }
                                }
                                else{
                                    $juri = Juri::insert([
                                        'user_id' => Auth::user()->id,
                                        'deskripsi' => $request->deskripsi.$j,
                                        'kategori_id' => $request->kategori_id,
                                        'modul_id' => $request->modul_id[$m],
                                        'tgl' => date('Y-m-d H:i:s', strtotime($tambah_satu_jam, strtotime($date)))
                                        ]);
                                        $juri_id = Juri::max('id');
                                        for ($i=0; $i < sizeof($request->peserta) ; $i++) {
                                            foreach ($penilaian as $item) {
                                                $insert = User_penilaian::insert([
                                                    'user_id' => $request->peserta[$i],
                                                    'penilaian_id' => $item->id,
                                                    'juri_id' => $juri_id,
                                                    'tgl' => date('Y-m-d H:i:s', strtotime($tambah_satu_jam, strtotime($date)))
                                                    ]);
                                                }

                                            }
                                        }
                                    }

            }
        }
        else{
            return back()->with('error' , 'error');
        }


        return back();
    }

    public function selesaiJuri(Request $request)
    {
        $update = Juri::where('id' , $request->id)->update([
            'selesai' => 1,
        ]);
        // return redirect()->route('penjurian' , ['tgl' => $tgl , 'juri_id' => $juri_id ])->route;('home');
        return back();
    }


}
