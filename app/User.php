<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;


class User extends Authenticatable
{
    use Notifiable;
    use CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'no_punggung' , 'name', 'email', 'password','gender', 'jabatan_id' , 'kategori_id'
    ];

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function juris()
    {
        return $this->hasMany('App\Juri');
    }

    public function usr_penilaians()
    {
        return $this->hasMany('App\User_penilaian');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const ADMIN_TYPE = '1';
    const DEFAULT_TYPE = '0';

    public function isAdmin()
    {
        return $this->is_admin === 1;
    }
}
