<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modul extends Model
{
    //
    protected $fillable = [
        'name'
    ];

    public function juris()
    {
        return $this->hasMany('App\Juri');
    }

    public function penilaians()
    {
        return $this->hasMany('App\Penilaian');
    }

}
