@component('mail::message')
# Introduction


@component('mail::button', ['url' => 'http://mpmsr.site/'])

Proses Login
@endcomponent
Password Anda : {{$password}}

<p style="color:red">Penting !!! Ubah Password Anda Setelah Anda Login Untuk Pertama kali</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
