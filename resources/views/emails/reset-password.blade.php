@component('mail::message')
# Introduction


@component('mail::button', ['url' => 'http://mpmsr.site/reset-password/'.$token])

Proses Ubah Password
@endcomponent
{{-- Password Anda : {{$token}} --}}

<p style="color:red">Penting !!! Jika anda tidak merasa menekan tombol lupa password , abaikan pesan ini</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
