@extends('layouts.dashboard')

@section('content')
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">

						<!-- BEGIN: Subheader -->
						<div class="m-subheader ">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title ">My Profile</h3>
								</div>
								{{-- <div>
									<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
										<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
											<i class="la la-plus m--hide"></i>
											<i class="la la-ellipsis-h"></i>
										</a>
										<div class="m-dropdown__wrapper">
											<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
											<div class="m-dropdown__inner">
												<div class="m-dropdown__body">
													<div class="m-dropdown__content">
														<ul class="m-nav">
															<li class="m-nav__section m-nav__section--first m--hide">
																<span class="m-nav__section-text">Quick Actions</span>
															</li>
															<li class="m-nav__item">
																<a href="" class="m-nav__link">
																	<i class="m-nav__link-icon flaticon-share"></i>
																	<span class="m-nav__link-text">Activity</span>
																</a>
															</li>
															<li class="m-nav__item">
																<a href="" class="m-nav__link">
																	<i class="m-nav__link-icon flaticon-chat-1"></i>
																	<span class="m-nav__link-text">Messages</span>
																</a>
															</li>
															<li class="m-nav__item">
																<a href="" class="m-nav__link">
																	<i class="m-nav__link-icon flaticon-info"></i>
																	<span class="m-nav__link-text">FAQ</span>
																</a>
															</li>
															<li class="m-nav__item">
																<a href="" class="m-nav__link">
																	<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																	<span class="m-nav__link-text">Support</span>
																</a>
															</li>
															<li class="m-nav__separator m-nav__separator--fit">
															</li>
															<li class="m-nav__item">
																<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div> --}}
							</div>
						</div>

						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								{{-- <div class="col-lg-4">
									<div class="m-portlet m-portlet--full-height  ">
										<div class="m-portlet__body">
											<div class="m-card-profile">
												<div class="m-card-profile__title m--hide">
													Your Profile
												</div>
												<div class="m-card-profile__pic">
													<div class="m-card-profile__pic-wrapper">
														<img src="assets/app/media/img/users/user4.jpg" alt="" />
													</div>
												</div>
												<div class="m-card-profile__details">
													<span class="m-card-profile__name">Mark Andre</span>
													<a href="" class="m-card-profile__email m-link">mark.andre@gmail.com</a>
												</div>
											</div>
											<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
												<li class="m-nav__separator m-nav__separator--fit"></li>
												<li class="m-nav__section m--hide">
													<span class="m-nav__section-text">Section</span>
												</li>
												<li class="m-nav__item">
													<a href="header/profile&amp;demo=default.html" class="m-nav__link">
														<i class="m-nav__link-icon flaticon-profile-1"></i>
														<span class="m-nav__link-title">
															<span class="m-nav__link-wrap">
																<span class="m-nav__link-text">My Profile</span>
																<span class="m-nav__link-badge"><span class="m-badge m-badge--success">2</span></span>
															</span>
														</span>
													</a>
												</li>
												<li class="m-nav__item">
													<a href="header/profile&amp;demo=default.html" class="m-nav__link">
														<i class="m-nav__link-icon flaticon-share"></i>
														<span class="m-nav__link-text">Activity</span>
													</a>
												</li>
												<li class="m-nav__item">
													<a href="header/profile&amp;demo=default.html" class="m-nav__link">
														<i class="m-nav__link-icon flaticon-chat-1"></i>
														<span class="m-nav__link-text">Messages</span>
													</a>
												</li>
												<li class="m-nav__item">
													<a href="header/profile&amp;demo=default.html" class="m-nav__link">
														<i class="m-nav__link-icon flaticon-graphic-2"></i>
														<span class="m-nav__link-text">Sales</span>
													</a>
												</li>
												<li class="m-nav__item">
													<a href="header/profile&amp;demo=default.html" class="m-nav__link">
														<i class="m-nav__link-icon flaticon-time-3"></i>
														<span class="m-nav__link-text">Events</span>
													</a>
												</li>
												<li class="m-nav__item">
													<a href="header/profile&amp;demo=default.html" class="m-nav__link">
														<i class="m-nav__link-icon flaticon-lifebuoy"></i>
														<span class="m-nav__link-text">Support</span>
													</a>
												</li>
											</ul>
											<div class="m-portlet__body-separator"></div>
											<div class="m-widget1 m-widget1--paddingless">
												<div class="m-widget1__item">
													<div class="row m-row--no-padding align-items-center">
														<div class="col">
															<h3 class="m-widget1__title">Member Profit</h3>
															<span class="m-widget1__desc">Awerage Weekly Profit</span>
														</div>
														<div class="col m--align-right">
															<span class="m-widget1__number m--font-brand">+$17,800</span>
														</div>
													</div>
												</div>
												<div class="m-widget1__item">
													<div class="row m-row--no-padding align-items-center">
														<div class="col">
															<h3 class="m-widget1__title">Orders</h3>
															<span class="m-widget1__desc">Weekly Customer Orders</span>
														</div>
														<div class="col m--align-right">
															<span class="m-widget1__number m--font-danger">+1,800</span>
														</div>
													</div>
												</div>
												<div class="m-widget1__item">
													<div class="row m-row--no-padding align-items-center">
														<div class="col">
															<h3 class="m-widget1__title">Issue Reports</h3>
															<span class="m-widget1__desc">System bugs and issues</span>
														</div>
														<div class="col m--align-right">
															<span class="m-widget1__number m--font-success">-27,49%</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div> --}}
								<div class="col-lg-12">
									<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
										<div class="m-portlet__head">
											<div class="m-portlet__head-tools">
												<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
															<i class="flaticon-share m--hide"></i>
															Ubah Profile
														</a>
													</li>
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
															Ubah Password
														</a>
													</li>
													{{--  <li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
															Settings
														</a>
													</li>  --}}
												</ul>
											</div>
											<div class="m-portlet__head-tools">
												<ul class="m-portlet__nav">

												</ul>
											</div>
										</div>
										<div class="tab-content">
											<div class="tab-pane active" id="m_user_profile_tab_1">
                                                <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{url('/ubah-profile')}}">
                                                    @csrf
													<div class="m-portlet__body">
														<div class="form-group m-form__group m--margin-top-10 m--hide">
															<div class="alert m-alert m-alert--default" role="alert">
																The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
															</div>
														</div>
														<div class="form-group m-form__group row">
															<div class="col-10 ml-auto">
																<h3 class="m-form__section">1. Personal Details</h3>
															</div>
                                                        </div>
                                                        <input type="hidden" name="id" value="{{Auth::user()->id}}">
														<div class="form-group m-form__group row">
															<label for="example-text-input" class="col-2 col-form-label">Full Name</label>
															<div class="col-7">
																<input class="form-control m-input" type="text" value="{{Auth::user()->name}}" name="name" required>
															</div>
														</div>
														<div class="form-group m-form__group row">
															<label for="example-text-input" class="col-2 col-form-label">No. Punggung</label>
															<div class="col-7">
																<input class="form-control m-input" type="text" value="{{Auth::user()->no_punggung}}" name="no_punggung">
															</div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
															<label for="example-text-input" class="col-2 col-form-label">Jabatan</label>
															<div class="col-7">
                                                                <select name="jabatan" class="form-control m-input" id="">
                                                                    @foreach ($jabatan as $item)
                                                                        @if ($item->id == Auth::user()->jabatan_id)
                                                                            <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                                                        @else
                                                                            @if(Auth::user()->jabatan_id == 1)
                                                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                                                            @else
                                                                                @if($item->id == 1)
                                                                                @else
                                                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                                                                @endif
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                </select>
															</div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
															<label for="example-text-input" class="col-2 col-form-label">Kategori</label>
															<div class="col-7">
                                                                <select name="kategori" class="form-control m-input" id="">
                                                                    @foreach ($kategori as $item)
                                                                        @if ($item->id == Auth::user()->kategori_id)
                                                                            <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                                                        @else
                                                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
															</div>
														</div>
														<div class="form-group m-form__group row">
															<label for="example-text-input" class="col-2 col-form-label">Gender</label>
															<div class="col-7">
																<input class="form-control m-input" type="text" value="{{Auth::user()->gender}}" name="gender" required>
																<span class="m-form__help">Isi gender dengan 'Pria' atau 'Wanita'.</span>
															</div>
														</div>
														<div class="form-group m-form__group row">
															<label for="example-text-input" class="col-2 col-form-label">E-mail</label>
															<div class="col-7">
																<input class="form-control m-input" type="text" name="email" value="{{Auth::user()->email}}" readonly>
															</div>
														</div>
													</div>
													<div class="m-portlet__foot m-portlet__foot--fit">
														<div class="m-form__actions">
															<div class="row">
																<div class="col-2">
																</div>
																<div class="col-7">
																	<button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">Save changes</button>&nbsp;&nbsp;
																</div>
															</div>
														</div>
													</div>
												</form>
											</div>
											<div class="tab-pane " id="m_user_profile_tab_2">
												<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{url('ubah-password')}}">
                                                    @csrf
													<div class="m-portlet__body">
														<div class="form-group m-form__group m--margin-top-10 m--hide">
															<div class="alert m-alert m-alert--default" role="alert">
																The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
															</div>
														</div>
														<div class="form-group m-form__group row">
															<div class="col-10 ml-auto">
																<h3 class="m-form__section">Ubah Password</h3>
															</div>
														</div>
														<div class="form-group m-form__group row">
															<label for="example-text-input" class="col-2 col-form-label">Password Lama</label>
															<div class="col-7">
																<input class="form-control m-input " id="password_lama" type="password"  placeholder="masukkan password lama anda" required>
															</div>
														</div>
														<div class="form-group m-form__group row">
															<label for="example-text-input" class="col-2 col-form-label">Password Baru</label>
															<div class="col-7">
                                                                <input class="form-control m-input password_baru" type="password" name="password" placeholder="masukkan password baru anda" required>
																<span class="m-form__help">Gunakan kombinasi angka , huruf kecil , huruf kapital , dan simbol agar lebih aman.</span>
															</div>
														</div>
														<div class="form-group m-form__group row">
															<label for="example-text-input" class="col-2 col-form-label">Ulangi Password</label>
															<div class="col-7">
																<input class="form-control m-input ulangi_password" type="password" required>
															</div>
														</div>
													</div>
													<div class="m-portlet__foot m-portlet__foot--fit">
														<div class="m-form__actions">
															<div class="row">
																<div class="col-2">
																</div>
																<div class="col-7">
																	<button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">Ubah Password</button>&nbsp;&nbsp;
																</div>
															</div>
														</div>
													</div>
												</form>
											</div>
											<div class="tab-pane " id="m_user_profile_tab_3">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script>
    $(function(){
        var password_baru = "";
        $('.password_baru').change(function(){
            password_baru = $(this).val();
        });
        $('.ulangi_password').change(function(){
            let ulangi_password = $(this).val();
            if(ulangi_password != password_baru  ){
                $('.ulangi_password').val("");
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Password yang anda masukkan tidak sama !  ");
                }

            });

        $('#password_lama').change(function(){
            let password_lama = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('/cek-password')}}" + "/" + {{Auth::user()->id}} + "/" + password_lama ,
                success:function(data) {
                   if(data != "true")
                   {
                       $('#password_lama').val("");
                        $('.password_baru').val("");
                        $('.ulangi_password').val("");
                       toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": false,
                        "positionClass": "toast-top-full-width",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                        };

                        toastr.error("Password yang anda masukkan salah !  ");
                    }
                    else{

                        $('.password_baru').change(function(){
                            password_baru = $(this).val();
                        });
                        $('.ulangi_password').change(function(){
                            let ulangi_password = $(this).val();
                            if(ulangi_password != password_baru  ){
                                $('.ulangi_password').val("");
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "newestOnTop": true,
                                    "progressBar": false,
                                    "positionClass": "toast-top-full-width",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                    };

                                    toastr.error("Password yang anda masukkan tidak sama !  ");

                            }
                        })
                    }

                }
            });
        })

     @if (session('sukses'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah data profil ");
        @endif
     @if (session('pass'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah password ");
        @endif


    })
</script>
@endsection
