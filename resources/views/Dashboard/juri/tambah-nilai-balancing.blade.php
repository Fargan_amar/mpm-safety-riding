@extends('layouts.dashboard')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            {{-- <h3 class="m-subheader__title ">Data Penjurian {{$tgl}}</h3> --}}
                        </div>

                    </div>
                </div>

                <!-- END: Subheader -->
                    <div class="m-content">
								{{-- <div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Poin User : <span class="poin_user"></span>
												</h3>
											</div>
										</div>
									</div>

									<!--begin::Form-->
									<form class="m-form m-form--label-align-center">
										<div class="m-portlet__body">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">Coaching Skill 1</h3>
                                                </div>
                                            @foreach ($nilai as $item)
                                                @if ($item->penilaian->coaching == "1")
                                                    @if ($item->penilaian->diskualifikasi == "no")

                                                    <div class="form-group m-form__group row">
                                                        <input type="hidden" name="id[]" value="{{$item->id}}">
                                                        <div class="col-lg-3">
                                                            <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input id="m_touchspin_1" type="text" class="form-control m-input allownumericwithdecimal kesalahan{{$item->id}}"  name="kesalahan[]" placeholder="Jumlah Kesalahan" onchange="hitung_nilai(this.value,'{{$item->id}}')">
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <input type="text" class="form-control m-input nilai-{{$item->id}}"  placeholder="nilai" readonly >
                                                            <input type="hidden" class="last_nilai_{{$item->id}}" value="0">
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endif
                                                @endforeach

                                            </div>
                                            <div class="m-form__seperator m-form__seperator--dashed"></div>
											<div class="m-form__section m-form__section--last">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">Coaching Skill 2</h3>
                                                </div>
                                            @foreach ($nilai as $item)
                                                @if ($item->penilaian->coaching == "2")
                                                    @if ($item->penilaian->diskualifikasi == "no")

                                                    <div class="form-group m-form__group row">
                                                        <input type="hidden" name="id[]" value="{{$item->id}}">
                                                        <div class="col-lg-3">
                                                            <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input id="m_touchspin_1" type="text" class="form-control m-input allownumericwithdecimal kesalahan{{$item->id}}"  name="kesalahan[]" placeholder="Jumlah Kesalahan" onchange="hitung_nilai(this.value,'{{$item->id}}')">
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <input type="text" class="form-control m-input nilai-{{$item->id}}"  placeholder="nilai" readonly >
                                                            <input type="hidden" class="last_nilai_{{$item->id}}" value="0">
                                                        </div>
                                                    </div>
                                                    @endif
                                              @endif
                                            @endforeach
                                            </div>
                                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                                            <div class="m-form__section m-form__section--last">
                                                <div class="m-form__heading">
                                                    <h3 class="m-form__heading-title">Coaching Skill 3</h3>
                                                </div>
                                            @foreach ($nilai as $item)
                                                @if ($item->penilaian->coaching == "3")
                                                    @if ($item->penilaian->diskualifikasi == "no")

                                                    <div class="form-group m-form__group row">
                                                        <input type="hidden" name="id[]" value="{{$item->id}}">
                                                        <div class="col-lg-3">
                                                            <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input id="m_touchspin_1" type="text" class="form-control m-input allownumericwithdecimal kesalahan{{$item->id}}"  name="kesalahan[]" placeholder="Jumlah Kesalahan" onchange="hitung_nilai(this.value,'{{$item->id}}')">
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <input type="text" class="form-control m-input nilai-{{$item->id}}"  placeholder="nilai" readonly >
                                                            <input type="hidden" class="last_nilai_{{$item->id}}" value="0">
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endif
                                            @endforeach
                                            </div>
                                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                                            <div class="m-form__section m-form__section--last">
                                                <div class="m-form__heading">
                                                    <h3 class="m-form__heading-title">Diskualifikasi</h3>
                                                </div>
                                            @foreach ($nilai as $item)
                                                    @if ($item->penilaian->diskualifikasi == "yes")

                                                    <div class="form-group m-form__group row">
                                                        <input type="hidden" name="id[]" value="{{$item->id}}">
                                                        <div class="col-lg-3">
                                                            <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input id="m_touchspin_2" type="text" class="form-control m-input allownumericwithdecimal kesalahan{{$item->id}}"  name="kesalahan[]" placeholder="Jumlah Kesalahan" onchange="hitung_nilai(this.value,'{{$item->id}}')">
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <input type="text" class="form-control m-input nilai-{{$item->id}}"  placeholder="nilai" readonly >
                                                            <input type="hidden" class="last_nilai_{{$item->id}}" value="0">
                                                        </div>
                                                    </div>
                                                    @endif

                                            @endforeach
                                            </div>
                                        </div>



										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions">
												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-6">
														<button type="reset" class="btn btn-primary">Submit</button>
														<button type="reset" class="btn btn-secondary">Cancel</button>
													</div>
												</div>
											</div>
										</div>
									</form>

									<!--end::Form-->
								</div> --}}
                            </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
            </div>
        </div>


<!--begin::Modal-->
        <div class="modal fade" id="modal_tambah_event" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-center" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Peserta : {{$user_name->name}} - {{$user_name->kategori->name}} <br> Poin : <span class="poin_user"></span>
                            <span><button type="submit" class="btn btn-primary btn-sm reset_nilai">Reset Nilai</button></span>
                        </h5>
                        {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button> --}}
                    </div>
                    <form action="{{url('/tambah-user-nilai')}}" class="simpan-nilai" method="POST">
                        @csrf
                    <div class="modal-body">
                    <div class="alert alert-info"  role="alert">
                        <strong>Penting !</strong> Simpan Data penilaian agar data-data penilaian yang telah anda inputkan tersimpan didalam database .
                    </div>
                        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="500">
                            <div class="m-form__section m-form__section--first">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title">Coaching Skill 1</h3>
                                </div>
                            @foreach ($nilai as $item)
                                @if ($item->penilaian->coaching == "1")
                                    @if ($item->penilaian->diskualifikasi == "no")
                                        @if ($item->penilaian->jarak == "yes")

                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">Penilaian Jarak</h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <input type="hidden" name="id[]" value="{{$item->id}}">
                                                <input type="hidden" name="kesalahan[]" value="{{$item->kesalahan}}">
                                                <input type="hidden" name="waktu[]" value="{{$item->waktu}}">
                                                <input type="hidden" name="nilai[]" value="{{$item->nilai}}">

                                                <div class="col-lg-3">
                                                    <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                </div>
                                                <div class="col-lg-4">
                                                    <input  type="text" class="form-control m-input allownumericwithdecimal "  name="jarak[]" value="{{$item->jarak}}" placeholder="Jarak Pengereman dlm meter" >
                                                </div>
                                                {{-- <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}" readonly >
                                                    <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                                </div> --}}
                                                <div class="col-lg-2">
                                                    <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3"  placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                                </div>

                                            </div>
                                        @endif
                                        @if ($item->penilaian->waktu == "yes")

                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">Penilaian Waktu</h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <input type="hidden" name="id[]" value="{{$item->id}}">
                                                <input type="hidden" name="kesalahan[]" value="{{$item->kesalahan}}">
                                                <input type="hidden" name="jarak[]" value="{{$item->jarak}}">
                                                <input type="hidden" name="nilai[]" value="{{$item->nilai}}">

                                                <div class="col-lg-3">
                                                    <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                </div>
                                                <div class="col-lg-4">
                                                    <input  type="text" class="form-control m-input  allownumericwithdecimal"  name="waktu[]" value="{{$item->waktu}}" placeholder="Waktu tempuh" >
                                                </div>
                                                {{-- <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}" readonly >
                                                    <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                                </div> --}}
                                                <div class="col-lg-2">
                                                    <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3"  placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                                </div>

                                            </div>
                                        @endif
                                        @if ($item->penilaian->jarak == "no" && $item->penilaian->waktu == "no" )

                                    <div class="form-group m-form__group row">
                                        <input type="hidden" name="id[]" value="{{$item->id}}">
                                        <input type="hidden" name="jarak[]" value="{{$item->jarak}}">
                                        <input type="hidden" name="waktu[]" value="{{$item->waktu}}">

                                        <div class="col-lg-3">
                                            <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                        </div>
                                        <div class="col-lg-3">
                                            <input id="m_touchspin_1" type="text" class="form-control m-input allownumericwithdecimal kesalahan{{$item->id}}"  name="kesalahan[]" value="{{$item->kesalahan}}" data-max="{{ceil(1000 / $item->penilaian->poin)}}" placeholder="0"  onchange="hitung_nilai(this.value,'{{$item->id}}')">
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}"  readonly >
                                            <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                        </div>
                                        <div class="col-lg-2">
                                            <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3" placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                        </div>

                                    </div>
                                    @endif
                                    @endif
                                    @endif
                                @endforeach

                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section m-form__section--last">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title">Coaching Skill 2</h3>
                                </div>
                                <input type="hidden" name="tgl" value="{{$tgl}}">
                                <input type="hidden" name="juri" value={{$juri}}>
                                <input type="hidden" name="user" value={{$id}}>
                            @foreach ($nilai as $item)
                                @php $waktu = $item->waktu    @endphp
                                @if ($item->penilaian->coaching == "2")
                                    @if ($item->penilaian->diskualifikasi == "no")

                                        @if ($item->penilaian->jarak == "yes")

                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">Penilaian Jarak</h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <input type="hidden" name="id[]" value="{{$item->id}}">
                                                <input type="hidden" name="kesalahan[]" value="{{$item->kesalahan}}">
                                                <input type="hidden" name="waktu[]" value="{{$item->waktu}}">
                                                <input type="hidden" name="nilai[]" value="{{$item->nilai}}">

                                                <div class="col-lg-3">
                                                    <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                </div>
                                                <div class="col-lg-4">
                                                    <input  type="text" class="form-control m-input allownumericwithdecimal "  name="jarak[]" value="{{$item->jarak}}" placeholder="Jarak Pengereman dlm meter" >
                                                </div>
                                                {{-- <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}" readonly >
                                                    <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                                </div> --}}
                                                <div class="col-lg-2">
                                                    <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3"  placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                                </div>

                                            </div>
                                        @endif
                                        @if ($item->penilaian->waktu == "yes")

                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">Penilaian Waktu</h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <input type="hidden" name="id[]" value="{{$item->id}}">
                                                <input type="hidden" name="kesalahan[]" value="{{$item->kesalahan}}">
                                                <input type="hidden" name="jarak[]" value="{{$item->jarak}}">
                                                <input type="hidden" name="nilai[]" value="{{$item->nilai}}">

                                                <div class="col-lg-3">
                                                    <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                </div>
                                                <div class="col-lg-4">
                                                    <input  type="text" class="form-control m-input  allownumericwithdecimal " name="waktu[]" value="{{$item->waktu}}"placeholder="Waktu tempuh"  >
                                                </div>
                                                {{-- <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}" readonly >
                                                    <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                                </div> --}}
                                                <div class="col-lg-2">
                                                    <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3"  placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                                </div>

                                            </div>
                                        @endif
                                        @if ($item->penilaian->jarak == "no" && $item->penilaian->waktu == "no" )

                                    <div class="form-group m-form__group row">
                                        <input type="hidden" name="id[]" value="{{$item->id}}">
                                        <input type="hidden" name="jarak[]" value="{{$item->jarak}}">
                                        <input type="hidden" name="waktu[]" value="{{$item->waktu}}">

                                        <div class="col-lg-3">
                                            <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                        </div>
                                        <div class="col-lg-3">
                                            <input id="m_touchspin_1" type="text" class="form-control m-input allownumericwithdecimal kesalahan{{$item->id}}"  name="kesalahan[]" value="{{$item->kesalahan}}" placeholder="0" data-max="{{ceil(1000 / $item->penilaian->poin)}}" onchange="hitung_nilai(this.value,'{{$item->id}}')">
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}" readonly >
                                            <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                        </div>
                                        <div class="col-lg-2">
                                            <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3" placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                        </div>

                                    </div>
                                    @endif
                                    @endif
                                @endif
                            @endforeach
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section m-form__section--last">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title">Coaching Skill 3</h3>
                                </div>
                                        @if (strcasecmp("balancing" , $modul->modul->name )  == "0")
                                            @if (!empty($modul->target_waktu))

                                        {{--  <input type="hidden" name="jarak[]" value="{{$item->jarak}}">
                                        <input type="hidden" name="waktu[]" value="{{$item->waktu}}">
                                        <input type="hidden" name="kesalahan[]" value="{{$item->kesalahan}}">  --}}
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">Penilaian Waktu</h3>
                                            </div>
                                            <div class="form-group m-form__group row">

                                                <div class="col-lg-3">
                                                    <textarea class="form-control m-input" name="" id="exampleFormControlTextarea1" rows="3" readonly >Waktu</textarea>
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input " value="{{$modul->poin_pengurangan}}" readonly>
                                                </div>
                                                <div class="col-lg-2">
                                                    <input  type="text" class="form-control m-input allownumericwithdecimal " name="" value="{{$modul->target_waktu}}" placeholder="Waktu tempuh" readonly >
                                                </div>
                                                @if ($rekap == null)
                                               <div class="col-lg-4">
                                                    <input  type="text" class="form-control m-input allownumericwithdecimal " name="time" value="0" placeholder="Waktu tempuh" >
                                                </div>

                                                @else
                                                <div class="col-lg-4">
                                                    <input  type="text" class="form-control m-input allownumericwithdecimal " name="time" value="{{$rekap->waktu}}" placeholder="Waktu tempuh" >
                                                </div>

                                                @endif
                                                {{-- <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}" readonly >
                                                    <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                                </div> --}}
                                                {{--  <div class="col-lg-2">
                                                    <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3"  placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                                </div>  --}}

                                            </div>
                                            @endif
                                        @endif
                            @foreach ($nilai as $item)
                                @if ($item->penilaian->coaching == "3")

                                    @if ($item->penilaian->diskualifikasi == "no")
                                        @if ($item->penilaian->jarak == "yes")

                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">Penilaian Jarak</h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <input type="hidden" name="id[]" value="{{$item->id}}">
                                                <input type="hidden" name="kesalahan[]" value="{{$item->kesalahan}}">
                                                <input type="hidden" name="waktu[]" value="{{$item->waktu}}">
                                                <input type="hidden" name="nilai[]" value="{{$item->nilai}}">

                                                <div class="col-lg-3">
                                                    <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                </div>
                                                <div class="col-lg-4">
                                                    <input  type="text" class="form-control m-input allownumericwithdecimal "  name="jarak[]" value="{{$item->jarak}}" placeholder="Jarak Pengereman dlm meter" >
                                                </div>
                                                {{-- <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}" readonly >
                                                    <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                                </div> --}}
                                                <div class="col-lg-2">
                                                    <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3"  value="{{$item->nilai}}"placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                                </div>

                                            </div>
                                        @endif
                                        @if ($item->penilaian->waktu == "yes")

                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">Penilaian Waktu</h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <input type="hidden" name="id[]" value="{{$item->id}}">
                                                <input type="hidden" name="kesalahan[]" value="{{$item->kesalahan}}">
                                                <input type="hidden" name="nilai[]" value="{{$item->nilai}}">
                                                <input type="hidden" name="jarak[]" value="{{$item->jarak}}">

                                                <div class="col-lg-3">
                                                    <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                </div>
                                                <div class="col-lg-4">
                                                    <input  type="text" class="form-control m-input allownumericwithdecimal " name="waktu[]" value="{{$item->waktu}}" placeholder="Waktu tempuh" >
                                                </div>
                                                {{-- <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}" readonly >
                                                    <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                                </div> --}}
                                                <div class="col-lg-2">
                                                    <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3"  placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                                </div>

                                            </div>
                                        @endif
                                        @if ($item->penilaian->jarak == "no" && $item->penilaian->waktu == "no" )
                                            <div class="form-group m-form__group row">
                                                <input type="hidden" name="id[]" value="{{$item->id}}">
                                                <input type="hidden" name="jarak[]" value="{{$item->jarak}}">
                                                <input type="hidden" name="waktu[]" value="{{$item->waktu}}">

                                                <div class="col-lg-3">
                                                    <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                                </div>
                                                <div class="col-lg-3">
                                                    <input id="m_touchspin_1" type="text" class="form-control m-input allownumericwithdecimal kesalahan{{$item->id}}"  name="kesalahan[]" value="{{$item->kesalahan}}" placeholder="0" data-max="{{ceil(1000 / $item->penilaian->poin)}}" onchange="hitung_nilai(this.value,'{{$item->id}}')">
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]" value="{{$item->nilai}}" readonly >
                                                    <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                                </div>
                                                <div class="col-lg-2">
                                                    <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3" placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                                </div>

                                            </div>
                                        @endif
                                    @endif
                                @endif
                            @endforeach
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section m-form__section--last">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title">Diskualifikasi</h3>
                                </div>
                            @foreach ($nilai as $item)
                                    @if ($item->penilaian->diskualifikasi == "yes")

                                    <div class="form-group m-form__group row">
                                        <input type="hidden" name="id[]" value="{{$item->id}}">
                                        <input type="hidden" name="jarak[]" value="{{$item->jarak}}">
                                        <input type="hidden" name="waktu[]" value="{{$item->waktu}}">
                                        <div class="col-lg-3">
                                            <textarea class="form-control m-input" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3" readonly >{{$item->penilaian->deskripsi}}</textarea>
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control m-input poin{{$item->id}}" value="{{$item->penilaian->poin}}" readonly>
                                        </div>
                                        <div class="col-lg-3">
                                            <input id="m_touchspin_2" type="text" class="form-control m-input allownumericwithdecimal kesalahan{{$item->id}}"  name="kesalahan[]" value="{{$item->kesalahan}}" placeholder="0" onchange="hitung_nilai(this.value,'{{$item->id}}')">
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control m-input nilai-{{$item->id}} nilai"  placeholder="nilai" name="nilai[]"  value="{{$item->nilai}}" readonly >
                                            <input type="hidden" class="last_nilai_{{$item->id}}" value="{{$item->nilai}}">
                                        </div>
                                        <div class="col-lg-2">
                                            <textarea class="form-control m-input" name="keterangan[]" id="exampleFormControlTextarea1" rows="3" placeholder="keterangan" >{{$item->keterangan}}</textarea>
                                        </div>
                                    </div>
                                    @endif

                            @endforeach
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="{{url('/penjurian' , ['tgl'=>$tgl , 'id'=>$juri])}}"><button type="button" class="btn btn-danger" >Kembali Ke Halaman Penjurian</button></a>
                        <button type="submit" class="btn btn-primary">Simpan Nilai</button>
                    </div>
                    </form>
<div class="modal fade" id="modal_selesai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <h6 class="m-badge m-badge--danger m-badge--wide">Jika anda telah selesai melakukan penjurian maka anda tidak dapat melakukan edit atau hapus data peserta setelah ini</h6>
                    <h5>Apakah anda yakin telah selesai melakukan penjurian? </h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-danger">Ya, saya telah selesai </button>
                </div>
        </div>
    </div>
</div>
                </div>
            </div>
        </div>

<!--end::Modal-->
<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script>

var BootstrapTimepicker = function () {

    //== Private functions
    var demos = function () {
        // minimum setup
        $('#m_timepicker_1, #m_timepicker_1_modal').timepicker();

        // minimum setup
        $('#m_timepicker_2, #m_timepicker_2_modal').timepicker({
            minuteStep: 1,
            defaultTime: '{{$waktu}}',
            showInputs: true,
            showSeconds: true,
            secondStep: 1,
            showMeridian: false,
            snapToStep: false,
        });

        // default time
        $('#m_timepicker_3, #m_timepicker_3_modal').timepicker({
            defaultTime: '11:45:20 AM',
            minuteStep: 1,
            showSeconds: true,
            showMeridian: true
        });

        // default time
        $('#m_timepicker_4, #m_timepicker_4_modal').timepicker({
            defaultTime: '10:30:20 AM',
            minuteStep: 1,
            showSeconds: true,
            showMeridian: true
        });

        // validation state demos
        // minimum setup
        $('#m_timepicker_1_validate, #m_timepicker_2_validate, #m_timepicker_3_validate').timepicker({
            minuteStep: 1,
            showSeconds: true,
            showMeridian: false,
            snapToStep: true
        });
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

jQuery(document).ready(function() {
    BootstrapTimepicker.init();
});
    $(function(){
  $('body').backDetect(function(){
        window.reload();
        var c = confirm("Gunakan Tombol 'Kembali ke halaman Penjurian' Untuk Kembali ke halaman sebelumnya !!! ")
        if(c == true){
            $('.simpan-nilai').submit()
        }
        else {
            $('.simpan-nilai').submit()
        }
  });


        $('#modal_tambah_event').modal({
              backdrop: 'static',
                keyboard: false
        });

        @if (session('sukses'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil Menyimpan Nilai ");
        @endif
        @if (session('error'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.error("Nama Modul Tersebut Sudah Ada !!!  ");

        @endif
        @if (session('update'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah data penilaian  ");
        @endif
        @if (session('delete'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menghapus data modul  ");
        @endif
        @if (session('penilaian'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan penilaian baru ");
        @endif


        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });




    });

    var klik = 0;
    function tambah_kategori(index){
         klik += 1;
        $('.coaching-'+index).append(
            '<div class="form-group  m-form__group kategori-'+klik+' " >'+
                '<input type="hidden" name="coaching[]" value="'+index+'">'+
                            '<div data-repeater-list="">'+
                                '<div data-repeater-item class="form-group m-form__group">'+
                                    '<div class="form-group">'+
                                        '<label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>'+
                                        '<textarea class="form-control" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3"  required ></textarea>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="m-form__group form-group row">'+
                                            '<div class="col-md-6 m--margin-bottom-10">'+
                                                '<label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>'+
                                                '<input type="text" class="form-control allownumericwithdecimal" id="recipient-name" name="poin[]"  min="0" required >'+
                                            '</div>'+
                                            '<div class="col-md-6 align-self-center" >'+
                                                '<div data-repeater-delete="" class="btn-md btn btn-danger m-btn m-btn--icon m-btn--pill float-right hapus-kategori" data-index="'+klik+'">'+
                                                    '<span>'+
                                                        '<i class="la la-trash-o"></i>'+
                                                        '<span>Delete</span>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+
                          '</div>'+
                    '</div>'
        );

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

        $('.hapus-kategori').click(function(){
            let data_index = $(this).data('index');
            $('.kategori-'+data_index).slideUp("normal" , function(){
                $(this).remove();
            });
        })


    }

    var kliks = 0;
    function tambah_kategori_dis(index){
         kliks += 1;
        $('.coaching-dis'+index).append(
            '<div class="form-group  m-form__group kategori-dis'+kliks+' " >'+
                '<input type="hidden" name="coaching[]" value="'+index+'">'+
                            '<div data-repeater-list="">'+
                                '<div data-repeater-item class="form-group m-form__group">'+
                                    '<div class="form-group">'+
                                        '<label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>'+
                                        '<textarea class="form-control" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3"  required ></textarea>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="m-form__group form-group row">'+
                                            '<div class="col-md-6 m--margin-bottom-10">'+
                                                '<label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>'+
                                                '<input type="text" class="form-control allownumericwithdecimal" id="recipient-name" name="poin[]"  min="0" value="1000" required readonly>'+
                                            '</div>'+
                                            '<div class="col-md-6 align-self-center" >'+
                                                '<div data-repeater-delete="" class="btn-md btn btn-danger m-btn m-btn--icon m-btn--pill float-right hapus-kategori-dis" data-index="'+kliks+'">'+
                                                    '<span>'+
                                                        '<i class="la la-trash-o"></i>'+
                                                        '<span>Delete</span>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+
                          '</div>'+
                    '</div>'
        );

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

      $('#m_touchspin_1, #m_touchspin_2_1').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',

            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
        });

        $('.hapus-kategori-dis').click(function(){
            let data_index = $(this).data('index');
            $('.kategori-dis'+data_index).slideUp("normal" , function(){
                $(this).remove();
            });
        })


    }

    function dis(value, index){
        if(value == "yes"){
            $('.poin-des-'+index).val('1000');
            $('.poin-des-'+index).prop('readonly', true);
        }
        else{
            $('.poin-des-'+index).val('');
            $('.poin-des-'+index).removeAttr('readonly');
        }
    }

    var poin_user = {{$poin}};
    $('.poin_user').text(poin_user);
    function hitung_nilai(value,index){
        let poin = $('.poin'+index).val();
        let hitung = value * poin;
        let nilai = $('.nilai-'+index);
        let last_hitung = $('.last_nilai_'+index).val();
        if(hitung > last_hitung){
            poin_user = poin_user - poin;
            nilai.val(hitung);
            if(poin_user <= 0){
                poin_user = 0
                $('.allownumericwithdecimal').prop("disabled" , true);
                $('.bootstrap-touchspin-down').click(function(){
                    $('.allownumericwithdecimal').prop("disabled" , false);
                })
                //$('.bootstrap-touchspin-up').prop("disabled", true);
                //$('.bootstrap-touchspin-down').prop("disabled", false);
            }
        }
        else if(hitung < last_hitung){
            poin_user = parseInt(poin_user) + parseInt(poin)
            nilai.val(hitung);
            if(poin_user >= 1000){
                poin_user = 1000
                $('.allownumericwithdecimal').val("");
                $('.nilai ').val("");
                $('.allownumericwithdecimal').prop("disabled" , true);
                $('.bootstrap-touchspin-up').click(function(){
                    $('.allownumericwithdecimal').prop("disabled" , false);
                })
            }
        }
        $('.poin_user').text(poin_user);
        $('.last_nilai_'+index).val(hitung);


    }
    $('.reset_nilai').click(function(){
        $('.poin_user').text("1000");
        $('.allownumericwithdecimal').val("");
        $('.nilai ').val("");
        $('.allownumericwithdecimal').prop("disabled" , true);
        $('.bootstrap-touchspin-up').click(function(){
            $('.allownumericwithdecimal').prop("disabled" , false);
        })

    })

</script>
@endsection
