@extends('layouts.dashboard')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">Data Penjurian Modul {{$juri->modul->name}} {{$tanggal}}</h3>
                        </div>

                    </div>
                </div>

                <!-- END: Subheader -->
                    <div class="m-content">

                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text m--font-info">
                                            Juri : {{$juri->user->name}} &nbsp;
                                        </h3>
                                        @if (strcasecmp("balancing", $juri->modul->name) == 0)
                                            <h3 class="m-portlet__head-text m--font-danger"> Target Waktu : ({{$juri->target_waktu}}) </h3>
                                        @endif
                                        @if ($poindanwaktu != null)

                                            @if (strcasecmp("slalom course", $juri->modul->name) == 0)
                                            <h3 class="m-portlet__head-text m--font-danger"> Waktu Terpendek : ({{$poindanwaktu->user->name . $poindanwaktu->user->no_punggung ."-" . $poindanwaktu->waktu}}) </h3>
                                            @endif
                                        @endif
                                        @if ($poindanjarak != null)

                                            @if (strcasecmp("braking" , $juri->modul->name == 0))
                                            <h3 class="m-portlet__head-text m--font-danger">Jarak Terpendek : ({{$poindanjarak->user->name . $poindanjarak->user->no_punggung  ." - " . $poindanjarak->jarak}}m)</h3>
                                            @endif
                                    @endif
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">

                                        @if (Auth::user()->id == $juri->user_id)
                                            @if ($juri->selesai == 0)
                                                <li class="m-portlet__nav-item">
                                                    <a href="#" class="m-portlet__nav-link  btn btn--sm m-btn--pill btn-success m-btn m-btn--label-brand" data-toggle="modal" data-target="#modal_selesai">
                                                        Selesai Penjurian
                                                    </a>
                                                </li>
                                            @else
                                                <li class="m-portlet__nav-item">
                                                    <a href="#" class="m-portlet__nav-link  btn btn--sm m-btn--pill btn-danger m-btn m-btn--label-brand" >
                                                        Penjurian Selesai
                                                    </a>
                                                </li>
                                            @endif
                                        @else
                                            @if ($juri->selesai == 0)
                                            @else
                                                <li class="m-portlet__nav-item">
                                                    <a href="#" class="m-portlet__nav-link  btn btn--sm m-btn--pill btn-danger m-btn m-btn--label-brand" >
                                                        Penjurian Selesai
                                                    </a>
                                                </li>
                                            @endif
                                        @endif
                                        {{--  <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                            <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                Export Options
                                            </a>
                                            <div class="m-dropdown__wrapper" style="z-index: 101;">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36px;"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            <ul class="m-nav">
                                                                <li class="m-nav__section m-nav__section--first">
                                                                    <span class="m-nav__section-text">Export Tools</span>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_print">
                                                                        <i class="m-nav__link-icon la la-print"></i>
                                                                        <span class="m-nav__link-text">Print</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_copy">
                                                                        <i class="m-nav__link-icon la la-copy"></i>
                                                                        <span class="m-nav__link-text">Copy</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_excel">
                                                                        <i class="m-nav__link-icon la la-file-excel-o"></i>
                                                                        <span class="m-nav__link-text">Excel</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_csv">
                                                                        <i class="m-nav__link-icon la la-file-text-o"></i>
                                                                        <span class="m-nav__link-text">CSV</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_pdf">
                                                                        <i class="m-nav__link-icon la la-file-pdf-o"></i>
                                                                        <span class="m-nav__link-text">PDF</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>  --}}
                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body">

                                <!--begin: Datatable -->
                                <table class="table table-striped-table-bordered table-hover table-checkable responsive no-wrap" id="m_table_2">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama</th>
                                            <th>No. Punggung</th>
                                            @if (strcasecmp("braking", $juri->modul->name) == 0)
                                            <th>Jarak</th>
                                            <th>Selisih <br> Dengan Jarak Terpendek  </th>
                                            @endif
                                            @if (strcasecmp("slalom course", $juri->modul->name) == 0)
                                            <th>Waktu</th>
                                            <th>Selisih <br> Dengan Waktu Terpendek <br> dalam milidetik </th>
                                            @endif
                                            @if (strcasecmp("balancing", $juri->modul->name) == 0)
                                            <th>Waktu</th>
                                            <th>Selisih <br> Dengan Waktu Standar <br> dalam milidetik </th>
                                            @endif
                                            <th>Total Nilai</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($nilai as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->user->name}}</td>
                                            <td>{{$item->user->no_punggung}}</td>
                                        @if ($poindanjarak != null || $poindanwaktu != null)

                                           @if (strcasecmp("braking", $juri->modul->name) == 0)
                                            <td>{{$item->jarak}}m</td>
                                            <td>{{$item->jarak - $poindanjarak->jarak}}</td>
                                            @endif
                                            @if (strcasecmp("slalom course", $juri->modul->name) == 0)
                                            <td>{{$item->waktu}}</td>
                                            <td>{{($item->waktu * 1000 ) - ( $poindanwaktu->waktu * 1000 ) }}</td>
                                            @endif
                                            @if (strcasecmp("balancing", $juri->modul->name) == 0)
                                            <td>{{$item->waktu}}</td>
                                            <td>{{($item->waktu * 1000 ) - ( $poindanwaktu->waktu * 1000 ) }}</td>
                                            @endif
                                        @else
                                            <td></td>
                                            <td></td>
                                        @endif
                                            <td>{{$item->nilai}}</td>
                                            <td nowrap="">
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                                                    <i class="la la-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        @if (Auth::user()->id == $juri->user_id || Auth::user()->jabatan_id == 1)
                                                            <a class="dropdown-item" href="{{url('/tambah-nilai' , ['tgl' => $tgl, 'id' => $item->user_id , 'juri' => $juri->id  , 'nilai' => $item->nilai]) }}" ><i class="la la-edit"></i> Beri Penilaian / Edit Penilaian</a>
                                                        @else
                                                        @endif
                                                        <a class="dropdown-item" href="{{url('/lihat-nilai' , ['tgl' => $tgl, 'id' => $item->user_id , 'juri' => $juri->id  , 'nilai' => $item->nilai]) }}" ><i class="la la-th-list "></i> Lihat Hasil Penilaian</a>

                                                        {{-- @if ($item->diskualifikasi == "yes")
                                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_edit_des{{$item->id}}"><i class="la la-edit"></i> Edit Details</a>
                                                        @else
                                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_edit_{{$item->id}}"><i class="la la-edit"></i> Edit Details</a>
                                                        @endif
                                                        @if (Auth::user()->jabatan->name == "Superadmin")
                                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_hapus_{{$item->id}}"><i class="la  la-trash-o"></i> Delete Data</a>
                                                        @else

                                                        @endif --}}
                                                    </div>
                                                </span>
                                            </td>
                                        </tr>
                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
            </div>
        </div>
    </div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal_target_waktu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Waktu Standar Untuk Penilaian Balancing</h5>
                {{--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>  --}}
            </div>
            <div class="modal-body">
                <form class="tambah" action="{{url('/tambah-target-waktu')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$juri->id}}">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Target Waktu:</label>
                        <input type="text" class="form-control allownumericwithdecimal" id="recipient-name" name="target_waktu" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Poin Pengurangan:</label>
                        <input type="text" class="form-control" id="recipient-name" name="poin_pengurangan" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{--
<!--begin::Modal-->
<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Modul</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="tambah" action="{{url('/tambah-modul')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nama Modul:</label>
                        <input type="text" class="form-control" id="recipient-name" name="name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@foreach ($nilai as $item)
<div class="modal fade" id="modal_edit_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" action="{{url('/edit-penilaian')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <input type="hidden" name="diskualifikasi" value="{{$item->diskualifikasi}}">
                    <div class="form-group">
                        <label for="" class="form-control-label">Pilih Modul</label>
                        <select name="modul_id" id="" class="form-control">
                            <option value="{{$item->modul_id}}" selected>{{$item->modul->name}}</option>
                            @foreach ($modul as $m)
                                @if ($item->modul_id == $m->id)
                                    <option value="{{$m->id}}" style="display:none">{{$m->name}}</option>
                                @else
                                    <option value="{{$m->id}}">{{$m->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="form-control-label">Diskualifikasi ? </label>
                        <select name="diskualifikasi" id="" class="form-control" onchange="dis(this.value,'{{$item->id}}' )">
                            <option value="{{$item->diskualifikasi}}" selected>{{$item->diskualifikasi}}</option>
                                @if ($item->diskualifikasi == "no")
                                    <option value="yes">yes</option>
                                @endif

                                @if ($item->diskualifikasi == "yes")
                                    <option value="no">no</option>
                                @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>
                        <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" rows="3"  required >{{$item->deskripsi}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>
                        <input type="text" class="form-control allownumericwithdecimal poin-des-{{$item->id}}" id="recipient-name" name="poin"  min="0" value="{{$item->poin}}" required >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_edit_des{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" action="{{url('/edit-penilaian')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <div class="alert alert-info"  role="alert">
                        <strong>Info !</strong> Pada bagian Diskualifikasi nilai poin berjumlah 1000.
                    </div>

                    <div class="form-group">
                        <label for="" class="form-control-label">Pilih Modul</label>
                        <select name="modul_id" id="" class="form-control" >
                            <option value="{{$item->modul_id}}" selected>{{$item->modul->name}}</option>
                            @foreach ($modul as $m)
                                @if ($item->modul_id == $m->id)
                                    <option value="{{$m->id}}" style="display:none">{{$m->name}}</option>
                                @else
                                    <option value="{{$m->id}}">{{$m->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="form-control-label">Diskualifikasi ? </label>
                        <select name="diskualifikasi" id="" class="form-control" onchange="dis(this.value,'{{$item->id}}' )">
                            <option value="{{$item->diskualifikasi}}" selected>{{$item->diskualifikasi}}</option>
                                @if ($item->diskualifikasi == "no")
                                    <option value="yes">yes</option>
                                @endif

                                @if ($item->diskualifikasi == "yes")
                                    <option value="no">no</option>
                                @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>
                        <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" rows="3"  required >{{$item->deskripsi}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>
                        <input type="text" class="form-control allownumericwithdecimal poin-des-{{$item->id}}" id="recipient-name" name="poin"  min="0" value="{{$item->poin}}" required readonly >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_hapus_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('/delete-penilaian')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <h6 class="m-badge m-badge--danger m-badge--wide">Dengan menghapus data penilaian ini, maka data-data penilaian yang berada di data ujian yang telah dilakukan oleh peserta yang memiliki penilaian ini akan terhapus dari database </h6>
                    <h5>Apakah Anda Yakin Akan Menghapus penilaian <span class="m--font-danger">'{{$item->deskripsi}}'</span> Dari Database ?</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-danger">Ya, saya ingin menghapus</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_penilaian{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Penilaian Modul {{$item->deskripsi}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="tambah" action="{{url('/tambah-penilaian' , $item->id)}}" method="POST">
                    {{ csrf_field() }}

                    @for ($i = 1; $i < 4 ; $i++)
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1" class="form-control-label"><h5>Coaching Skill {{$i}}</h5></label>
                    </div>
                    <div class="m-form__group form-group row">
                        <div class="col-lg-6">
                            <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide btn-coaching-{{$i}}" onclick="tambah_kategori('{{$i}}')">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Tambah Kategori</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="coaching-{{$i}}">

                    </div>

                @endfor
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_penilaian_dis{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Penilaian Diskualifikasi Modul {{$item->deskripsi}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="tambah" action="{{url('/tambah-penilaian-dis' , $item->id)}}" method="POST">
                    {{ csrf_field() }}
                    <div class="alert alert-info"  role="alert">
                        <strong>Info !</strong> Pada bagian Diskualifikasi nilai poin berjumlah 1000.
                    </div>
                    @for ($i = 1; $i < 4 ; $i++)
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1" class="form-control-label"><h5>Coaching Skill {{$i}}</h5></label>
                    </div>
                    <div class="m-form__group form-group row">
                        <div class="col-lg-6">
                            <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide btn-coaching-{{$i}}" onclick="tambah_kategori_dis('{{$i}}')">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Tambah Kategori</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="coaching-dis{{$i}}">

                    </div>

                @endfor
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach
<!--end::Modal--> --}}
<div class="modal fade" id="modal_selesai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('/selesai-juri')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$juri->id}}">
                    <h6 class="m-badge m-badge--danger m-badge--wide">Jika anda telah selesai melakukan penjurian maka anda tidak dapat melakukan edit atau hapus data peserta setelah ini</h6>
                    <h5>Apakah anda yakin telah selesai melakukan penjurian? </h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-danger">Ya, saya telah selesai </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>

<script>

    $(function(){

        @if (empty($juri->target_waktu))
            $('#m_modal_target_waktu').modal({
                backdrop: 'static',
                keyboard: false
            });
        @endif

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

        @if (session('sukses'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan penilaian baru untuk peserta  ");
        @endif
        @if (session('error'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.error("Nama Modul Tersebut Sudah Ada !!!  ");

        @endif
        @if (session('update'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah data penilaian  ");
        @endif
        @if (session('delete'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menghapus data modul  ");
        @endif
        @if (session('penilaian'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan penilaian baru ");
        @endif
        {{--  var values = "";
        var kat = 0;
        $('.gender').change(function(){
            let value = $(this).val();
            values = value;
            let kategoris = $('.kategori option:selected').val();
             if(values != "Pria" && kategoris != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                values = $('.gender option:selected').val();

                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })


    @foreach ($nilai as $item)
            var e_gender{{$item->id}} = ""
            var e_kat{{$item->id}} = 0;
        $('.edit-gender{{$item->id}}').change(function(){
            let value = $(this).val();
            e_gender{{$item->id}} = value;
            let kategoris = $('.edit-kategori{{$item->id}} option:selected').val();
             if(e_gender{{$item->id}} != "Pria" && kategoris != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.edit-kategori{{$item->id}} ').change(function(){
                let kategori = $(this).val();
                e_kat{{$item->id}} = kategori;
                if(e_gender{{$item->id}} != "Pria" && kategori != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.edit-kategori{{$item->id}} ').change(function(){
                let kategori = $(this).val();
                e_kat{{$item->id}} = kategori;
                e_gender{{$item->id}} = $('.edit-gender{{$item->id}} option:selected').val();
                if(e_gender{{$item->id}} != "Pria" && kategori != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

    @endforeach  --}}

    });

    var klik = 0;
    function tambah_kategori(index){
         klik += 1;
        $('.coaching-'+index).append(
            '<div class="form-group  m-form__group kategori-'+klik+' " >'+
                '<input type="hidden" name="coaching[]" value="'+index+'">'+
                            '<div data-repeater-list="">'+
                                '<div data-repeater-item class="form-group m-form__group">'+
                                    '<div class="form-group">'+
                                        '<label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>'+
                                        '<textarea class="form-control" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3"  required ></textarea>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="m-form__group form-group row">'+
                                            '<div class="col-md-6 m--margin-bottom-10">'+
                                                '<label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>'+
                                                '<input type="text" class="form-control allownumericwithdecimal" id="recipient-name" name="poin[]"  min="0" required >'+
                                            '</div>'+
                                            '<div class="col-md-6 align-self-center" >'+
                                                '<div data-repeater-delete="" class="btn-md btn btn-danger m-btn m-btn--icon m-btn--pill float-right hapus-kategori" data-index="'+klik+'">'+
                                                    '<span>'+
                                                        '<i class="la la-trash-o"></i>'+
                                                        '<span>Delete</span>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+
                          '</div>'+
                    '</div>'
        );

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

        $('.hapus-kategori').click(function(){
            let data_index = $(this).data('index');
            $('.kategori-'+data_index).slideUp("normal" , function(){
                $(this).remove();
            });
        })


    }

    var kliks = 0;
    function tambah_kategori_dis(index){
         kliks += 1;
        $('.coaching-dis'+index).append(
            '<div class="form-group  m-form__group kategori-dis'+kliks+' " >'+
                '<input type="hidden" name="coaching[]" value="'+index+'">'+
                            '<div data-repeater-list="">'+
                                '<div data-repeater-item class="form-group m-form__group">'+
                                    '<div class="form-group">'+
                                        '<label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>'+
                                        '<textarea class="form-control" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3"  required ></textarea>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="m-form__group form-group row">'+
                                            '<div class="col-md-6 m--margin-bottom-10">'+
                                                '<label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>'+
                                                '<input type="text" class="form-control allownumericwithdecimal" id="recipient-name" name="poin[]"  min="0" value="1000" required readonly>'+
                                            '</div>'+
                                            '<div class="col-md-6 align-self-center" >'+
                                                '<div data-repeater-delete="" class="btn-md btn btn-danger m-btn m-btn--icon m-btn--pill float-right hapus-kategori-dis" data-index="'+kliks+'">'+
                                                    '<span>'+
                                                        '<i class="la la-trash-o"></i>'+
                                                        '<span>Delete</span>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+
                          '</div>'+
                    '</div>'
        );

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

        $('.hapus-kategori-dis').click(function(){
            let data_index = $(this).data('index');
            $('.kategori-dis'+data_index).slideUp("normal" , function(){
                $(this).remove();
            });
        })


    }

    function dis(value, index){
        if(value == "yes"){
            $('.poin-des-'+index).val('1000');
            $('.poin-des-'+index).prop('readonly', true);
        }
        else{
            $('.poin-des-'+index).val('');
            $('.poin-des-'+index).removeAttr('readonly');
        }
    }
</script>
@endsection
