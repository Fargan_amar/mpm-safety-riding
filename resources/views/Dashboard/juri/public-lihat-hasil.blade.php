@extends('layouts.public-dashboard')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">Data Penjurian Peserta {{$user_name->name}} Modul {{$nama_juri->modul->name}}, Tanggal :  {{date("d , F Y" , strtotime($tgl))}}</h3>
                        </div>

                    </div>
                </div>

                <!-- END: Subheader -->
                    <div class="m-content">

                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text m--font-info">
                                            Juri : {{$nama_juri->user->name}}
                                        </h3>
                                        <h3 class="m-portlet__head-text m--font-success" style="margin-left:1rem">Kecepatan : {{$rekap->kecepatan}}km/jam</h3>
                                        
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a href="{{url()->previous()}}" class="m-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air">
                                               <i class="la  la-arrow-left "></i> back
                                            </a>
                                        </li>

                                        <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                            <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                {{--  m-portlet__nav-link m-dropdown__toggle btn btn-primary m-btn m-btn--pill m-btn--air  --}}
                                                Export Options
                                            </a>
                                            <div class="m-dropdown__wrapper" style="z-index: 101;">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36px;"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            <ul class="m-nav">
                                                                <li class="m-nav__section m-nav__section--first">
                                                                    <span class="m-nav__section-text">Export Tools</span>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_print">
                                                                        <i class="m-nav__link-icon la la-print"></i>
                                                                        <span class="m-nav__link-text">Print</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_copy">
                                                                        <i class="m-nav__link-icon la la-copy"></i>
                                                                        <span class="m-nav__link-text">Copy</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_excel">
                                                                        <i class="m-nav__link-icon la la-file-excel-o"></i>
                                                                        <span class="m-nav__link-text">Excel</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_csv">
                                                                        <i class="m-nav__link-icon la la-file-text-o"></i>
                                                                        <span class="m-nav__link-text">CSV</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_pdf">
                                                                        <i class="m-nav__link-icon la la-file-pdf-o"></i>
                                                                        <span class="m-nav__link-text">PDF</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body">

                                <!--begin: Datatable -->
                                <table class="table table-striped-table-bordered table-hover table-checkable responsive no-wrap" id="m_table_hasil">
                                    <thead>
                                        <tr>
                                            <th>Coaching Skill </th>
                                            <th>Deskripsi Penilaian</th>
                                            <th>Poin</th>
                                            @if (strcasecmp("braking", $nama_juri->modul->name) == 0)
                                                <th>Jarak</th>
                                                <th>Jumlah Kesalahan/selisih jarak</th>
                                            @endif
                                            @if (strcasecmp("slalom course", $nama_juri->modul->name) == 0)
                                                <th>Waktu</th>
                                                <th>Jumlah Kesalahan/selisih Waktu</th>
                                            @endif
                                            @if (strcasecmp("balancing", $nama_juri->modul->name) == 0)
                                                <th>Waktu</th>
                                                <th>Target Waktu</th>
                                                <th>Jumlah Kesalahan/selisih Waktu</th>
                                            @endif
                                            <th>Nilai</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($nilai as $item)
                                        <tr>
                                            <td>Coaching Skill {{$item->penilaian->coaching}}</td>
                                            <td>{{$item->penilaian->deskripsi}}</td>
                                            <td>{{$item->penilaian->poin}}</td>
                                            @if (strcasecmp("braking", $nama_juri->modul->name) == 0)
                                                @if ($item->jarak == null || $item->jarak == 0)
                                                    <td></td>
                                                @else
                                                    <td>{{$item->jarak}}m</td>
                                                @endif
                                                @if ($item->penilaian->jarak == "yes")
                                                    <td>{{$item->kesalahan}}cm</td>
                                                @else
                                                    <td>{{$item->kesalahan}}</td>
                                                @endif
                                            @endif
                                            @if (strcasecmp("slalom course", $nama_juri->modul->name) == 0)
                                                @if ($item->waktu == null || $item->waktu == 0)
                                                    <td></td>
                                                @else
                                                    <td>{{$item->waktu}}</td>
                                                @endif
                                                @if ($item->penilaian->waktu == "yes")
                                                    <td>{{$item->kesalahan}}</td>
                                                @else
                                                    <td>{{$item->kesalahan}}</td>
                                                @endif
                                            @endif
                                            @if (strcasecmp("balancing", $nama_juri->modul->name) == 0)
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            @endif
                                            <td>{{$item->nilai}}</td>
                                            <td>{{$item->keterangan}}</td>
                                        </tr>
                                            @endforeach
                                            @if (strcasecmp("balancing", $nama_juri->modul->name) == 0)
                                        <tr>
                                            <td>Coaching Skill 3</td>
                                            <td>Waktu Peserta</td>
                                            <td>{{$nama_juri->poin_pengurangan}}</td>
                                            <td>{{$rekap->waktu}}</td>
                                            <td>{{$nama_juri->target_waktu}}</td>
                                            <td>{{($nama_juri->target_waktu * 1000 ) - ($rekap->waktu * 1000) }}</td>
                                            @if ($rekap->waktu < $nama_juri->target_waktu)
                                                <td>{{$nama_juri->poin_pengurangan}}</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>{{$rekap->nilai}}</td>
                                            <td>{{$item->keterangan}}</td>
                                        </tr>
                                            @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
            </div>
        </div>
    </div>

{{--
<!--begin::Modal-->
<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Modul</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="tambah" action="{{url('/tambah-modul')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nama Modul:</label>
                        <input type="text" class="form-control" id="recipient-name" name="name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@foreach ($nilai as $item)
<div class="modal fade" id="modal_edit_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" action="{{url('/edit-penilaian')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <input type="hidden" name="diskualifikasi" value="{{$item->diskualifikasi}}">
                    <div class="form-group">
                        <label for="" class="form-control-label">Pilih Modul</label>
                        <select name="modul_id" id="" class="form-control">
                            <option value="{{$item->modul_id}}" selected>{{$item->modul->name}}</option>
                            @foreach ($modul as $m)
                                @if ($item->modul_id == $m->id)
                                    <option value="{{$m->id}}" style="display:none">{{$m->name}}</option>
                                @else
                                    <option value="{{$m->id}}">{{$m->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="form-control-label">Diskualifikasi ? </label>
                        <select name="diskualifikasi" id="" class="form-control" onchange="dis(this.value,'{{$item->id}}' )">
                            <option value="{{$item->diskualifikasi}}" selected>{{$item->diskualifikasi}}</option>
                                @if ($item->diskualifikasi == "no")
                                    <option value="yes">yes</option>
                                @endif

                                @if ($item->diskualifikasi == "yes")
                                    <option value="no">no</option>
                                @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>
                        <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" rows="3"  required >{{$item->deskripsi}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>
                        <input type="text" class="form-control allownumericwithdecimal poin-des-{{$item->id}}" id="recipient-name" name="poin"  min="0" value="{{$item->poin}}" required >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_edit_des{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" action="{{url('/edit-penilaian')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <div class="alert alert-info"  role="alert">
                        <strong>Info !</strong> Pada bagian Diskualifikasi nilai poin berjumlah 1000.
                    </div>

                    <div class="form-group">
                        <label for="" class="form-control-label">Pilih Modul</label>
                        <select name="modul_id" id="" class="form-control" >
                            <option value="{{$item->modul_id}}" selected>{{$item->modul->name}}</option>
                            @foreach ($modul as $m)
                                @if ($item->modul_id == $m->id)
                                    <option value="{{$m->id}}" style="display:none">{{$m->name}}</option>
                                @else
                                    <option value="{{$m->id}}">{{$m->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="form-control-label">Diskualifikasi ? </label>
                        <select name="diskualifikasi" id="" class="form-control" onchange="dis(this.value,'{{$item->id}}' )">
                            <option value="{{$item->diskualifikasi}}" selected>{{$item->diskualifikasi}}</option>
                                @if ($item->diskualifikasi == "no")
                                    <option value="yes">yes</option>
                                @endif

                                @if ($item->diskualifikasi == "yes")
                                    <option value="no">no</option>
                                @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>
                        <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" rows="3"  required >{{$item->deskripsi}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>
                        <input type="text" class="form-control allownumericwithdecimal poin-des-{{$item->id}}" id="recipient-name" name="poin"  min="0" value="{{$item->poin}}" required readonly >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_hapus_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('/delete-penilaian')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <h6 class="m-badge m-badge--danger m-badge--wide">Dengan menghapus data penilaian ini, maka data-data penilaian yang berada di data ujian yang telah dilakukan oleh peserta yang memiliki penilaian ini akan terhapus dari database </h6>
                    <h5>Apakah Anda Yakin Akan Menghapus penilaian <span class="m--font-danger">'{{$item->deskripsi}}'</span> Dari Database ?</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-danger">Ya, saya ingin menghapus</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_penilaian{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Penilaian Modul {{$item->deskripsi}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="tambah" action="{{url('/tambah-penilaian' , $item->id)}}" method="POST">
                    {{ csrf_field() }}

                    @for ($i = 1; $i < 4 ; $i++)
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1" class="form-control-label"><h5>Coaching Skill {{$i}}</h5></label>
                    </div>
                    <div class="m-form__group form-group row">
                        <div class="col-lg-6">
                            <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide btn-coaching-{{$i}}" onclick="tambah_kategori('{{$i}}')">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Tambah Kategori</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="coaching-{{$i}}">

                    </div>

                @endfor
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_penilaian_dis{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Penilaian Diskualifikasi Modul {{$item->deskripsi}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="tambah" action="{{url('/tambah-penilaian-dis' , $item->id)}}" method="POST">
                    {{ csrf_field() }}
                    <div class="alert alert-info"  role="alert">
                        <strong>Info !</strong> Pada bagian Diskualifikasi nilai poin berjumlah 1000.
                    </div>
                    @for ($i = 1; $i < 4 ; $i++)
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1" class="form-control-label"><h5>Coaching Skill {{$i}}</h5></label>
                    </div>
                    <div class="m-form__group form-group row">
                        <div class="col-lg-6">
                            <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide btn-coaching-{{$i}}" onclick="tambah_kategori_dis('{{$i}}')">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Tambah Kategori</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="coaching-dis{{$i}}">

                    </div>

                @endfor
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach
<!--end::Modal--> --}}
<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>

<script>
    $(function(){

        @if (session('sukses'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan penilaian baru untuk peserta  ");
        @endif
        @if (session('error'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.error("Nama Modul Tersebut Sudah Ada !!!  ");

        @endif
        @if (session('update'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah data penilaian  ");
        @endif
        @if (session('delete'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menghapus data modul  ");
        @endif
        @if (session('penilaian'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan penilaian baru ");
        @endif
        {{--  var values = "";
        var kat = 0;
        $('.gender').change(function(){
            let value = $(this).val();
            values = value;
            let kategoris = $('.kategori option:selected').val();
             if(values != "Pria" && kategoris != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                values = $('.gender option:selected').val();

                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })


    @foreach ($nilai as $item)
            var e_gender{{$item->id}} = ""
            var e_kat{{$item->id}} = 0;
        $('.edit-gender{{$item->id}}').change(function(){
            let value = $(this).val();
            e_gender{{$item->id}} = value;
            let kategoris = $('.edit-kategori{{$item->id}} option:selected').val();
             if(e_gender{{$item->id}} != "Pria" && kategoris != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.edit-kategori{{$item->id}} ').change(function(){
                let kategori = $(this).val();
                e_kat{{$item->id}} = kategori;
                if(e_gender{{$item->id}} != "Pria" && kategori != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.edit-kategori{{$item->id}} ').change(function(){
                let kategori = $(this).val();
                e_kat{{$item->id}} = kategori;
                e_gender{{$item->id}} = $('.edit-gender{{$item->id}} option:selected').val();
                if(e_gender{{$item->id}} != "Pria" && kategori != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

    @endforeach  --}}

    });

    var klik = 0;
    function tambah_kategori(index){
         klik += 1;
        $('.coaching-'+index).append(
            '<div class="form-group  m-form__group kategori-'+klik+' " >'+
                '<input type="hidden" name="coaching[]" value="'+index+'">'+
                            '<div data-repeater-list="">'+
                                '<div data-repeater-item class="form-group m-form__group">'+
                                    '<div class="form-group">'+
                                        '<label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>'+
                                        '<textarea class="form-control" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3"  required ></textarea>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="m-form__group form-group row">'+
                                            '<div class="col-md-6 m--margin-bottom-10">'+
                                                '<label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>'+
                                                '<input type="text" class="form-control allownumericwithdecimal" id="recipient-name" name="poin[]"  min="0" required >'+
                                            '</div>'+
                                            '<div class="col-md-6 align-self-center" >'+
                                                '<div data-repeater-delete="" class="btn-md btn btn-danger m-btn m-btn--icon m-btn--pill float-right hapus-kategori" data-index="'+klik+'">'+
                                                    '<span>'+
                                                        '<i class="la la-trash-o"></i>'+
                                                        '<span>Delete</span>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+
                          '</div>'+
                    '</div>'
        );

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

        $('.hapus-kategori').click(function(){
            let data_index = $(this).data('index');
            $('.kategori-'+data_index).slideUp("normal" , function(){
                $(this).remove();
            });
        })


    }

    var kliks = 0;
    function tambah_kategori_dis(index){
         kliks += 1;
        $('.coaching-dis'+index).append(
            '<div class="form-group  m-form__group kategori-dis'+kliks+' " >'+
                '<input type="hidden" name="coaching[]" value="'+index+'">'+
                            '<div data-repeater-list="">'+
                                '<div data-repeater-item class="form-group m-form__group">'+
                                    '<div class="form-group">'+
                                        '<label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>'+
                                        '<textarea class="form-control" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3"  required ></textarea>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="m-form__group form-group row">'+
                                            '<div class="col-md-6 m--margin-bottom-10">'+
                                                '<label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>'+
                                                '<input type="text" class="form-control allownumericwithdecimal" id="recipient-name" name="poin[]"  min="0" value="1000" required readonly>'+
                                            '</div>'+
                                            '<div class="col-md-6 align-self-center" >'+
                                                '<div data-repeater-delete="" class="btn-md btn btn-danger m-btn m-btn--icon m-btn--pill float-right hapus-kategori-dis" data-index="'+kliks+'">'+
                                                    '<span>'+
                                                        '<i class="la la-trash-o"></i>'+
                                                        '<span>Delete</span>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+
                          '</div>'+
                    '</div>'
        );

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

        $('.hapus-kategori-dis').click(function(){
            let data_index = $(this).data('index');
            $('.kategori-dis'+data_index).slideUp("normal" , function(){
                $(this).remove();
            });
        })


    }

    function dis(value, index){
        if(value == "yes"){
            $('.poin-des-'+index).val('1000');
            $('.poin-des-'+index).prop('readonly', true);
        }
        else{
            $('.poin-des-'+index).val('');
            $('.poin-des-'+index).removeAttr('readonly');
        }
    }

var initTableHasil = function() {

		// begin first table
		var table = $('#m_table_hasil').DataTable({
			responsive: true,

			buttons: [
				{extend: 'print' ,title: 'Hasil Ujian Tanggal {{$tgl}} ', messageTop: 'Nama Peserta : {{$user_name->name}} , No.Punggung : {{$user_name->no_punggung}} , Juri : {{$nama_juri->user->name}}'},
				'copyHtml5',
				{extend: 'excelHtml5' , title: 'Hasil Ujian Tanggal {{$tgl}} ' , messageTop: 'Nama Peserta : {{$user_name->name}} , No.Punggung : {{$user_name->no_punggung}} , Juri : {{$nama_juri->user->name}}' } ,
				'csvHtml5',
				'pdfHtml5',
			],
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});

};


var report = function() {

var initTableHasil = function() {

		// begin first table
		var table = $('#m_table_hasil').DataTable({
			responsive: true,

			buttons: [
				{extend: 'print' ,title: 'Hasil Ujian Tanggal {{$tgl}} Modul {{$nama_juri->modul->name}} ', messageTop: 'Nama Peserta : {{$user_name->name}} , No.Punggung : {{$user_name->no_punggung}} , Juri : {{$nama_juri->user->name}}'},
				'copyHtml5',
				{extend: 'excelHtml5' , title: 'Hasil Ujian Tanggal {{$tgl}} Modul {{$nama_juri->modul->name}} ' , messageTop: 'Nama Peserta : {{$user_name->name}} , No.Punggung : {{$user_name->no_punggung}} , Juri : {{$nama_juri->user->name}}' } ,
				'csvHtml5',
				'pdfHtml5',
			],
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});

};


	return {

		//main function to initiate the module
		init: function() {
		   initTableHasil();
		},

	};

}();

jQuery(document).ready(function() {
	report.init();
});
</script>
@endsection
