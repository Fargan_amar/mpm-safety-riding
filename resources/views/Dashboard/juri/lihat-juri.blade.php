@extends('layouts.dashboard')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">Data Penjurian Modul {{$modul->name}} {{$tgl}} </h3>
                        </div>

                    </div>
                </div>

                <!-- END: Subheader -->
                    <div class="m-content">

                        <div class="m-portlet m-portlet--tabs">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <ul class="m-portlet__nav nav">
                                            <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                    Pilih Waktu
                                                </a>
                                                <div class="m-dropdown__wrapper" style="z-index: 101;left:10%">
                                                    <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
                                                    <div class="m-dropdown__inner">
                                                        <div class="m-dropdown__body">
                                                            <div class="m-dropdown__content">
                                                                <ul class="m-nav">
                                                                    <li class="m-nav__section m-nav__section--first">
                                                                        <span class="m-nav__section-text">Pilih Waktu</span>
                                                                    </li>
                                                                    @foreach ($juri as $item)
                                                                        <li class="m-nav__item">
                                                                            <a href="{{url('penjurian' , ['tgl' => $item->tgl , 'id' => $item->id])}}" class="m-nav__link" >
                                                                                <span class="m-nav__link-text">({{date('H:i', strtotime($item->tgl))}}) {{$item->deskripsi}} - Sesi {{$loop->iteration}}</span>
                                                                            </a>
                                                                        </li>
                                                                    @endforeach

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>



                                        </ul>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">

                                </div>
                            </div>
                            <div class="m-portlet__body">


                            </div>
                        </div>

                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
            </div>
        </div>
    </div>

<!--begin::Modal-->


<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>

<script>

    $(function(){
        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

        @if (session('sukses'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan penilaian baru untuk peserta  ");
        @endif
        @if (session('peserta'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.error("Kategori yang dipilih tidak memiliki peserta, silahkan tambah peserta dengan kategori tersebut terlebih dahulu !  ");

        @endif
        @if (session('update'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah data penilaian  ");
        @endif
        @if (session('delete'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menghapus data modul  ");
        @endif
        @if (session('penilaian'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan penilaian baru ");
        @endif
        {{--  var values = "";
        var kat = 0;
        $('.gender').change(function(){
            let value = $(this).val();
            values = value;
            let kategoris = $('.kategori option:selected').val();
             if(values != "Pria" && kategoris != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                values = $('.gender option:selected').val();

                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })


    @foreach ($nilai as $item)
            var e_gender{{$item->id}} = ""
            var e_kat{{$item->id}} = 0;
        $('.edit-gender{{$item->id}}').change(function(){
            let value = $(this).val();
            e_gender{{$item->id}} = value;
            let kategoris = $('.edit-kategori{{$item->id}} option:selected').val();
             if(e_gender{{$item->id}} != "Pria" && kategoris != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.edit-kategori{{$item->id}} ').change(function(){
                let kategori = $(this).val();
                e_kat{{$item->id}} = kategori;
                if(e_gender{{$item->id}} != "Pria" && kategori != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.edit-kategori{{$item->id}} ').change(function(){
                let kategori = $(this).val();
                e_kat{{$item->id}} = kategori;
                e_gender{{$item->id}} = $('.edit-gender{{$item->id}} option:selected').val();
                if(e_gender{{$item->id}} != "Pria" && kategori != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

    @endforeach  --}}

    });

    var klik = 0;
    function tambah_kategori(index){
         klik += 1;
        $('.coaching-'+index).append(
            '<div class="form-group  m-form__group kategori-'+klik+' " >'+
                '<input type="hidden" name="coaching[]" value="'+index+'">'+
                            '<div data-repeater-list="">'+
                                '<div data-repeater-item class="form-group m-form__group">'+
                                    '<div class="form-group">'+
                                        '<label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>'+
                                        '<textarea class="form-control" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3"  required ></textarea>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="m-form__group form-group row">'+
                                            '<div class="col-md-6 m--margin-bottom-10">'+
                                                '<label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>'+
                                                '<input type="text" class="form-control allownumericwithdecimal" id="recipient-name" name="poin[]"  min="0" required >'+
                                            '</div>'+
                                            '<div class="col-md-6 align-self-center" >'+
                                                '<div data-repeater-delete="" class="btn-md btn btn-danger m-btn m-btn--icon m-btn--pill float-right hapus-kategori" data-index="'+klik+'">'+
                                                    '<span>'+
                                                        '<i class="la la-trash-o"></i>'+
                                                        '<span>Delete</span>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+
                          '</div>'+
                    '</div>'
        );

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

        $('.hapus-kategori').click(function(){
            let data_index = $(this).data('index');
            $('.kategori-'+data_index).slideUp("normal" , function(){
                $(this).remove();
            });
        })


    }

    var kliks = 0;
    function tambah_kategori_dis(index){
         kliks += 1;
        $('.coaching-dis'+index).append(
            '<div class="form-group  m-form__group kategori-dis'+kliks+' " >'+
                '<input type="hidden" name="coaching[]" value="'+index+'">'+
                            '<div data-repeater-list="">'+
                                '<div data-repeater-item class="form-group m-form__group">'+
                                    '<div class="form-group">'+
                                        '<label for="exampleFormControlTextarea1" class="form-control-label">Deskripsi</label>'+
                                        '<textarea class="form-control" name="deskripsi[]" id="exampleFormControlTextarea1" rows="3"  required ></textarea>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="m-form__group form-group row">'+
                                            '<div class="col-md-6 m--margin-bottom-10">'+
                                                '<label for="recipient-name" class="form-control-label">Poin Pengurangan : </label>'+
                                                '<input type="text" class="form-control allownumericwithdecimal" id="recipient-name" name="poin[]"  min="0" value="1000" required readonly>'+
                                            '</div>'+
                                            '<div class="col-md-6 align-self-center" >'+
                                                '<div data-repeater-delete="" class="btn-md btn btn-danger m-btn m-btn--icon m-btn--pill float-right hapus-kategori-dis" data-index="'+kliks+'">'+
                                                    '<span>'+
                                                        '<i class="la la-trash-o"></i>'+
                                                        '<span>Delete</span>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+
                          '</div>'+
                    '</div>'
        );

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
        });

        $('.hapus-kategori-dis').click(function(){
            let data_index = $(this).data('index');
            $('.kategori-dis'+data_index).slideUp("normal" , function(){
                $(this).remove();
            });
        })


    }

    function dis(value, index){
        if(value == "yes"){
            $('.poin-des-'+index).val('1000');
            $('.poin-des-'+index).prop('readonly', true);
        }
        else{
            $('.poin-des-'+index).val('');
            $('.poin-des-'+index).removeAttr('readonly');
        }
    }
</script>
@endsection
