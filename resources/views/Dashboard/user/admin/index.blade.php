@extends('layouts.dashboard')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">Data Admin</h3>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>

                <!-- END: Subheader -->
                    <div class="m-content">

                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            {{-- Dropdown Export Tools --}}
                                        </h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a href="#" class="btn btn-danger m-btn m-btn--custom m-btn--icon m-btn--air" data-toggle="modal" data-target="#m_modal_4">
                                                <span>
                                                    <i class="la la-plus-circle"></i>
                                                    <span>Tambah Admin</span>
                                                </span>
                                            </a>
                                        </li>

                                        <li class="m-portlet__nav-item"></li>
                                        <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                            <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                Export Options
                                            </a>
                                            <div class="m-dropdown__wrapper" style="z-index: 101;">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36px;"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            <ul class="m-nav">
                                                                <li class="m-nav__section m-nav__section--first">
                                                                    <span class="m-nav__section-text">Export Tools</span>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_print">
                                                                        <i class="m-nav__link-icon la la-print"></i>
                                                                        <span class="m-nav__link-text">Print</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_copy">
                                                                        <i class="m-nav__link-icon la la-copy"></i>
                                                                        <span class="m-nav__link-text">Copy</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_excel">
                                                                        <i class="m-nav__link-icon la la-file-excel-o"></i>
                                                                        <span class="m-nav__link-text">Excel</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_csv">
                                                                        <i class="m-nav__link-icon la la-file-text-o"></i>
                                                                        <span class="m-nav__link-text">CSV</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_pdf">
                                                                        <i class="m-nav__link-icon la la-file-pdf-o"></i>
                                                                        <span class="m-nav__link-text">PDF</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body">

                                <!--begin: Datatable -->
                                <table class="table table-striped-table-bordered table-hover table-checkable responsive no-wrap" id="m_table_2">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Gender</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($user as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>{{$item->gender}}</td>
                                            <td nowrap="">
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                                                    <i class="la la-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_edit_{{$item->id}}"><i class="la la-edit"></i> Edit Details</a>
                                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_hapus_{{$item->id}}"><i class="la  la-trash-o"></i> Delete Data</a>
                                                        {{--  <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>  --}}

                                                    </div>
                                                </span>
                                            </td>
                                        </tr>
                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
            </div>
        </div>
    </div>


<!--begin::Modal-->
<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="tambah" action="{{url('/tambah-admin')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nama:</label>
                        <input type="text" class="form-control" id="recipient-name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">E-mail:</label>
                        <input type="email" class="form-control" id="recipient-name" name="email" placeholder="optional">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Role</label>
                        <select class="form-control m-input jabatan" id="exampleSelect1" name="jabatan_id">
                            @foreach ($jabatan as $item)
                                @if(Auth::user()->jabatan_id == 1)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @else
                                    @if($item->id == 1)
                                    @else
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endif
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="m-form__group form-group row admin">
                        <label class="col-3 col-form-label">Jadikan User ini sebagai admin ? </label>
                        <div class="col-2">
                            <span class="m-switch m-switch--icon">
                                <label>
                                    <input type="checkbox" class="is_admin" checked="checked" name="is_admin">
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                    <div class="alert alert-info"  role="alert">
                        <strong>Info</strong> Kosongkan no. Punggung jika jabatan yang anda pilih 'admin'.
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">No. Punggung:</label>
                        <input type="number" class="form-control" id="recipient-name" name="no_punggung" oninput="validity.valid||(value='');" min="1" value="{{$item->no_punggung}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Gender</label>
                        <select class="form-control m-input gender" id="exampleSelect1" name="gender"   required>
                            <option value=""></option>
                            <option value="Pria">Pria</option>
                            <option value="Wanita">Wanita</option>
                        </select>
                    </div>
                    <div class="alert alert-danger" id="alert-kategori" role="alert">
                        <strong>Oh snap!</strong> Gunakan kategori "non-kategori" untuk jabatan 'admin'.
                    </div>
                    <div class="form-group form-kategori">
                        <label for="exampleSelect1">Kategori</label>
                        <select class="form-control m-input kategori" id="exampleSelect1" name="kategori_id" required>
                            <option value=""></option>
                            @foreach ($kategori as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@foreach ($user as $item)
<div class="modal fade" id="modal_edit_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="tambah" action="{{url('/edit-admin')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nama:</label>
                        <input type="text" class="form-control" id="recipient-name" name="name" value="{{$item->name}}" required readonly>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">E-mail:</label>
                        <input type="email" class="form-control" id="recipient-name" name="email" placeholder="optional" value="{{$item->email}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Role</label>
                        <select class="form-control m-input jabatan{{$item->id}}" id="exampleSelect1" name="jabatan_id">
                            <option value="{{$item->jabatan_id}}">{{$item->jabatan->name}}</option>
                            @foreach ($jabatan as $j)
                                <option value="{{$j->id}}">{{$j->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="m-form__group form-group row admin{{$item->id}}">
                        <label class="col-3 col-form-label">Jadikan User ini sebagai admin ? </label>
                        <div class="col-2">
                            <span class="m-switch m-switch--icon">
                                <label>
                                    @if ($item->is_admin == 1)
                                    <input type="checkbox" class="is_admin" checked="checked" name="is_admin">

                                    @else
                                    <input type="checkbox" class="is_admin" checked="" name="is_admin">

                                    @endif
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                    <div class="alert alert-info"  role="alert">
                        <strong>Info</strong> Kosongkan no. Punggung  jika jabatan yang anda pilih 'admin'.
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">No. Punggung:</label>
                        <input type="number" class="form-control" id="recipient-name" name="no_punggung" oninput="validity.valid||(value='');" min="1" value="{{$item->no_punggung}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Gender</label>
                        <select class="form-control m-input edit-gender{{$item->id}}" id="exampleSelect1" name="gender"   required>
                            <option value="{{$item->gender}}" selected>{{$item->gender}}</option>
                            @if ($item->gender == "Pria")
                            <option value="Wanita">Wanita</option>
                            @endif
                            @if ($item->gender == "Wanita")
                            <option value="Pria">Pria</option>
                            @endif
                        </select>
                    </div>
                    <div class="alert alert-danger" id="alert-kategori{{$item->id}}" role="alert">
                        <strong>Oh snap!</strong> Gunakan kategori "non-kategori" untuk jabatan 'admin'.
                    </div>
                    <div class="form-group form-edit-kategori{{$item->id}}">
                        <label for="exampleSelect1">Kategori</label>
                        <select class="form-control m-input edit-kategori{{$item->id}}" id="exampleSelect1" name="kategori_id" required>
                            <option value="{{$item->kategori_id}}" selected>{{$item->kategori->name}}</option>
                            @foreach ($kategori as $k)
                                @if ($item->kategori_id == $k->id )
                                <option value="{{$k->id}}" style="display:none">{{$k->name}}</option>
                                @else
                                <option value="{{$k->id}}" >{{$k->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_hapus_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('/delete-user')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <h6 class="m-badge m-badge--danger m-badge--wide">Dengan menghapus data admin ini, maka data-data seperti nilai dan record data ujian yang telah dilakukan oleh admin ini akan terhapus semua </h6>
                    <h5>Apakah Anda Yakin Akan Menghapus Peserta <span class="m--font-danger">'{{$item->name}}'</span> Dari Database ?</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-danger">Ya, saya ingin menghapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach
<!--end::Modal-->
<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>

<script>
    $(function(){

        @if (session('sukses'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan user/admin baru ");
        @endif
        @if (session('error'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.error("Email admin/peserta Tersebut Sudah Ada !!!  ");

        @endif
        @if (session('update'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah data peserta/admin  ");
        @endif
        @if (session('delete'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menghapus data peserta/admin  ");
        @endif
        var values = "";
        var kat = 0;
        $('.gender').change(function(){
            let value = $(this).val();
            values = value;
            let kategoris = $('.kategori option:selected').val();
             if(values != "Pria" && kategoris != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                values = $('.gender option:selected').val();

                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })
            $('.admin').hide();
            $('#alert-kategori').hide();
            $('.jabatan').change(function(){
                let jabatan = $(this).val();
                if(jabatan == 3)
                {
                    $('.admin').show();
                    $('#alert-kategori').hide();
                }
                else
                {
                    $('.admin').hide();
                    $('#alert-kategori').show();
                }
            })


    @foreach ($user as $item)
            var e_gender{{$item->id}} = ""
            var e_kat{{$item->id}} = 0;
        $('.edit-gender{{$item->id}}').change(function(){
            let value = $(this).val();
            e_gender{{$item->id}} = value;
            let kategoris = $('.edit-kategori{{$item->id}} option:selected').val();
             if(e_gender{{$item->id}} != "Pria" && kategoris != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.edit-kategori{{$item->id}} ').change(function(){
                let kategori = $(this).val();
                e_kat{{$item->id}} = kategori;
                if(e_gender{{$item->id}} != "Pria" && kategori != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.edit-kategori{{$item->id}} ').change(function(){
                let kategori = $(this).val();
                e_kat{{$item->id}} = kategori;
                e_gender{{$item->id}} = $('.edit-gender{{$item->id}} option:selected').val();
                if(e_gender{{$item->id}} != "Pria" && kategori != 4 ){
                    $('.form-edit-kategori{{$item->id}} select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

            @if ($item->jabatan->name == "Peserta")
            $('.admin{{$item->id}}').show();

            @else
            $('.admin{{$item->id}}').hide();
            @endif
            $('#alert-kategori{{$item->id}}').hide();
            $('.jabatan{{$item->id}}').change(function(){
                let jabatan = $(this).val();
                if(jabatan == 3)
                {
                    $('.admin{{$item->id}}').show();
                    $('#alert-kategori{{$item->id}}').hide();
                }
                else
                {
                    $('.admin{{$item->id}}').hide();
                    $('#alert-kategori{{$item->id}}').show();
                }
            })

    @endforeach

    });
</script>
@endsection
