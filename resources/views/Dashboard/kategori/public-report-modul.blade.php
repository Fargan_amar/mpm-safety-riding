@extends('layouts.public-dashboard')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                           <h5> Report Data Kategori {{$kategori->name}} Modul {{$nama_modul->name}} Bulan {{date("F" , mktime(0, 0, 0, $bulan, 10))}} Tgl  {{$tgl}} - Tahun {{$tahun}} </h5>
                        </div>
                        <div>
                            {{--  <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                                <span class="m-subheader__daterange-label">
                                    <span class="m-subheader__daterange-title"></span>
                                    <span class="m-subheader__daterange-date m--font-brand"></span>
                                </span>
                                <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="la la-angle-down"></i>
                                </a>
                            </span>  --}}
                        </div>
                    </div>
                </div>

                <!-- END: Subheader -->
                    <div class="m-content">

                        <div class="m-portlet  m-portlet--tabs">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            {{-- Dropdown Export Tools --}}
                                            <div class="m-portlet__head-tools">
                                                <ul class="m-portlet__nav nav">
                                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                            Pilih Kategori
                                                        </a>
                                                        <div class="m-dropdown__wrapper" style="z-index: 101;left:10%">
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            <li class="m-nav__section m-nav__section--first">
                                                                                <span class="m-nav__section-text">Daftar Kategori</span>
                                                                            </li>
                                                                            @foreach ($data_kategori as $k)
                                                                                <li class="m-nav__item">
                                                                                    <a href="{{url('laporan-kategori' , [ 'token' => $token , 'id' => $k , 'modul' => $nama_modul , 'tahun' => $tahun , 'bulan' => $bulan , 'tgl' => $tgl ])}}" class="m-nav__link" >
                                                                                        <span class="m-nav__link-text">{{$k->name}}
                                                                                        @if ($k->id == $id)
                                                                                            <i class="la  la-check-circle m--font-success "></i>

                                                                                        @else

                                                                                        @endif
                                                                                        </span>
                                                                                    </a>
                                                                                </li>
                                                                            @endforeach

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                            Pilih Modul
                                                        </a>
                                                        <div class="m-dropdown__wrapper" style="z-index: 101;left:10%">
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            <li class="m-nav__section m-nav__section--first">
                                                                                <span class="m-nav__section-text">Daftar Modul</span>
                                                                            </li>
                                                                            @foreach ($modul as $item)
                                                                                <li class="m-nav__item">
                                                                                    <a href="{{url('laporan-kategori' , [ 'token' => $token ,'id' => $id , 'modul' => $item->id , 'tahun' => $tahun , 'bulan' => $bulan , 'tgl' => $tgl ])}}" class="m-nav__link" >
                                                                                        <span class="m-nav__link-text">{{$item->name}}
                                                                                        @if ($item->id == $nama_modul->id)
                                                                                            <i class="la  la-check-circle m--font-success "></i>

                                                                                        @else

                                                                                        @endif
                                                                                        </span>
                                                                                    </a>
                                                                                </li>
                                                                            @endforeach

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                            Pilih Tahun
                                                        </a>
                                                        <div class="m-dropdown__wrapper" style="z-index: 101;left:10%">
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            <li class="m-nav__section m-nav__section--first">
                                                                                <span class="m-nav__section-text"></span>
                                                                            </li>
                                                                            @for ($i = date('Y') - 5; $i < date('Y') + 5; $i++)
                                                                                <li class="m-nav__item">
                                                                                    <a href="{{url('laporan-kategori' , ['token' => $token , 'id' => $id , 'modul' => $nama_modul->id , 'tahun' => $i , 'bulan' => $bulan , 'tgl' => $tgl])}}" class="m-nav__link" >
                                                                                        <span class="m-nav__link-text">{{$i}}
                                                                                        @if ($tahun == $i)
                                                                                            <i class="la  la-check-circle m--font-success "></i>

                                                                                        @else

                                                                                        @endif
                                                                                        </span>
                                                                                    </a>
                                                                                </li>

                                                                            @endfor



                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                            Pilih Bulan
                                                        </a>
                                                        <div class="m-dropdown__wrapper" style="z-index: 101;left:10%">
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            <li class="m-nav__section m-nav__section--first">
                                                                                <span class="m-nav__section-text"></span>
                                                                            </li>
                                                                            @foreach ($array_bulan as $item)
                                                                                <li class="m-nav__item">
                                                                                    <a href="{{url('laporan-kategori' , [ 'token' => $token , 'id' => $id , 'modul' => $nama_modul->id , 'tahun' => $tahun , 'bulan' => $loop->iteration , 'tgl' => $tgl])}}" class="m-nav__link" >
                                                                                        <span class="m-nav__link-text">{{$item}}
                                                                                        @if ($bulan == $loop->iteration)
                                                                                            <i class="la  la-check-circle m--font-success "></i>

                                                                                        @else

                                                                                        @endif
                                                                                        </span>
                                                                                    </a>
                                                                                </li>
                                                                            @endforeach

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                            Pilih Tanggal
                                                        </a>
                                                        <div class="m-dropdown__wrapper" style="z-index: 101;left:10%">
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            <li class="m-nav__section m-nav__section--first">
                                                                                <span class="m-nav__section-text"></span>
                                                                            </li>
                                                                            @for ($i = 1; $i < 32; $i++)
                                                                                <li class="m-nav__item">
                                                                                    <a href="{{url('laporan-kategori' , [ 'token' => $token , 'id' => $id , 'modul' => $nama_modul->id , 'tahun' => $tahun , 'bulan' => $bulan , 'tgl' => $i])}}" class="m-nav__link" >
                                                                                        <span class="m-nav__link-text">{{$i}}
                                                                                        @if ($tgl == $i)
                                                                                            <i class="la  la-check-circle m--font-success "></i>

                                                                                        @else

                                                                                        @endif
                                                                                        </span>
                                                                                    </a>
                                                                                </li>
                                                                            @endfor

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>


                                                </ul>
                                            </div>
                                        </h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="nav nav-tabs m-tabs m-tabs-line  m-tabs-line--right  m-tabs-line-danger" role="tablist">
                                        {{--  <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_portlet_tab_3_1" role="tab">
                                                <i class="la  la-area-chart "></i>
                                                Grafik
                                            </a>
                                        </li>  --}}
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_3_3" role="tab">
                                                <i class="la la-trophy "></i>
                                                Ranking
                                            </a>
                                        </li>
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_3_2" role="tab">
                                                <i class="la  la-table "></i>
                                                Table
                                            </a>
                                        </li>
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_3_4" role="tab">
                                                <i class="la  la-link "></i>
                                                Share Link
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
@if (sizeof($rekap) > 0)

                            <div class="m-portlet__body" >
                                <div class="tab-content">
                                    <div class="tab-pane " id="" >
                                        <div id="chart" style="height:100vh;width:100%;">

                                        </div>
                                        <div style='margin-top: 10px; margin-left: 1rem' class="form-group m-form__group row">
                                            <input style='float: left;' id="jpegButton" type="button" value="Save As JPEG" />
                                            <input style='float: left; margin-left: 5px;' id="pngButton" type="button" value="Save As PNG" />
                                            <input style='float: left; margin-left: 5px;' id="pdfButton" type="button" value="Save As PDF" />
                                        </div>
                                    </div>

                                    <div class="tab-pane " id="m_portlet_tab_3_2">
                                            <!--begin: Datatable -->
                                            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal</th>
                                                        <th>Poin</th>
                                                        <th>Detail</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($rekap as $item)
                                                    <tr>
                                                        <td>{{$loop->iteration}}</td>
                                                        <td>{{date("d-M-Y G:i" , strtotime($item->tgl))}}</td>
                                                        <td>{{$item->poin}}</td>
                                                        <td nowrap="">
                                                            <span class="dropdown">
                                                                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                                                                <i class="la la-ellipsis-h"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="{{url('/lihat-report-tgl' , ['token' => $token ,'modul' => $nama_modul->id, 'tgl' => $item->tgl ]) }}" ><i class="la  la-history "></i>Lihat Detail</a>
                                                                </div>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                        @endforeach
                                                </tbody>
                                            </table>
                                    </div>
                                    <div class="tab-pane active" id="m_portlet_tab_3_3" style="text-align: center">
                                         <a href="{{url('ranking-peserta' , ['token' => $token , 'id' => $id ,  'tahun' => $tahun , 'bulan' => $bulan , 'tgl' => $tgl])}}" class="m-nav__link" ><button type="button" class="btn m-btn--pill m-btn--air btn-outline-success btn-lg">Lihat Ranking</button></a>

                                        </div>
                                        <div class="tab-pane " id="m_portlet_tab_3_4">
                                            <div class="form-group m-form__group row">
                                                <label class="col-form-label col-lg-3 col-sm-12">Copy This Link and Share It </label>
                                                <div class="col-lg-6 col-md-9 col-sm-12">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="m_clipboard_1" value="{{url('laporan-kategori' , ['token' => $token , 'id' => $id , 'modul' => $nama_modul->id , 'tahun' => $tahun , 'bulan' => $bulan , 'tgl' => $tgl ])}}" >
                                                        <div class="input-group-append">
                                                            <a href="#" class="btn btn-secondary" data-clipboard="true" data-clipboard-target="#m_clipboard_1"><i class="la la-copy"></i></a>
                                                        </div>
											</div>
										</div>
									</div>
                                    </div>

                                </div>



                            </div>
@endif

                        </div>

                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
            </div>
        </div>
    </div>


<!--begin::Modal-->

<!--end::Modal-->
<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>

<script>
    $(function(){
            function getExportServer() {
                return 'https://www.jqwidgets.com/export_server/export.php';
            }

@if (sizeof($rekap) > 0)
                var  sampleData = [
                @foreach ($rekap as  $key =>$item)
                { Tanggal:'{{date("d-M-Y G:i" , strtotime($item->tgl))}}',  @php  $index_poin = 0; @endphp@for ($index = 0 ; $index < $rekap[$key]->jml_peserta ; $index++) @php $nama = "nama".$index; $poin = "poin".$index_poin++; @endphp {{$rekap[$key]->$nama}}: {{$rekap[$key]->$poin}} , @endfor },


                @endforeach
              ];

            // prepare jqxChart settings

 var settings = {
                title: "Grafik Modul {{$nama_modul->name}} Bulan {{date('F' , mktime(0, 0, 0, $bulan, 10))}} Tgl {{$tgl}} ",
                description: "",
                enableAnimations: true,
                showLegend: true,
                padding: { left: 10, top: 10, right: 15, bottom: 10 },
                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
                source: sampleData,
                colorScheme: 'scheme05',
                categoryAxis:
                    {
                        dataField: 'Tanggal',
                        showGridLines: false
                    },
                valueAxis: {
                    unitInterval: 100,
                    minValue: -100,
                    maxValue: 1100,
                    title: { text: 'Poin<br><br>' },
                    labels: { horizontalAlignment: 'right' }
                },
                seriesGroups:
                    [
                        {
                            type: 'line',
                            symbolType: 'square',
                            series:
                            [
                                @for ($i = 0; $i < sizeof($array_peserta); $i++)
                                @php $nama = "nama".$i; @endphp
                                {

                                    dataField: '{{$array_peserta[$i]}}', displayText:  '{{$array_peserta[$i]}}',
                                },
                                @endfor

                            ]
                        }
                    ]
            };
            {{--  var settings = {
                title: "Fitness & exercise weekly scorecard",
                description: "Time spent in vigorous exercise",
                padding: { left: 5, top: 5, right: 5, bottom: 5 },
                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
                source: sampleData,
                categoryAxis:
                    {
                        dataField: 'Tanggal',
                        showGridLines: false,

                    },
                colorScheme: 'scheme01',
                seriesGroups:
                    [
                        {
                            type: 'line',
                            columnsGapPercent: 30,
                            seriesGapPercent: 0,
                            valueAxis:
                            {
                                minValue: 0,
                                maxValue: 1500,
                                unitInterval: 100,
                                description: 'Time in minutes'
                            },
                            series: [
                                    { dataField: 'Poin', displayText: 'Poin'}
                                ]
                        }
                    ]
            };  --}}

            // select the chartContainer DIV element and render the chart.
            $('#chart').jqxChart(settings);
            $("#jpegButton").jqxButton({});
            $("#pngButton").jqxButton({});
            $("#pdfButton").jqxButton({});
            $("#jpegButton").click(function () {
                // call the export server to create a JPEG image
                $('#chart').jqxChart('saveAsJPEG', 'mpmsr-chart.jpeg', getExportServer());
            });
            $("#pngButton").click(function () {
                // call the export server to create a PNG image
                $('#chart').jqxChart('saveAsPNG', 'mpmsr-chart.png', getExportServer());
            });
            $("#pdfButton").click(function () {
                // call the export server to create a PNG image
                $('#chart').jqxChart('saveAsPDF', 'mpmsr-chart.pdf', getExportServer());
            });
@endif


        @if (session('sukses'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan peserta baru ");
        @endif
        @if (session('error'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.error("Email Peserta Tersebut Sudah Ada !!!  ");

        @endif
        @if (session('update'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah data peserta  ");
        @endif
        @if (session('delete'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menghapus data peserta  ");
        @endif
        var values = "";
        var kat = 0;
        $('.gender').change(function(){
            let value = $(this).val();
            values = value;
            let kategoris = $('.kategori option:selected').val();
             if(values != "Pria" && kategoris != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                values = $('.gender option:selected').val();

                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })




    });

    var report = function() {

    var initTableReportModul = function() {

            // begin first table
            var table = $('#m_table_ranking').DataTable({
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": [0 ,1,2,3,4,5,6,7,8]
                }],
                "order": [[9, 'desc']],
            });

    };


	return {

		//main function to initiate the module
		init: function() {
            initTableReportModul();
			// initTableHasil();
		},

	};

}();
jQuery(document).ready(function() {
	report.init();
});
</script>
@endsection
