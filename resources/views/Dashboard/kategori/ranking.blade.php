@extends('layouts.dashboard')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                           <h5> Report Rangking Kategori {{$kategori->name}} Bulan {{date("F" , mktime(0, 0, 0, $bulan, 10))}} Tgl  {{$tgl}} - Tahun {{$tahun}} </h5>
                        </div>
                        <div>
                            {{--  <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                                <span class="m-subheader__daterange-label">
                                    <span class="m-subheader__daterange-title"></span>
                                    <span class="m-subheader__daterange-date m--font-brand"></span>
                                </span>
                                <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="la la-angle-down"></i>
                                </a>
                            </span>  --}}
                        </div>
                    </div>
                </div>

                <!-- END: Subheader -->
                    <div class="m-content">

                        <div class="m-portlet  m-portlet--tabs">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            {{-- Dropdown Export Tools --}}
                                            <div class="m-portlet__head-tools">
                                                <ul class="m-portlet__nav nav">

                                                    <li class="m-portlet__nav-item">
                                                        <a href="{{url()->previous()}}" class="m-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air">
                                                        <i class="la  la-arrow-left "></i> back
                                                        </a>
                                                    </li>
                                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                            Pilih Kategori
                                                        </a>
                                                        <div class="m-dropdown__wrapper" style="z-index: 101;left:10%">
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            <li class="m-nav__section m-nav__section--first">
                                                                                <span class="m-nav__section-text">Daftar Kategori</span>
                                                                            </li>
                                                                            @foreach ($data_kategori as $k)
                                                                                <li class="m-nav__item">
                                                                                    <a href="{{url('ranking' , [ 'id' => $k , 'tahun' => $tahun , 'bulan' => $bulan , 'tgl' => $tgl ])}}" class="m-nav__link" >
                                                                                        <span class="m-nav__link-text">{{$k->name}}
                                                                                        @if ($k->id == $id)
                                                                                            <i class="la  la-check-circle m--font-success "></i>

                                                                                        @else

                                                                                        @endif
                                                                                        </span>
                                                                                    </a>
                                                                                </li>
                                                                            @endforeach

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                            Pilih Bulan
                                                        </a>
                                                        <div class="m-dropdown__wrapper" style="z-index: 101;left:10%">
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            <li class="m-nav__section m-nav__section--first">
                                                                                <span class="m-nav__section-text">Daftar Bulan</span>
                                                                            </li>
                                                                            @foreach ($array_bulan as $item)
                                                                                <li class="m-nav__item">
                                                                                    <a href="{{url('ranking' , [ 'id' => $id , 'tahun' => $tahun , 'bulan' => $loop->iteration , 'tgl' => $tgl ])}}" class="m-nav__link" >
                                                                                        <span class="m-nav__link-text">{{$item}}
                                                                                        @if ($bulan == $loop->iteration)
                                                                                            <i class="la  la-check-circle m--font-success "></i>

                                                                                        @else

                                                                                        @endif
                                                                                        </span>
                                                                                    </a>
                                                                                </li>
                                                                            @endforeach

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                            Pilih Tanggal
                                                        </a>
                                                        <div class="m-dropdown__wrapper" style="z-index: 101;left:10%">
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            <li class="m-nav__section m-nav__section--first">
                                                                                <span class="m-nav__section-text">Daftar Tanggal</span>
                                                                            </li>
                                                                            @foreach ($date as $item)
                                                                                <li class="m-nav__item">
                                                                                    <a href="{{url('ranking' , [ 'id' => $id , 'tahun' => $tahun , 'bulan' => $loop->iteration , 'tgl' => date('d' , strtotime($item->tgl_calendar)) ])}}" class="m-nav__link" >
                                                                                        <span class="m-nav__link-text">{{date('d' , strtotime($item->tgl_calendar))}}
                                                                                        @if ($tgl == date('d' , strtotime($item->tgl_calendar)))
                                                                                            <i class="la  la-check-circle m--font-success "></i>

                                                                                        @else

                                                                                        @endif
                                                                                        </span>
                                                                                    </a>
                                                                                </li>
                                                                            @endforeach

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                        </h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="nav nav-tabs m-tabs m-tabs-line  m-tabs-line--right  m-tabs-line-danger" role="tablist">
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_portlet_tab_3_1" role="tab">
                                                <i class="la la-trophy "></i>
                                                Ranking
                                            </a>
                                        </li>
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_3_4" role="tab">
                                                <i class="la  la-link "></i>
                                                Share Link
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                            <div class="m-portlet__body" >
                                <div class="tab-content">

                                    <div class="tab-pane active" id="m_portlet_tab_3_1">
                                            <!--begin: Datatable -->
                                        <div class="table-responsive">

                                            <table class="table table-striped- table-bordered table-hover table-checkable " id="m_table_ranking" >
                                                <thead>
                                                    <tr>
                                                        <th colspan="1" style="text-align: center"></th>
                                                        <th colspan="1" style="text-align: center">Peserta</th>
                                                        <th colspan="1" style="text-align: center">Total Braking</th>
                                                        <th colspan="1" style="text-align: center">Total Slalom</th>
                                                        <th colspan="5" style="text-align: center">Total Low Speed Balancing</th>
                                                        <th colspan="1" style="text-align: center">Total Global</th>
                                                    </tr>
                                                    <tr>
                                                        <th style="text-align: center">Rangking</th>
                                                        <th style="text-align: center">Nama</th>
                                                        <th style="text-align: center">Total</th>
                                                        <th style="text-align: center">Total</th>
                                                        <th style="text-align: center">Section 1</th>
                                                        <th style="text-align: center">Section 2</th>
                                                        <th style="text-align: center">Section 3</th>
                                                        <th style="text-align: center">Section 4</th>
                                                        <th style="text-align: center">Total</th>
                                                        <th style="text-align: center">Total Semua Modul</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($array_total_global as $index => $item)
                                                    <tr>
                                                        <td style="text-align: center"> {{$loop->iteration}} </td>
                                                        <td style="text-align: center">{{$item['name']}}</td>
                                                        <td style="text-align: center">{{$item['poin_braking']}}</td>
                                                        <td style="text-align: center">{{$item['poin_slalom']}}</td>
                                                        @for ($i = 1; $i < 5; $i++)
                                                        @php $nama_section = "section".$i;   @endphp
                                                        <td style="text-align: center">{{$item[$nama_section]}}</td>
                                                        @endfor
                                                        <td style="text-align: center">{{$item['poin_low_speed']}}</td>
                                                        <td style="text-align: center">{{$item['poin_braking'] + $item['poin_slalom'] + $item['poin_low_speed'] }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                        <div class="tab-pane " id="m_portlet_tab_3_4">
                                            <div class="form-group m-form__group row">
                                                <label class="col-form-label col-lg-3 col-sm-12">Copy This Link and Share It </label>
                                                <div class="col-lg-6 col-md-9 col-sm-12">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="m_clipboard_1" value="{{url('ranking-peserta' , ['token' => $token , 'id' => $id , 'tahun' => $tahun , 'bulan' => $bulan , 'tgl' => $tgl ])}}" >
                                                        <div class="input-group-append">
                                                            <a href="#" class="btn btn-secondary" data-clipboard="true" data-clipboard-target="#m_clipboard_1"><i class="la la-copy"></i></a>
                                                        </div>
											</div>
										</div>
									</div>
                                    </div>

                                </div>



                            </div>


                        </div>

                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
            </div>
        </div>
    </div>


<!--begin::Modal-->

<!--end::Modal-->
<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script>
    $(function(){


        @if (session('sukses'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan peserta baru ");
        @endif
        @if (session('error'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.error("Email Peserta Tersebut Sudah Ada !!!  ");

        @endif
        @if (session('update'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah data peserta  ");
        @endif
        @if (session('delete'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menghapus data peserta  ");
        @endif
        var values = "";
        var kat = 0;
        $('.gender').change(function(){
            let value = $(this).val();
            values = value;
            let kategoris = $('.kategori option:selected').val();
             if(values != "Pria" && kategoris != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                values = $('.gender option:selected').val();

                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })




    });

    var report = function() {

    var initTableReportModul = function() {

            // begin first table
            var table = $('#m_table_ranking').DataTable({
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": [0 ,1,2,3,4,5,6,7,8]
                }],
                "order": [[9, 'desc']],
            });
            		table.on('order.dt search.dt', function () {
			table.column(0, {
				search: 'applied',
				order: 'applied'
			}).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
    };


	return {

		//main function to initiate the module
		init: function() {
            initTableReportModul();
			// initTableHasil();
		},

	};

}();
jQuery(document).ready(function() {
	report.init();
});
</script>
@endsection
