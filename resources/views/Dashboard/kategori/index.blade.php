@extends('layouts.dashboard')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">Data Kategori</h3>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>

                <!-- END: Subheader -->
                    <div class="m-content">

                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            {{-- Dropdown Export Tools --}}
                                        </h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a href="#" class="btn btn-danger m-btn m-btn--custom m-btn--icon m-btn--air" data-toggle="modal" data-target="#m_modal_4">
                                                <span>
                                                    <i class="la la-plus-circle"></i>
                                                    <span>Tambah Kategori</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-portlet__nav-item"></li>
                                        <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                            <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                Export Options
                                            </a>
                                            <div class="m-dropdown__wrapper" style="z-index: 101;">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36px;"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            <ul class="m-nav">
                                                                <li class="m-nav__section m-nav__section--first">
                                                                    <span class="m-nav__section-text">Export Tools</span>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_print">
                                                                        <i class="m-nav__link-icon la la-print"></i>
                                                                        <span class="m-nav__link-text">Print</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_copy">
                                                                        <i class="m-nav__link-icon la la-copy"></i>
                                                                        <span class="m-nav__link-text">Copy</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_excel">
                                                                        <i class="m-nav__link-icon la la-file-excel-o"></i>
                                                                        <span class="m-nav__link-text">Excel</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_csv">
                                                                        <i class="m-nav__link-icon la la-file-text-o"></i>
                                                                        <span class="m-nav__link-text">CSV</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link" id="export_pdf">
                                                                        <i class="m-nav__link-icon la la-file-pdf-o"></i>
                                                                        <span class="m-nav__link-text">PDF</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body">

                                <!--begin: Datatable -->
                                <table class="table table-striped-table-bordered table-hover table-checkable responsive no-wrap" id="m_table_2">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($kategori as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->name}}</td>
                                            <td nowrap="">
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                                                    <i class="la la-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#m_modal_edit_{{$item->id}}"><i class="la la-edit"></i> Edit Kategori</a>
                                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_hapus_{{$item->id}}"><i class="la  la-trash-o"></i> Delete Kategori</a>
                                                        <a class="dropdown-item" href="{{url('/report-kategori' , $item->id)}}"><i class="la la-print"></i> Lihat Report</a>
                                                    </div>
                                                </span>
                                            </td>
                                        </tr>
                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
            </div>
        </div>
    </div>

<!--begin::Modal-->
    <div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="tambah" action="{{url('/tambah-kategori')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Nama Kategori:</label>
                            <input type="text" class="form-control" id="recipient-name" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @foreach ($kategori as $item)
    <div class="modal fade" id="m_modal_edit_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="tambah" action="{{url('/edit-kategori')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="hidden" name="id" value="{{$item->id}}">
                            <label for="recipient-name" class="form-control-label">Nama Kategori:</label>
                            <input type="text" class="form-control" id="recipient-name" name="name" value="{{$item->name}}" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_hapus_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('/delete-kategori')}}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{$item->id}}">
                        <h6 class="m-badge m-badge--danger m-badge--wide">Dengan menghapus data kategori ini, maka data-data seperti peserta akan terhapus semua </h6>
                        <h5>Apakah Anda Yakin Akan Menghapus Kategori <span class="m--font-danger">'{{$item->name}}'</span> Dari Database ?</h5>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                        <button type="submit" class="btn btn-danger">Ya, saya ingin menghapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach

<!--end::Modal-->
<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>


<script>
    $(function(){

        @if (session('sukses'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menambahkan kategori baru ");
        @endif
        @if (session('error'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.error("Email Peserta Tersebut Sudah Ada !!!  ");

        @endif
        @if (session('update'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil merubah data kategori  ");
        @endif
        @if (session('delete'))
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.success("Berhasil menghapus data peserta  ");
        @endif
        var values = "";
        var kat = 0;
        $('.gender').change(function(){
            let value = $(this).val();
            values = value;
            let kategoris = $('.kategori option:selected').val();
             if(values != "Pria" && kategoris != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })

        });

            $('.kategori ').change(function(){
                let kategori = $(this).val();
                kat = kategori;
                values = $('.gender option:selected').val();

                if(values != "Pria" && kategori != 4 ){
                    $('.form-kategori select').val(4);
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };

                    toastr.error("Gender wanita hanya dapat memiliki kategori 'matic' ");
                }
            })




    });
</script>
@endsection
