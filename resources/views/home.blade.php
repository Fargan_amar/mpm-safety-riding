@extends('layouts.dashboard')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">Dashboard</h3>
                        </div>
                        <div>
                            {{--  <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                                <span class="m-subheader__daterange-label">
                                    <span class="m-subheader__daterange-title"></span>
                                    <span class="m-subheader__daterange-date m--font-brand"></span>
                                </span>
                                <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="la la-angle-down"></i>
                                </a>
                            </span>  --}}
                        </div>
                    </div>
                </div>

                <!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">

								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--tabs" id="m_portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-calendar-2"></i>
												</span>
												<h3 class="m-portlet__head-text">

												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav nav">
                                                <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                        Pilih Modul
                                                    </a>
                                                    <div class="m-dropdown__wrapper" style="z-index: 101;left:0%">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">Daftar Modul</span>
                                                                        </li>
                                                                        @foreach ($daftar_modul as $item)
                                                                            <li class="m-nav__item">
                                                                                <a href="{{url('event' , ['tgl' => date('Y-m-d H:i:s') , 'modul' => $item->id ])}}" class="m-nav__link" >
                                                                                    <span class="m-nav__link-text">{{$item->name}}</span>
                                                                                </a>
                                                                            </li>
                                                                        @endforeach

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
												{{--  <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
													<a href="#" class="m-portlet__nav-link btn btn-primary m-btn m-btn--pill m-btn--air" data-toggle="modal" data-target="#modal_tambah_event">
														<span>
															<i class="la la-plus"></i>
															<span>Add Event</span>
														</span>
													</a>
												</li>  --}}
                                                <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                        Lihat Ranking
                                                    </a>
                                                    <div class="m-dropdown__wrapper" style="z-index: 101;left:0%">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">Pilih Kategori</span>
                                                                        </li>
                                                                        @foreach ($kategori as $item)
                                                                            <li class="m-nav__item">
                                                                                <a href="{{url('ranking' , ['id' => $item->id , 'tahun' => date('Y')  , 'bulan' => date('m') , 'tgl' => date('d') ])}}" class="m-nav__link" >
                                                                                    <span class="m-nav__link-text">{{$item->name}}</span>
                                                                                </a>
                                                                            </li>
                                                                        @endforeach

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                @if (!empty($cek_juri))
                                                @if (empty($juri))
												{{-- <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
													<a data-url="{{url('/penjurian' , ['tgl'=>$cek_juri->tgl , 'id'=>$cek_juri->id])}}" class="m-portlet__nav-link btn btn-info m-btn m-btn--pill m-btn--air" id="lihat-penjurian" >
														<span>
															<i class="la la-book" style="color:white"></i>
															<span style="color:white">Lihat Penjurian</span>
														</span>
													</a>
												</li> --}}
                                                @else
                                                {{-- <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                        Lihat Ranking
                                                    </a>
                                                    <div class="m-dropdown__wrapper" style="z-index: 101;left:0%">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">Pilih Kategori</span>
                                                                        </li>
                                                                        @foreach ($kategori as $item)
                                                                            <li class="m-nav__item">
                                                                                <a href="{{url('ranking' , ['id' => $item->id , 'tahun' => date('Y')  , 'bulan' => date('m') , 'tgl' => date('d') ])}}" class="m-nav__link" >
                                                                                    <span class="m-nav__link-text">{{$item->name}}</span>
                                                                                </a>
                                                                            </li>
                                                                        @endforeach

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li> --}}
												{{-- <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
													<a data-url="{{url('/penjurian' , ['tgl'=>$juri->tgl , 'id'=>$juri->id])}}" class="m-portlet__nav-link btn btn-info m-btn m-btn--pill m-btn--air" id="lihat-penjurian" >
														<span>
															<i class="la la-book" style="color:white"></i>
															<span style="color:white">Lihat Penjurian</span>
														</span>
													</a>
												</li> --}}

                                                @endif

                                                @endif
											</ul>
										</div>
									</div>
									<div class="m-portlet__body" style="height:auto">
										<div id="m_calendar"></div>
									</div>
								</div>

								<!--end::Portlet-->
							</div>
						</div>
					</div>
            </div>
        </div>
    </div>

<!--begin::Modal-->
        <div class="modal fade" id="modal_tambah_event" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Event/Ujian Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{url('/tambah-juri')}}" class="tambah-juri"method="POST">
                        @csrf
                    <div class="modal-body">
                    <div class="alert alert-info"  role="alert">
                        <strong>Penting !</strong> Dengan Menambahkan Ujian Baru , Maka Otomatis Anda Menjadi Juri .
                    </div>
                        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="450">
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Deskripsi</label>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="input-group">
                                        <textarea name="deskripsi" id="" class="form-control m-input deskripsi" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Jumlah Event</label>
                                <div class="col-lg-6 col-md-12 col-sm-12">
								    <input id="m_touchspin_10" type="text" class="form-control bootstrap-touchspin-vertical-btn allownumericwithoutdecimal jml_event" name="jml_event"  type="text" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Pilih Tanggal</label>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="input-group date">
                                        @if (Auth::user()->jabatan_id == 1)
                                            <input type="text" class="form-control m-input" readonly value="{{date("m/d/Y")}}" id="m_datepicker_superadmin" name="tgl" required/>
                                        @else
                                            <input type="text" class="form-control m-input" readonly value="{{date("m/d/Y")}}" id="m_datepicker_3" name="tgl" required/>

                                        @endif
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Pilih Waktu Mulai</label>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="input-group date">
                                        <input class="form-control" id="m_timepicker_1_modal" readonly placeholder="Select time" type="text" name="waktu" required/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide tambah_modul" onclick="tambah_modul()">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Tambah Modul</span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="pilih_modul">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Pilih Modul</label>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="input-group">
                                            <select name="modul_id[]" id="" class="form-control modul" required>
                                                <option value="" selected></option>
                                                @foreach ($modul as $item)
                                                <option value="{{$item->modul->id}}">{{$item->modul->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Pilih Kategori</label>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="input-group">
                                        <select name="kategori_id" id="" class="form-control kategori" required>
                                            <option value=""></option>
                                            @foreach ($kategori as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__group form-group">
                                <label>Pilih Peserta</label>
                                <div class="m-checkbox-list peserta">
                                    {{-- <label class="m-checkbox m-checkbox--check-bold m-checkbox--state-success">
                                        <input type="checkbox"> Success state
                                        <span></span>
                                    </label> --}}
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary submit-event" >Save changes</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

<!--end::Modal-->
<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script>
    $(function(){

        @if(session('peserta'))
                toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                };

                toastr.warning("Pilih Peserta Terlebih Dahulu !!! ");
        @endif
        @if (session('error'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };

            toastr.error(" Event Penjurian Pada Jam Tersebut Sudah Ada !!! Silahkan Ulangi Kembali Daftar Event Baru  ");

        @endif

        $('#lihat-penjurian').click(function(){
            window.open($(this).data('url') , 'gcalevent', 'width=auto,height=auto')
        })
         $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            if($(this).val() > 10)
            {
                $(this).val("10");
            }
        });

        $('.kategori').change(function(){
            let kategori = $(this).val();
            if(kategori == "1"){
                $('.child').remove();
                $('.peserta').append(
                    '<div class="child">'+
                            '<label class="m-checkbox m-checkbox--check-bold m-checkbox--state-info">'+
                                '<input type="checkbox" name="" value="" id="checkAll">Pilih Semua Peserta'+
                                '<span></span>'+
                            '</label>'+
                        @foreach ($p_instruktur as $item)
                            '<label class="m-checkbox m-checkbox--check-bold m-checkbox--state-success">'+
                                '<input type="checkbox" name="peserta[]" value="{{$item->id}}" > {{$item->name}}-{{$item->no_punggung}}'+
                                '<span></span>'+
                            '</label>'+
                        @endforeach
                    '</div>'

                )
            }
            else if(kategori == "2"){
                $('.child').remove();
                $('.peserta').append(
                    '<div class="child">'+
                            '<label class="m-checkbox m-checkbox--check-bold m-checkbox--state-info">'+
                                '<input type="checkbox" name="" value="" id="checkAll">Pilih Semua Peserta'+
                                '<span></span>'+
                            '</label>'+
                        @foreach ($p_sport as $item)
                            '<label class="m-checkbox m-checkbox--check-bold m-checkbox--state-success" >'+
                                '<input type="checkbox" name="peserta[]" value="{{$item->id}}"> {{$item->name}}-{{$item->no_punggung}}'+
                                '<span></span>'+
                            '</label>'+
                        @endforeach
                    '</div>'

                )
            }
            else if(kategori == "3"){
                $('.child').remove();
                $('.peserta').append(
                    '<div class="child">'+
                            '<label class="m-checkbox m-checkbox--check-bold m-checkbox--state-info">'+
                                '<input type="checkbox" name="" value="" id="checkAll">Pilih Semua Peserta'+
                                '<span></span>'+
                            '</label>'+
                        @foreach ($p_dealer as $item)
                            '<label class="m-checkbox m-checkbox--check-bold m-checkbox--state-success" >'+
                                '<input type="checkbox" name="peserta[]" value="{{$item->id}}"> {{$item->name}}-{{$item->no_punggung}}'+
                                '<span></span>'+
                            '</label>'+
                        @endforeach
                    '</div>'

                )
            }
            else if(kategori == "4"){
                $('.child').remove();
                $('.peserta').append(
                    '<div class="child">'+
                            '<label class="m-checkbox m-checkbox--check-bold m-checkbox--state-info">'+
                                '<input type="checkbox" name="" value="" id="checkAll">Pilih Semua Peserta'+
                                '<span></span>'+
                            '</label>'+
                        @foreach ($p_matic as $item)
                            '<label class="m-checkbox m-checkbox--check-bold m-checkbox--state-success" >'+
                                '<input type="checkbox" name="peserta[]" value="{{$item->id}}"> {{$item->name}}-{{$item->no_punggung}}'+
                                '<span></span>'+
                            '</label>'+
                        @endforeach
                    '</div>'

                )
            }
            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });



        })

        $('.submit-event').click(function(){
            countCheckedCheckboxes = $('input[name="peserta[]"]').filter(':checked').length;
            let deskripsi = $('.deskripsi').val();
            let modul = $('.modul').val();
            let kategori = $('.kategori').val();
            let jml_event = $('.jml_event').val();

            if(jml_event == ""){
                toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                };

                toastr.warning("Masukkan Jumlah Event  Terlebih Dahulu !!! ");
            }

            if(deskripsi == ""){
                toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                };

                toastr.warning("Isi Deskripsi Terlebih Dahulu !!! ");
            }
            if(modul == ""){
                toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                };

                toastr.warning("Pilih Modul Terlebih Dahulu !!! ");
            }
            if(kategori == ""){
                toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                };

                toastr.warning("Pilih Kategori Terlebih Dahulu !!! ");
            }
            if(countCheckedCheckboxes == 0){
                toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                };

                toastr.warning("Pilih Peserta Terlebih Dahulu !!! ");
            }
            else if(countCheckedCheckboxes > 0){
                console.log(countCheckedCheckboxes);
                $('.tambah-juri').submit();
            }
        })

        var klik = 0;
        $('.tambah_modul').click(function(){
            klik += 1;
            $('.pilih_modul').append(
                '<div class="pilih_modul'+klik+'">'+
                    '<div class="form-group m-form__group row">'+
                        '<label class="col-form-label col-lg-3 col-sm-12">Pilih Modul</label>'+
                        '<div class="col-lg-6 col-md-12 col-sm-12">'+
                            '<div class="input-group">'+
                                '<select name="modul_id[]" id="" class="form-control modul" required>'+
                                    '<option value="" selected></option>'+
                                    @foreach ($modul as $item)
                                    '<option value="'+{{$item->modul->id}}+'">{{$item->modul->name}}</option>'+
                                    @endforeach
                                '</select>'+
                            '</div>'+
                    ' </div>'+
                    '<div class="col-md-2 " >'+
                        '<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill float-right hapus-modul" data-index="'+klik+'">'+
                            '<span>'+
                                '<i class="la la-trash-o"></i>'+
                                '<span>Delete</span>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '</div>'

            );
            $('.hapus-modul').click(function(){
                var index = $(this).data('index');
                $('.pilih_modul'+index).remove();
            })
        });
    })

var CalendarGoogle = function() {

    return {
        //main function to initiate the module
        init: function() {
            var todayDate = moment().startOf('day');
            var YM = todayDate.format('YYYY-MM');
            var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
            var TODAY = todayDate.format('YYYY-MM-DD');
            var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');


            $('#m_calendar').fullCalendar({
                isRTL: mUtil.isRTL(),
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,listYear'
                },


                displayEventTime: true, // don't show the time column in list view

                // THIS KEY WON'T WORK IN PRODUCTION!!!
                // To make your own Google API key, follow the directions here:
                // http://fullcalendar.io/docs/google_calendar/
                googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',

                // US Holidays
                // events: 'en.usa#holiday@group.v.calendar.google.com',
                events:
                 [

                 @foreach ($kalender_juri as $item)


                     {
                        title: ' Modul {{$item->modul->name}}',
                        url: '{{url("/lihat-event" , ["tgl"=>$item->tgl_calendar , "id"=>$item->modul_id])}}',
                        start: '{{$item->tgl_calendar}}',
                        description: 'Modul {{$item->modul->name}}',
                        @if (strcasecmp("braking" , $item->modul->name) == 0)
                            className: "m-fc-event--solid-info m-fc-event--light"
                        @elseif(strcasecmp("slalom course" , $item->modul->name) == 0)
                            className: "m-fc-event--solid-success m-fc-event--light"
                        @elseif(strcasecmp("low speed balancing" , $item->modul->name) == 0)
                            className: "m-fc-event--solid-warning m-fc-event--light"
                        @endif
                    },


                @endforeach
                ],


                eventClick: function(event) {
                    // opens events in a popup window
                    window.open(event.url, 'gcalevent', 'width=700,height=600');
                    return false;
                },

                loading: function(bool) {
                    return;

                    /*
                    mApp.block(portlet.getSelf(), {
                        type: 'loader',
                        state: 'success',
                        message: 'Please wait...'
                    });
                    */
                },

                eventRender: function(event, element) {
                    if (!event.description) {
                        return;
                    }

                    if (element.hasClass('fc-day-grid-event')) {
                        element.data('content', event.description);
                        element.data('placement', 'top');
                        mApp.initPopover(element);
                    } else if (element.hasClass('fc-time-grid-event')) {
                        element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
                    } else if (element.find('.fc-list-item-title').lenght !== 0) {
                        element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
                    }
                }
            });
        }
    };
}();

jQuery(document).ready(function() {
    CalendarGoogle.init();
});

</script>
@endsection
