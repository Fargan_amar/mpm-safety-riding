<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile' , 'HomeController@profile')->middleware('auth');
Route::get('/mpmsr','HomeController@mpm');


Route::post('/lupa-password' , 'UserController@lupaPassword')->name('lupa-password');
Route::get('/reset-password/{token}' , 'UserController@resetPassword');
Route::post('/submit-reset/{token}' , 'UserController@submitReset');


//kategori
Route::get('/kategori' , 'KategoriController@index')->middleware('auth');
Route::post('/tambah-kategori' ,'KategoriController@tambah')->middleware('auth');
Route::post('/edit-kategori', 'KategoriController@edit')->middleware('auth');
Route::post('/delete-kategori' , 'KategoriController@delete')->middleware('auth');
Route::get('/report-kategori/{id}' , 'KategoriController@report' )->middleware('auth');
Route::get( '/report-kategori/{id}/{modul}/{tahun}/{bulan}/{tgl}' ,'KategoriController@reportKategori')->middleware('auth');
Route::get( '/laporan-kategori/{token}/{id}/{modul}/{tahun}/{bulan}/{tgl}', 'KategoriController@publicReportKategori');
Route::get('/ranking/{id}/{tahun}/{bulan}/{tgl}' , 'KategoriController@Ranking')->middleware('auth');
Route::get('/ranking-peserta/{token}/{id}/{tahun}/{bulan}/{tgl}', 'KategoriController@publicRanking');


//Peserta / user
Route::resource('user', 'UserController')->middleware('auth');
Route::get('/peserta' , 'UserController@peserta')->middleware('auth');
Route::get('/report-peserta/{id}' , 'UserController@report')->middleware('auth');
Route::get('/report-peserta/{id}/{modul}/{tahun}/{bulan}', 'UserController@reportPeserta')->middleware('auth');
Route::get('/laporan-peserta/{token}/{id}/{modul}/{tahun}/{bulan}', 'UserController@publicReportPeserta');

Route::post('/tambah-peserta' , 'UserController@tambahPeserta')->middleware('auth');
Route::post('/user-edit', 'UserController@EditUser')->middleware('auth');
Route::post('/delete-user', 'UserController@DeleteUser')->middleware('auth');
Route::get('/admin' , 'UserController@admin')->middleware('auth');
Route::post('/tambah-admin' , 'UserController@tambahAdmin')->middleware('auth');
Route::post('/edit-admin', 'UserController@EditAdmin')->middleware('auth');
Route::post('/makeadmin' , 'UserController@makeAdmin')->middleware('auth');
Route::post('/removeadmin', 'UserController@removeAdmin')->middleware('auth');

Route::post('/ubah-profile' ,'UserController@ubahProfile')->middleware('auth');
Route::get('/cek-password/{id}/{pass}' ,'UserController@CekPassword')->middleware('auth');
Route::post('/ubah-password' ,'UserController@UbahPassword')->middleware('auth');

//modul
Route::resource('/modul', 'ModulController')->middleware('auth');
Route::post('/tambah-modul' , 'ModulController@tambahModul')->middleware('auth');
Route::post('/edit-modul', 'ModulController@editModul')->middleware('auth');
Route::Post('/delete-modul', 'ModulController@hapusModul')->middleware('auth');
Route::get('/lihat-report/{modul_id}' , 'ModulController@lihatReport')->middleware('auth');
Route::get('/lihat-report-tgl/{modul_id}/{tgl}', 'ModulController@lihatReportTgl')->middleware('auth');
Route::get('/lihat-report-tgl/{token}/{modul_id}/{tgl}', 'ModulController@PublicLihatReportTgl');
Route::get('/lihat-daftar-penilaian/{modul}' ,'ModulController@LihatPenilaian')->middleware('auth');

//penilaian
Route::get('/penilaian', 'PenilaianController@index')->middleware('auth');
Route::post('/tambah-penilaian/{id}', 'PenilaianController@tambahPenilaian')->middleware('auth');
Route::post('/tambah-penilaian-dis/{id}', 'PenilaianController@tambahPenilaianDis')->middleware('auth');
Route::post('/tambah-penilaian-jarak/{id}', 'PenilaianController@tambahPenilaianJarak')->middleware('auth');
Route::post('/tambah-penilaian-waktu/{id}', 'PenilaianController@tambahPenilaianWaktu')->middleware('auth');
Route::post('/edit-penilaian', 'PenilaianController@editPenilaian')->middleware('auth');
Route::post('/delete-penilaian', 'PenilaianController@deletePenilaian')->middleware('auth');


//juri
Route::get('/daftar-penjurian/{bulan}/{tahun}','JuriController@DaftarPenjurian')->middleware('auth');
Route::get('/hapus-penjurian/{id}' ,'JuriController@HapusPenjurian')->middleware('auth');
Route::get('/batal-penjurian/{id}/{modul}', 'JuriController@BatalPenjurian')->middleware('auth');
Route::get('/event/{tgl}/{modul}','JuriController@Event')->middleware('auth');
Route::post('/tambah-event' ,'JuriController@TambahEvent')->middleware('auth');
Route::get('/penjurian/{tgl}/{u}' , 'JuriController@index')->name('penjurian')->middleware('auth');
Route::post('/daftar-penjurian/{tgl}/{modul}', 'JuriController@LihatJuri')->middleware('auth');
Route::get('/lihat-event/{tgl}/{u}', 'JuriController@LihatEvent')->middleware('auth');

// Route::get('/penjurian/{tgl}/{u}/{kategori}', 'JuriController@juri')->name('penjurian')->middleware('auth');
Route::post('/tambah-juri' , 'JuriController@tambahJuri')->middleware('auth');
Route::post('/selesai-juri', 'JuriController@selesaiJuri')->middleware('auth');
Route::post('/tambah-target-waktu',  'JuriController@tambahTargetWaktu')->middleware('auth');


//penilaian peserta
Route::get('/tambah-nilai/{tgl}/{u}/{j}/{n}', 'UserPenilaianController@tambah')->name('tambah-nilai')->middleware('auth');
Route::get('/lihat-nilai/{tgl}/{u}/{j}/{n}', 'UserPenilaianController@lihatNilaiTanggal')->middleware('auth');
Route::get('/lihat-nilai/{token}/{tgl}/{u}/{j}/{n}', 'UserPenilaianController@PublicLihatNilaiTanggal');
Route::post('/tambah-user-nilai', 'UserPenilaianController@tambahNilai')->middleware('auth');


